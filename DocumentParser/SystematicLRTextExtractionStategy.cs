﻿using System;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text.pdf.parser;
using iTextSharp.text.pdf;
using SystematicLR.Common;
namespace SystematicLR.DocumentParser
{
    public class SystematicLRTextExtractionStategy : iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy
    {

        /// <summary>
        /// initialising the local variables
        /// </summary>
        /// 
        public Dictionary<string, string> KeywordsOptions { set; get; }
        public Dictionary<string, string> ExpressionsOptions { set; get; }
        private StringBuilder result = new StringBuilder();        
        private Vector lastBaseLine;
        private string lastFont;
        private float lastFontSize;
        private bool lastisBold;
        private int TermIndex;



        private Vector lastStart;
        private Vector lastEnd;
        public Document document { private set; get; }
        private enum TextRenderMode
        {
            FillText = 0,
            StrokeText = 1,
            FillThenStrokeText = 2,
            Invisible = 3,
            FillTextAndAddToPathForClipping = 4,
            StrokeTextAndAddToPathForClipping = 5,
            FillThenStrokeTextAndAddToPathForClipping = 6,
            AddTextToPaddForClipping = 7
        }        
       
        public SystematicLRTextExtractionStategy(Document doc)
        {
            this.document = doc;
        }
      
        public override void RenderText(iTextSharp.text.pdf.parser.TextRenderInfo renderInfo)
        {
            

            string curFont = renderInfo.GetFont().PostscriptFontName;
            //Check if font bold is used
            if ((renderInfo.GetTextRenderMode() == (int)TextRenderMode.FillThenStrokeText))
            {
                
                curFont += "-Bold";
            }

            //This code assumes that if the baseline changes then we're on a newline
            Vector curBaseline = renderInfo.GetBaseline().GetStartPoint();
            Vector topRight = renderInfo.GetAscentLine().GetEndPoint();
            Vector end= renderInfo.GetBaseline().GetEndPoint();
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(curBaseline[Vector.I1], curBaseline[Vector.I2], topRight[Vector.I1], topRight[Vector.I2]);
            Single curFontSize =float.Parse(rect.Height.ToString("0.000"));
            bool curIsBold = curFont.ToLower().Contains("bold");
            
            //See if something has changed, either the baseline, the font or the font size
            if ((this.lastBaseLine == null)        
                || (curFontSize != lastFontSize)
                || (curIsBold!=lastisBold) 
                )
            {                
                //if we've put down at least one span tag close it
                if ((this.lastBaseLine != null))
                {                    

                    var lst =Utilities.SystematicLRTokenize(result.ToString());
                    lst.ForEach((item)=>ProcessTerm(item));                    
                    result.Clear();
                }                               
            }
            if(result.Length>0)
            {
                if (result[result.Length - 1] != ' ' && renderInfo.GetText().Length > 0 && renderInfo.GetText()[0] != ' ')
                {
                    // we only insert a blank space if the trailing character of the previous string wasn't a space, and the leading character of the current string isn't a space
                    float spacing = lastEnd.Subtract(curBaseline).Length;
                    if (spacing > renderInfo.GetSingleSpaceWidth() / 2f)
                    {
                        result.Append(" ");
                    }
                }
            }
           

            //Append the current text
            this.result.Append(renderInfo.GetText());

            //Set currently used properties
            this.lastBaseLine = curBaseline;
            this.lastEnd = end;
            this.lastFontSize = curFontSize;
            this.lastFont = curFont;
            this.lastisBold = curIsBold;
        }
        private void ProcessWord(string item,bool withSteming)
        {
            var term = new ExtractedTerm(this.document, item, ++TermIndex, KeywordsOptions,withSteming) { Font = new TermFont() { Bold = lastisBold, FontSize = lastFontSize } };
            document.AddTerm(term);
        }
        private void ProcessTerm(string item)
        {
            if(ExpressionsOptions!=null)
            {
                string tempExpression;
                string stemexpression = Utilities.StemTerm(item);
                if(ExpressionsOptions.TryGetValue(stemexpression, out tempExpression))
                {
                    var lst = Utilities.SystematicLRTokenize(tempExpression);
                    lst.ForEach((key) => ProcessWord(key,false));
                    return;
                }
            }
            ProcessWord(item, true);
        }
        public override string GetResultantText()
        {
            //If we wrote anything then we'll always have a missing closing tag so close it here
            if (result.Length > 0)
            {
                var lst = Utilities.SystematicLRTokenize(result.ToString());
                lst.ForEach((item) => ProcessTerm(item));
                result.Clear();          
            }
            return result.ToString();
        }
       
        //Not needed
        //public  void BeginTextBlock() { }
        //public void EndTextBlock() { }
        //public void RenderImage(ImageRenderInfo renderInfo) { }
    }

}