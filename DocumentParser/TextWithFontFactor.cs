﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using SystematicLR.Common;

namespace SystematicLR.DocumentParser
{
    public class TextFont
    {
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var newValue = (TextFont)obj;
            if (newValue.isBold == this.isBold && newValue.Size == this.Size)
                return true;
            return false;
        }
        public override int GetHashCode()
        {
            return Size.GetHashCode()  ^ isBold.GetHashCode();
        }
        public float Size
        {
            set; get;
        }
        //public string FontName
        //{
        //    set; get;
        //}
        public bool isBold
        {
            set; get;
        }

    }
    public class TextFactor
    {
      public Decimal MultipleFactor { set; get; }
        private StringBuilder stringbuilder;
       // public TextFont Font { set; get; }
        public TextFactor()
        {
            this.stringbuilder = new StringBuilder();
        }
        public TextFactor(string text)
        {
            this.stringbuilder = new StringBuilder(text);
        }
        public string Text
        {
            get
            {
                return this.stringbuilder.ToString();
            }
        }
      
        public void Append(string text)
        {
            this.stringbuilder.Append(text);
        }
        public List<Term> Analayize()
        {
            var text = stringbuilder.ToString();
            //text = Utilities.Stem(text);
            var token = Utilities.TokenizeContent(text, true);
            //var tags = Utilities.POSTagger(token.ToArray());
            //var ssss = Utilities.ExtractNounPhrases(token.ToArray(), tags);
            //var terms=Utilities.ExtractTokensFromContent(stringbuilder.ToString());
            //  var contTerm = Utilities.GetTermsFromContent(stringbuilder.ToString(), true);
            //  var non = Utilities.GetNounsFromContent(text,true);

            var frequency = token.GroupBy(e => e.Trim().ToLower()).Select(e => new Term(MultipleFactor) { word = e.Key, frequency = e.Count() }).ToList();

            return frequency;

            // var terms = Utilities.Getterms(token);
            // return terms.Select(e => new Term(MultipleFactor) { word = e.Key, frequency = e.Value }).ToList();
        }

        /// <summary>
        /// public method, using to extract the keywords and terms by calling the suitable private method as per the desired extraction mode 
        /// </summary>
        /// <param name="mode">ExtractingMode ( ExtractTerms, ExtractKeywords,ExtractKeywordsWithStemming)</param>
        /// <returns>list of term object, that contains the keyword or term along with frequency (stringth) </returns>
        public List<Term> Extract(ExtractingMode mode)
        {
            switch (mode)
            {
                case ExtractingMode.ExtractTerms:
                    return this.ExtractTerms();
                case ExtractingMode.ExtractKeywords:
                    return this.ExtractKeywords();
                case ExtractingMode.ExtractKeywordsWithStemming:
                    return this.ExtractKeywordsWithStemming();
                default:
                    return new List<Term>();
            }

        }
        /// <summary>
        /// private method using to extract keywords  
        /// </summary>
        /// <returns></returns>
        private List<Term> ExtractKeywords()
        {
            var text = stringbuilder.ToString();
            var token = Utilities.TokenizeContent(text, true);
            var frequency = token.GroupBy(e => e.Trim().ToLower()).Select(e => new Term(MultipleFactor) { word = e.Key, frequency = e.Count() }).ToList();
            return frequency;
        }
        /// <summary>
        /// private method using to extract keywords after applying stemming 
        /// </summary>
        /// <returns></returns>
        private List<Term> ExtractKeywordsWithStemming()
        {
            var text = stringbuilder.ToString();
            text = Utilities.Stem(text);
            var token = Utilities.TokenizeContent(text, true);           
            var frequency = token.GroupBy(e => Utilities.Stem(e.Trim().ToLower())).Select(e => new Term(MultipleFactor) { word = e.Key, frequency = e.Count() }).ToList();
            return frequency;

        }
        /// <summary>
        ///  private method using to extract terms from the document  
        /// </summary>
        /// <returns></returns>
        private List<Term> ExtractTerms()
        {
            var text = stringbuilder.ToString();
            var token = Utilities.TokenizeContent(text, true);
            var terms = Utilities.Getterms(token);
            return terms.Select(e => new Term(MultipleFactor) { word = e.Key, frequency = e.Value }).ToList();
        }

    }
}