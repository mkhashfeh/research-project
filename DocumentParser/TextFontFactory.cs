﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
namespace SystematicLR.DocumentParser
{
    public enum ExtractingMode
    {
        ExtractTerms,
        ExtractKeywords,
        ExtractKeywordsWithStemming

    }
    public class TextFontFactory
    {
        public int TextLength
        {
            private set;
            get;
        }
        Dictionary<TextFont, TextFactor> dictionary;
        public TextFontFactory()
        {
            dictionary = new Dictionary<TextFont, TextFactor>();
        }
        public void add(TextFont font, string text)
        {
            if (string.IsNullOrEmpty(text.Trim()))
                return;
            if(dictionary.ContainsKey(font))
            {
                dictionary[font].Append(" " + text);
            }
            else
            {
                dictionary.Add(font, new TextFactor(text));
            }
        }
        public Dictionary<TextFont, TextFactor> GetDictionary()
        {
            return this.dictionary;
        }
        public Dictionary<string, decimal> Analayize(ExtractingMode mode)
        {
            decimal boldWight = 1;
            int maxSize = this.GetDictionary().Max(e => e.Value.Text.Length);
            var mainparegraph = this.dictionary.FirstOrDefault(e => e.Value.Text.Length == maxSize);
            if (!mainparegraph.Key.isBold)
            {
                boldWight = new decimal(1.5);
            }
            List<Term> AlltextTermsRaw = new List<Term>();
            List<Term> AlltextTerms = new List<Term>();
            foreach (var item in this.dictionary.Where(e => e.Key.Size <= mainparegraph.Key.Size))
            {
                item.Value.MultipleFactor = item.Key.isBold ? boldWight : 1;
                AlltextTermsRaw.AddRange(item.Value.Extract(mode));
            }
            decimal fontsizefactor = new decimal(1.5);
            foreach (var item in this.dictionary.Where(e => e.Key.Size > mainparegraph.Key.Size).OrderBy(e => e.Key.Size))
            {
                item.Value.MultipleFactor = (item.Key.isBold ? boldWight : 1) * fontsizefactor;
                var alz = item.Value.Extract(mode);
                if(alz.Count>0)
                {
                    AlltextTermsRaw.AddRange(alz);
                    fontsizefactor = fontsizefactor + new decimal(0.5);
                }
                
            }
            AlltextTerms= AlltextTermsRaw.GroupBy(e => e.word).Select(e => new Term() { word = e.Key, frequency = e.Sum(g => g.LoadedFrequency) }).ToList();

            decimal maxFreq = AlltextTerms.Max(e => e.frequency);
            Dictionary<string, decimal> WordsWight = new Dictionary<string, decimal>();
            foreach(var w in AlltextTerms.OrderByDescending(e=>e.frequency))
            {
                WordsWight.Add(w.word, Math.Round(w.frequency / maxFreq,2));
            }
            return WordsWight;

        }
    }
}