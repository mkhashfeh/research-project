﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SystematicLR.DocumentParser;
using System.IO;
using SystematicLR.Common;
namespace SystematicLR.DocumentParser
{
    public class Parser
    {
        public async static Task<SystematicLR.Common.Document> ParseAsync(DocumentContext context)
        {
            return Parse(context);
          
        }
        public static SystematicLR.Common.Document Parse(DocumentContext context)
        {
            SystematicLR.Common.Document doc = null;

            using (PdfReader reader = new PdfReader(context.DocumentPath))
            {
                doc = new SystematicLR.Common.Document(context);
                SystematicLRTextExtractionStategy EziFindStrategy = new SystematicLRTextExtractionStategy(doc);
                EziFindStrategy.KeywordsOptions = context.KeywordsOptions;
                EziFindStrategy.ExpressionsOptions = context.ExpressionsOptions;
                doc.PagesCount = reader.NumberOfPages;
                // passing the pages one by one to itextSharp engine along with our own parsing strategy
                for (int i = 1; i <= doc.PagesCount && i <= 25; i++)
                {
                    iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(reader, i, EziFindStrategy);

                }
            }

            doc.AnalyzeDocument();
            return doc;
        }
    }
}
