﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf.parser;
using SystematicLR.Common;

namespace SystematicLR.DocumentParser
{
    public class EziRenderListener : IRenderListener
    {
        private StringBuilder result = new StringBuilder();
        private StringBuilder AllText = new StringBuilder();
        private TextFontFactory factory = new TextFontFactory();
        private Vector lastBaseLine;
        private string lastFont;
        private float lastFontSize;
        private bool lastisBold;
        private int charcount = 0;
        private Indices indices;
        public Dictionary<Font, List<Indices>> fontslist = new Dictionary<Font, List<Indices>>();
        public List<Indices> boldFonts = new List<Indices>();

        //http://api.itextpdf.com/itext/com/itextpdf/text/pdf/parser/TextRenderInfo.html
        private enum TextRenderMode
        {
            FillText = 0,
            StrokeText = 1,
            FillThenStrokeText = 2,
            Invisible = 3,
            FillTextAndAddToPathForClipping = 4,
            StrokeTextAndAddToPathForClipping = 5,
            FillThenStrokeTextAndAddToPathForClipping = 6,
            AddTextToPaddForClipping = 7
        }

        public TextFontFactory getFactory()
        {
            return this.factory;
        }

        public void RenderText(iTextSharp.text.pdf.parser.TextRenderInfo renderInfo)
        {
            if(indices==null)
            {
                indices = Indices.GetInstance();
               
            }
            string curFont = renderInfo.GetFont().PostscriptFontName;
            //Check if faux bold is used
            if ((renderInfo.GetTextRenderMode() == (int)TextRenderMode.FillThenStrokeText))
            {

                curFont += "-Bold";
            }

            //This code assumes that if the baseline changes then we're on a newline
            Vector curBaseline = renderInfo.GetBaseline().GetStartPoint();
            Vector topRight = renderInfo.GetAscentLine().GetEndPoint();
            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(curBaseline[Vector.I1], curBaseline[Vector.I2], topRight[Vector.I1], topRight[Vector.I2]);
            Single curFontSize = float.Parse(rect.Height.ToString("0.000"));
            bool curIsBold = curFont.ToLower().Contains("bold");
            //See if something has changed, either the baseline, the font or the font size
            if ((this.lastBaseLine == null)
                || (curBaseline[Vector.I2] != lastBaseLine[Vector.I2]) 
                || (curFontSize != lastFontSize)
                || (curIsBold != lastisBold)
                )
            {
               
                //if we've put down at least one span tag close it
                if ((this.lastBaseLine != null))
                {
                    factory.add(new TextFont() { Size = lastFontSize, isBold = lastisBold }, result.ToString());
                    indices.EndIndex = AllText.Length;
                    //var font = new Font() { FontSize = lastFontSize };
                    //if (this.fontslist.ContainsKey(font))
                    //{
                    //    this.fontslist[font].Add(new Indices()
                    //}
                    //else
                    //{
                    //    dictionary.Add(font, new TextFactor(text));
                    //}
                    //this.fontslist.Add(,);
                    result.Clear();
                }
                //If the baseline has changed then insert a line break
                //if ((this.lastBaseLine != null) && curBaseline[Vector.I2] != lastBaseLine[Vector.I2])
                //{
                //    this.result.AppendLine("<br />");
                //}
                //Create an HTML tag with appropriate styles
                
                if ((this.lastBaseLine != null) && curBaseline[Vector.I2] != lastBaseLine[Vector.I2])
                {
                    this.result.AppendLine("\n");
                    this.AllText.Append("\n");
                }
                
                

            }

            //Append the current text
            this.result.Append(renderInfo.GetText());
            this.AllText.Append(renderInfo.GetText());
            //Set currently used properties
            this.lastBaseLine = curBaseline;
            this.lastFontSize = curFontSize;
            this.lastFont = curFont;
            this.lastisBold = curIsBold;

        }

        public string GetResultantText()
        {
            //If we wrote anything then we'll always have a missing closing tag so close it here
            if (result.Length > 0)
            {
                factory.add(new TextFont() { Size = lastFontSize, isBold = lastisBold }, result.ToString());
            }
            return AllText.ToString();

        }

        //Not needed
        public void BeginTextBlock() { }
        public void EndTextBlock() { }
        public void RenderImage(ImageRenderInfo renderInfo) { }
    }
}
