﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.DocumentParser
{
    public class Term
    {       
        public Term(decimal MultipleFactor)
        {
            this.MultipleFactor = MultipleFactor;

        }
        public Term()
        {
            this.MultipleFactor = 1;
        }
        private decimal MultipleFactor;
        public string word { set; get; }
        public decimal frequency { set; get; }
        public decimal LoadedFrequency
        {
            get
            {
                return frequency * MultipleFactor;
            }
        }
    }
}
