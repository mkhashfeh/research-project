﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Common;
using SystematicLR.Entities;
using System.Threading;
using SystematicLR.DocumentParser;
using SystematicLR.AnalyzerAgent.Helpers;
using System.Configuration;
using System.IO;
namespace SystematicLR.AnalyzerAgent
{
    class Program
    {
        static int sleep = 0;
        static bool ExportResults = false;
       static void Main(string[] args)
        {
            Console.Title = "Documents Parser Application - Powered by Mouayad Khashfeh";            
            string DocumentsSource = ConfigurationManager.AppSettings["DocumentsSource"];
            string MultiAgent = ConfigurationManager.AppSettings["MultiAgent"];
            sleep = int.Parse(ConfigurationManager.AppSettings["sleep"]);
            ExportResults = bool.Parse(ConfigurationManager.AppSettings["exportResults"]);
            List<ConsoleColor> col = new List<ConsoleColor>();
            
            


            if (DocumentsSource == "DB")
            {
                string DBRootPath = ConfigurationManager.AppSettings["RootPath"];
                var documentscount = DocumentPipeline.FillDocumentPipeline(DBRootPath);

                if (MultiAgent == "E")
                {
                    int numberOfAgents = int.Parse(ConfigurationManager.AppSettings["MaxNumberOfAgents"]);

                    Task[] threads = new Task[numberOfAgents];
                    for (int i = 0; i < numberOfAgents; i++)
                    {
                        threads[i] = new Task(() => parseDocumentsFromQueue());


                        threads[i].Start();
                    }

                    Task.WaitAll(threads);
                }
                else if (MultiAgent == "D")
                {
                    parseDocumentsFromQueue();
                }
                Console.ReadLine();
            }
            else if(DocumentsSource== "Folder")
            {
                string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
                var documentscount = DocumentPipeline.FillDocumentPipeline(new DirectoryInfo(FolderPath));
                int numberOfAgents = int.Parse(ConfigurationManager.AppSettings["MaxNumberOfAgents"]);
                DateTime start = DateTime.Now;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(string.Format("{0}\tStarting the parsing process:\n* Number of documents ={1}\n* Number of Agents={2}\n\n",start.ToString("HH:mm:ss"),documentscount,numberOfAgents));
                Console.ForegroundColor=ConsoleColor.White;
                if (MultiAgent == "E")
                {
                    //Task[] taskArray = new Task[numberOfAgents];
                    //for (int i = 0; i < taskArray.Length; i++)
                    //{
                    //    string agentName = "Agent-" + (i + 1).ToString();
                    //    taskArray[i] = Task.Factory.StartNew((object agentname)=>{  parseDocumentsFromFolder(agentname as string); }, agentName);
                    //}

                    //var docs = DocumentPipeline.Documents.ToList();
                    //int numPerArr = docs.Count / numberOfAgents;
                    //List<List<DocumentContext>> lstDoc = new List<List<DocumentContext>>(numberOfAgents);
                    //int i = 0;
                    //for( i=0;i<numberOfAgents-1;i++)
                    //{
                    //    lstDoc.Add(docs.Skip(i * numPerArr).Take(numPerArr).ToList());
                    //}
                    //lstDoc.Add(docs.Skip(i * numPerArr).ToList());

                    //Task[] workers = new Task[numberOfAgents];



                    //ThreadPool.SetMaxThreads(numberOfAgents, numberOfAgents);                    
                    //AutoResetEvent auto = new AutoResetEvent(false);
                    //var numOfDoc = DocumentPipeline.Documents.Count;
                    //var handler = new ManualResetEvent[numOfDoc];
                    //int i = 0;
                    //while (true)
                    //{
                    //    DocumentContext context = new DocumentContext(); ;
                    //    if (!DocumentPipeline.TryDequeue(out context))
                    //        break;
                    //    handler[i] = new ManualResetEvent(false);
                    //    var currenthandler = handler[i];
                    //    Action<DocumentContext> action = (c) => { try { parseDocuments(c); } finally { currenthandler.Set(); } };
                    //    //string agentName = "Agent-" + (i+1).ToString();
                    //    ThreadPool.QueueUserWorkItem(x=> action(context));

                    //    //workers[i]=Task.Run(async () => await parseDocumentsFromFolder(agentName));
                    //    i++;
                    //}
                    //WaitHandle.WaitAll(handler);
                    LimitedConcurrencyLevelTaskScheduler lcts = new LimitedConcurrencyLevelTaskScheduler(numberOfAgents);
                    List<Task> tasks = new List<Task>();

                    // Create a TaskFactory and pass it our custom scheduler. 
                    TaskFactory factory = new TaskFactory(lcts);
                    CancellationTokenSource cts = new CancellationTokenSource();
                    int tCtr = 0;
                    while (true)
                    {
                        DocumentContext context = new DocumentContext(); ;
                        if (!DocumentPipeline.TryDequeue(out context))
                            break;
                        int iteration = tCtr;
                        string agentName = "Agent-" + (tCtr + 1).ToString();
                        Action<DocumentContext> action = (c) => { parseDocuments(c); };
                        Task t = factory.StartNew(x=> action(context), cts.Token);
                        tasks.Add(t);

                    }
                    //for (int tCtr = 0; tCtr < numberOfAgents; tCtr++)
                    //{
                    //    int iteration = tCtr;
                    //    string agentName = "Agent-" + (tCtr + 1).ToString();
                    //    Task t = factory.StartNew(() => {
                    //        parseDocumentsFromFolder(agentName);
                    //    }, cts.Token);
                    //    tasks.Add(t);
                    //}                    
                    // Wait for the tasks to complete before displaying a completion message.
                    Task.WaitAll(tasks.ToArray());
                    cts.Dispose();
                    Console.WriteLine("\n\nSuccessful completion.");




                    //Task.WaitAll(workers);
                    //Parallel.For(0, numberOfAgents, async (n) =>
                    //{
                    //    string agentName = "Agent-" + (n + 1).ToString();
                    //    await parseDocumentsFromFolder(agentName);
                    //});

                    //    List<Thread> threads = new List<Thread>(numberOfAgents);
                    //for (int i = 0; i < numberOfAgents; i++)
                    //{
                    //    string agentName = "Agent-" + (i + 1).ToString();
                    //    threads.Add(new Thread(new ThreadStart(() => { parseDocumentsFromFolder(agentName); })));
                    //    threads[i].Start();

                    //}
                    //for (int i = 0; i < numberOfAgents; i++)
                    //{

                    //    threads[i].Join();

                    //}

                    //Task.WaitAll(taskArray);

                }
                else if (MultiAgent == "D")
                {
                    parseDocumentsFromFolder("Agent_1");
                }
                DateTime finish = DateTime.Now;
                Console.ForegroundColor = ConsoleColor.Green;
                var tm = (finish - start);                
                Console.WriteLine(string.Format("{0}\tThe Parsing process is done,The Total time of the processing is {1}H:{2}Min:{3}Sec", finish.ToString("HH:mm:ss"),tm.Hours,tm.Minutes,tm.Seconds));
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadLine();
            }
        }
        static void parseDocumentsFromQueue()
        {
            while(true)
            {
                var documentMetta = DocumentPipeline.Dequeue();
                if (documentMetta == null)
                    return;
                DateTime start = DateTime.Now;
                Console.WriteLine(start.ToString("HH:mm:ss")+ "   Startig ...."+documentMetta.DocumentPath);
                var repositoryKeywords = KeywordsManager.GetKeyWords(documentMetta.RepositoryId);
                documentMetta.KeywordsOptions = repositoryKeywords.Where(e => e.Type == "KO").ToDictionary(key => key.Stemming, k => k.ParentId.HasValue ? repositoryKeywords.FirstOrDefault(f => f.Id == k.ParentId).Stemming : k.Stemming);

                documentMetta.ExpressionsOptions = repositoryKeywords.Where(e => e.Type == "TO").ToDictionary(key => key.Stemming, k => k.ParentId.HasValue ? repositoryKeywords.FirstOrDefault(f => f.Id == k.ParentId).Stemming : k.Stemming);

                //documentMetta.DocumentPath = "c:\\testfile.pdf";
                ParsingResult parsingResult = new ParsingResult(documentMetta);
                Document document = new Document(documentMetta);
                try
                {
                    document = Parser.Parse(documentMetta);
                    parsingResult.PagesCount = document.PagesCount;
                    parsingResult.WordsInDocument = document.WordsInDocument;
                    int matchKeywordCount = 0;
                    var requieredKeywords = repositoryKeywords.Where(e => e.Type.Trim() == "K").ToList();
                    var packages = document.ExtractedKeywords.Join(requieredKeywords, e => e.StemTerm, d => d.Stemming, (e, d) => new { extratedTermPackage = e, keyword = d }).Select(e => new Tuple<Entities.Model.Keyword, ExtratedTermPackage>(e.keyword, e.extratedTermPackage));

                    if (packages != null)
                    {
                        parsingResult.listofExtractedPackages.AddRange(packages);
                        matchKeywordCount = packages.Count();

                        //packages.forea
                        //foreach(var package in packages)
                        //{
                        //    parsingResult.listofExtractedPackages.Add(new Tuple<Entities.Model.Keyword, ExtratedTermPackage>(package.keyword, package.extratedTermPackage));
                        //    matchKeywordCount++;
                        //}

                    }
                    if (matchKeywordCount == 0)
                    {
                        parsingResult.CoordinationFactor = 0;
                        parsingResult.RelevenceScore = 0;
                    }
                    else
                    {
                        parsingResult.CoordinationFactor = (double)matchKeywordCount / (double)requieredKeywords.Count();
                        parsingResult.RelevenceScore = parsingResult.listofExtractedPackages.Sum(e => e.Item2.TermFequancyFactor) * parsingResult.CoordinationFactor;
                    }
                    //foreach (var keyword in requieredKeywords)
                    //{
                    //    //var packages = document.ExtractedKeywords.Join(requieredKeywords, e => e.StemTerm, d => d.Stemming, (e, d) => new { extratedTermPackage = e, keyword = d }).Select(e => new Tuple<Entities.Model.Keyword, ExtratedTermPackage>(e.keyword, e.extratedTermPackage)) ;

                    //    //if (packages != null)
                    //    //{
                    //    //    parsingResult.listofExtractedPackages.AddRange(packages);
                    //    //    matchKeywordCount = packages.Count();

                    //    //    //packages.forea
                    //    //    //foreach(var package in packages)
                    //    //    //{
                    //    //    //    parsingResult.listofExtractedPackages.Add(new Tuple<Entities.Model.Keyword, ExtratedTermPackage>(package.keyword, package.extratedTermPackage));
                    //    //    //    matchKeywordCount++;
                    //    //    //}

                    //    //}
                    //    //if(matchKeywordCount==0)
                    //    //{
                    //    //    parsingResult.CoordinationFactor = 0;
                    //    //    parsingResult.RelevenceScore = 0;
                    //    //}
                    //    //else
                    //    //{
                    //    //    parsingResult.CoordinationFactor = requieredKeywords.Count() / matchKeywordCount;
                    //    //    parsingResult.RelevenceScore = parsingResult.listofExtractedPackages.Sum(e => e.Item2.TermFequancyFactor) * parsingResult.CoordinationFactor;
                    //    //}                                  
                    //}
                    var listofExtractedExpressions = repositoryKeywords.Where(e => e.Type.Trim() == "T").Join(document.ExtractedExpressions, e => e.Stemming, exp => exp.Key, (e, d) => new { term = e, expressionCount = d.Value }).Select(e => new Tuple<Entities.Model.Keyword, int>(e.term, e.expressionCount));
                    if (listofExtractedExpressions != null)
                    {
                        parsingResult.listofExtractedExpressions.AddRange(listofExtractedExpressions);

                    }
                    //foreach (var term in repositoryKeywords.Where(e => e.Type.Trim() == "T"))
                    //{
                    //    int expressionCount = 0;
                    //    if (document.ExtractedExpressions.TryGetValue(term.Stemming, out expressionCount))
                    //    {
                    //        parsingResult.listofExtractedExpressions.Add(new Tuple<Entities.Model.Keyword, int>(term, expressionCount));
                    //    }
                    //}
                    //parsingResult.top20Keyword = document.ExtractedKeywords.Take(20).ToList();
                    //parsingResult.top20Expressions = document.ExtractedExpressions.Take(20).ToList();
                }
                catch (Exception ex)
                {
                    parsingResult.ErrorMessage = ex.Message;
                }
                try
                {
                    DBManager.Save(parsingResult);
                }
                catch(Exception ex2)
                {
                    Console.WriteLine(document.context.DocumentPath + "......... Can't be saved");
                }
               
                DateTime finish = DateTime.Now;
                Console.WriteLine(finish.ToString("HH:mm:ss") + "   Finishing ...." + documentMetta.DocumentPath+"        ..... Total Min:" +(finish-start).TotalMinutes.ToString() );
            }
           
        }
        static void parseDocumentsFromFolder()
        {
            while (true)
            {
                var documentMetta = DocumentPipeline.Dequeue();
                if (documentMetta == null)
                    return;
                DateTime start = DateTime.Now;
                Console.WriteLine(start.ToString("HH:mm:ss") + "   Startig ...." + documentMetta.DocumentPath);
              
                Document document = new Document(documentMetta);
                try
                {
                    document = Parser.Parse(documentMetta);
                               
                  
                }
                catch (Exception ex)
                {
                    Console.WriteLine(document.context.DocumentPath + "......... Erorr:\n"+ex.Message);
                    continue;
                }

                try
                {
                    string output =Newtonsoft.Json.JsonConvert.SerializeObject(document,Newtonsoft.Json.Formatting.Indented);
                    File.WriteAllText(documentMetta.DocumentPath + ".json", output);                    
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(document.context.DocumentPath + "......... Can't be saved");
                }

                DateTime finish = DateTime.Now;
                Console.WriteLine(finish.ToString("HH:mm:ss") + "   Finishing ...." + documentMetta.DocumentPath + "        ..... Total Min:" + (finish - start).TotalMinutes.ToString());
            }

        }
        static void parseDocumentsFromFolder(string agentName)
        {
            
            DocumentContext documentMetta = null;
            DateTime start;
            DateTime finish;
            while (DocumentPipeline.TryDequeue(out documentMetta))
            {                                
               start = DateTime.Now;               
                Console.WriteLine(string.Format("** {0}\t {1} has gotten '{2}' document from MAC and will start the parsing process", start.ToString("HH:mm:ss"),agentName,documentMetta.DocumentName));              
                using (Document document = new Document(documentMetta))
                {
                    try
                    {
                        var doc2 = Parser.Parse(documentMetta);
                        Thread.Sleep(sleep);
                        if (ExportResults)
                        {
                            string output = Newtonsoft.Json.JsonConvert.SerializeObject(doc2, Newtonsoft.Json.Formatting.Indented);
                            File.WriteAllText(documentMetta.DocumentPath + ".json", output);
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(document.context.DocumentPath + "......... Erorr:\n" + ex.Message);
                        continue;
                    }
                }
                

                finish = DateTime.Now;
                Console.WriteLine(string.Format("** {0}\t {1} has finished parsing '{2}' document and the results will be handed over to DB Agent", finish.ToString("HH:mm:ss"), agentName, documentMetta.DocumentName));
                                
            }

        }
        static void parseDocuments(DocumentContext documentMetta)
        {
            string agentName = string.Format("Agent_{0}", Thread.CurrentThread.ManagedThreadId);

            DateTime start;
            DateTime finish;
            start = DateTime.Now;
            Console.WriteLine(string.Format("** {0}\t {1} has gotten '{2}' document from MAC and will start the parsing process", start.ToString("HH:mm:ss"), agentName, documentMetta.DocumentName));
            using (Document document = new Document(documentMetta))
            {
                try
                {
                    var doc2=Parser.Parse(documentMetta);
                    Thread.Sleep(sleep);
                    if(ExportResults)
                    {
                        string output = Newtonsoft.Json.JsonConvert.SerializeObject(doc2, Newtonsoft.Json.Formatting.Indented);
                        File.WriteAllText(documentMetta.DocumentPath + ".json", output);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(document.context.DocumentPath + "......... Erorr:\n" + ex.Message);
                }
            }

            finish = DateTime.Now;
            Console.WriteLine(string.Format("** {0}\t {1} has finished parsing '{2}' document and the results will be handed over to DB Agent", finish.ToString("HH:mm:ss"), agentName, documentMetta.DocumentName));
        }
        static void parseDocumentsFromFolder(string agentName,List<DocumentContext> docs)
        {
            foreach (var documentMetta in docs)
            {
               
                DateTime start = DateTime.Now;
                Console.WriteLine(string.Format("** {0}\t {1} has gotten '{2}' document from MAC and will start the parsing process\n", start.ToString("HH:mm:ss"), agentName, documentMetta.DocumentName));

                using (Document document = new Document(documentMetta))
                {
                    try
                    {
                        Parser.Parse(documentMetta);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(document.context.DocumentPath + "......... Erorr:\n" + ex.Message);
                        continue;
                    }
                }

                DateTime finish = DateTime.Now;
                Console.WriteLine(string.Format("** {0}\t {1} has finished parsing '{2}' document and the results will be handed over to DB Agent\n", finish.ToString("HH:mm:ss"), agentName, documentMetta.DocumentName));
              
            }

        }
    }
}
