﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities;
using System.Data.SqlClient;
using SystematicLR.Common;
namespace SystematicLR.AnalyzerAgent.Helpers
{
    public class DBManager
    {
        const string DeleteExpressionSql = "delete from dbo.ExtractedExpressions where referenceId=@ReferenceId;";

        const string insertExpresstionsSql = "exec [dbo].[ins_ExtractedExpressions] @ReferenceId ,@Expression{0} ,null,@Count{0},@Ordering{0};";
        
        const string insertAnalyzedReference = "exec dbo.ins_AnalyzedReference @ReferenceId,@PageCounts, @WordsCount,@CoordinationFactor,@RelevanceScore";
        const string insertExtractedTerms = "exec dbo.ins_ExtractedTerms @ReferenceId ,@Term{0} ,@TF{0},@FF{0},@TFBMTF{0},@Count{0},0 ";

        const string DeleteExtractedTermsSql = "delete from dbo.ExtractedTerms where referenceId=@ReferenceId;";

        const string FinishAnalysinReferenceSQL = "exec FinishAnalysingReference @ReferenceId,@Error";
        public static bool Save(ParsingResult results)
        {
            StringBuilder commandbuilder = new StringBuilder();
            //commandbuilder.AppendLine("begin transaction");
            List<SqlParameter> prams = new List<SqlParameter>();
            prams.Add(new SqlParameter() { ParameterName = "@ReferenceId", SqlDbType = System.Data.SqlDbType.Int, Value = results.context.DocumentId });

            if (!string.IsNullOrEmpty(results.ErrorMessage))
            {
                commandbuilder.AppendLine(FinishAnalysinReferenceSQL);
                prams.Add(new SqlParameter() { ParameterName = "@Error", SqlDbType = System.Data.SqlDbType.VarChar, Value = results.ErrorMessage });

            }
            else
            {
                int i = 0;
                commandbuilder.AppendLine(DeleteExpressionSql);
                commandbuilder.AppendLine(DeleteExtractedTermsSql);
                commandbuilder.AppendLine(insertAnalyzedReference);

                prams.Add(new SqlParameter() { ParameterName = "@PageCounts", SqlDbType = System.Data.SqlDbType.Int, Value = results.PagesCount });
                prams.Add(new SqlParameter() { ParameterName = "@WordsCount", SqlDbType = System.Data.SqlDbType.Int, Value = results.WordsInDocument });
                prams.Add(new SqlParameter() { ParameterName = "@CoordinationFactor", SqlDbType = System.Data.SqlDbType.Decimal, Value = results.CoordinationFactor });
                prams.Add(new SqlParameter() { ParameterName = "@RelevanceScore", SqlDbType = System.Data.SqlDbType.Float, Value = results.RelevenceScore });                
                    
                prams.Add(new SqlParameter() { ParameterName = "@Error", SqlDbType = System.Data.SqlDbType.VarChar, Value = DBNull.Value });                

                foreach (var item in results.listofExtractedExpressions.OrderByDescending(e => e.Item2))
                {
                    commandbuilder.AppendLine(string.Format(insertExpresstionsSql, i));
                    prams.Add(new SqlParameter() { ParameterName = "@Expression" + i.ToString(), SqlDbType = System.Data.SqlDbType.VarChar, Value = item.Item1.Stemming });
                    prams.Add(new SqlParameter() { ParameterName = "@Count" + i.ToString(), SqlDbType = System.Data.SqlDbType.Int, Value = item.Item2 });
                    prams.Add(new SqlParameter() { ParameterName = "@Ordering" + i.ToString(), SqlDbType = System.Data.SqlDbType.Int, Value = i + 1 });
                    i++;
                }

                
                foreach (var item in results.listofExtractedPackages.OrderByDescending(e => e.Item2.TermFequancyBaseMaxTermFactor))
                {
                    commandbuilder.AppendLine(string.Format(insertExtractedTerms, i));

                    prams.Add(new SqlParameter() { ParameterName = "@Term" + i.ToString(), SqlDbType = System.Data.SqlDbType.VarChar, Value = item.Item1.Stemming });
                    prams.Add(new SqlParameter() { ParameterName = "@FF" + i.ToString(), SqlDbType = System.Data.SqlDbType.Decimal, Value = item.Item2.FontFactor });
                    prams.Add(new SqlParameter() { ParameterName = "@Count" + i.ToString(), SqlDbType = System.Data.SqlDbType.Int, Value = item.Item2.TermCount });
                    prams.Add(new SqlParameter() { ParameterName = "@TF" + i.ToString(), SqlDbType = System.Data.SqlDbType.Decimal, Value = item.Item2.TermFequancyFactor });
                    prams.Add(new SqlParameter() { ParameterName = "@TFBMTF" + i.ToString(), SqlDbType = System.Data.SqlDbType.Decimal, Value = item.Item2.TermFequancyBaseMaxTermFactor });

                    i++;
                }
            }


            commandbuilder.AppendLine(FinishAnalysinReferenceSQL);
            
            int affected=new SystematicLR.Entities.Model.SystematicLREntities().Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.EnsureTransaction,commandbuilder.ToString(),prams.ToArray());
            results = null;
            commandbuilder = null;
            prams = null;
            System.GC.Collect();
            return true;
        }




    }
}
