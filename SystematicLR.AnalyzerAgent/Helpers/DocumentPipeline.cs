﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities.Model;
using SystematicLR.Entities;
using SystematicLR.Common;
using System.IO;
namespace SystematicLR.AnalyzerAgent.Helpers
{
    public static class DocumentPipeline
    {
        private static string prefixUpload;
        public static Queue<DocumentContext> Documents;
        private static object lockObject=new object();
        public static int FillDocumentPipeline(string DBRootPath=null)
        {
            prefixUpload = DBRootPath;
            Documents = new Queue<DocumentContext>(DBServices.GetAnalyzeQ().Select(e=> Convert(e)));
            return Documents.Count;
        }
        public static int FillDocumentPipeline(DirectoryInfo Folder)
        {
            var files=Folder.GetFiles("*.pdf");
            Documents = new Queue<DocumentContext>();
            
            foreach (var file in files)
            {
                Documents.Enqueue(new DocumentContext() { DocumentId = Documents.Count + 1, DcoumentRef = Guid.NewGuid(),DocumentPath=file.FullName,DocumentName=file.Name,RepositoryId=0 });
            }
            
            return Documents.Count;
        }
        private static DocumentContext Convert(GetAnalyzeQ_Result documentMetta)
        {
            if (documentMetta.DocumentPath.StartsWith("Uploads"))
            {
                documentMetta.DocumentPath = prefixUpload + documentMetta.DocumentPath;
            }
            DocumentContext context = new DocumentContext()
            {
                Abstract = documentMetta.Abstract,
                DcoumentRef = documentMetta.ReferenceGUID,
                DocumentId = documentMetta.ReferenceId,
                DocumentName = documentMetta.Title,
                DocumentPath = documentMetta.DocumentPath,
                Keywords = documentMetta.Keywords,
                RepositoryId = documentMetta.RepositoryId,
                QId=documentMetta.QId              
            };
            return context;
        }
        public static DocumentContext Dequeue()
        {
            DocumentContext selected;
            lock (lockObject)
            {
                if (Documents.Count == 0)
                    return null;
                selected=Documents.Dequeue();
            }
            return selected;          
        }
        public static bool TryDequeue(out DocumentContext selected)
        {
            selected = null;
           
            lock (lockObject)
            {
                if (Documents.Count == 0)
                    return false;
                selected = Documents.Dequeue();
            }
            return true ;
        }

    }
}
