﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Common;
namespace SystematicLR.AnalyzerAgent.Helpers
{
    public class ParsingResult
    {
        public int PagesCount { set; get; }
        public int WordsInDocument { set; get; }
        public List<Tuple<Entities.Model.Keyword, ExtratedTermPackage>> listofExtractedPackages { set; get; }
        public List<Tuple<Entities.Model.Keyword, int>> listofExtractedExpressions { set; get; }
        public List<ExtratedTermPackage> top20Keyword { set; get; }
        public List<KeyValuePair<string,int>> top20Expressions { set; get; }
        public double CoordinationFactor { set; get; }
        public double RelevenceScore { set; get; }
        public DocumentContext context { set; get; }   
        public string ErrorMessage { set; get; }
        public ParsingResult(DocumentContext context)
        {
            listofExtractedPackages = new List<Tuple<Entities.Model.Keyword, ExtratedTermPackage>>();
            listofExtractedExpressions = new List<Tuple<Entities.Model.Keyword, int>>();
            top20Keyword = new List<ExtratedTermPackage>(20);
            top20Expressions = new List<KeyValuePair<string, int>>(20);
            this.context = context;
        }

    }
}
