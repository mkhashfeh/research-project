﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities.Model;
using SystematicLR.Entities;

namespace SystematicLR.AnalyzerAgent.Helpers
{
    public static class KeywordsManager
    {
       static Dictionary<int, List<Keyword>> keywords = new Dictionary<int, List<Keyword>>();
        static object locker=new object();
        public static List<Keyword> GetKeyWords(int repositoryId)
        {
            lock (locker)
            {
                if (!keywords.ContainsKey(repositoryId))
                    keywords.Add(repositoryId, DBServices.GetKeywords(repositoryId));
            }           
            return keywords[repositoryId];

        }

    }
}
