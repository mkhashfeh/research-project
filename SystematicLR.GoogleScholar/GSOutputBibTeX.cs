﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.GoogleScholar
{
    public class GSOutputBibTeX
    {
        public string IssueYear { set; get; }
        public string Title { set; get; }
        public string Publisher { set; get; }
        public string Type { set; get; }
        public string Authers { set; get; }
        private string GetFirstAvailableValue(params string[] varibles)
        {
            return varibles.FirstOrDefault(e => !string.IsNullOrEmpty(e));
        }
        public GSOutputBibTeX(BibTeXLibrary.BibEntry entry)
        {            
            var entryType = entry.GetEntryType();
            switch (entryType)
            {
                case BibTeXLibrary.EntryType.Article:                    
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = GetFirstAvailableValue(entry.Publisher,entry.Organization, entry.Journal);
                    this.Authers = entry.Author;
                    this.Type = entry.Type;
                    break;
                case BibTeXLibrary.EntryType.Book:
                case BibTeXLibrary.EntryType.Booklet:
                case BibTeXLibrary.EntryType.InBook:
                case BibTeXLibrary.EntryType.InCollection:
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = entry.Publisher;
                    this.Authers = entry.Author;
                    this.Type = "Book";
                    break;                                  
                case BibTeXLibrary.EntryType.Conference:                    
                case BibTeXLibrary.EntryType.InProceedings:
                case BibTeXLibrary.EntryType.Proceedings:
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = GetFirstAvailableValue(entry.Publisher, entry.Organization, entry.Booktitle);
                    this.Authers = entry.Author;
                    this.Type = "Conference";
                    break;
                case BibTeXLibrary.EntryType.Manual:
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = entry.Organization;
                    this.Authers = entry.Author;
                    this.Type = "Technical";
                    break;
                case BibTeXLibrary.EntryType.PhDThesis:
                case BibTeXLibrary.EntryType.Mastersthesis:
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = entry.School;
                    this.Authers = entry.Author;
                    this.Type = "Thesis";
                    break;
                case BibTeXLibrary.EntryType.Misc:
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;                    
                    this.Authers = entry.Author;
                    break;                                    
                case BibTeXLibrary.EntryType.TechReport:                    
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;
                    this.Publisher = entry.Institution;
                    this.Authers = entry.Author;
                    this.Type = "Technical";
                    break;
                case BibTeXLibrary.EntryType.Unpublished:                    
                    this.IssueYear = entry.Year;
                    this.Title = entry.Title;                    
                    this.Authers = entry.Author;
                    this.Type = "Unpublished";
                    break;
                default:
                    break;
            }           
        }
    }
}
