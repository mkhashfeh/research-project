﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.GoogleScholar
{
    public class GSOutputCitation
    {
        public string APA { set; get; }
        public string Chicago { set; get; }
        public string Harvard { set; get; }
        public string MLA { set; get; }
        public string Vancouver { set; get; }
        public string BibTeXURL { set; get; }
        public GSOutputCitation(Citation citation)
        {
            foreach(var cite in citation.Cites)
            {
                cite.CiteText = ParsingHelper.StripHTML(cite.CiteText);
                switch (cite.CiteType)
                {
                    case "APA":
                        APA = cite.CiteText; break;
                    case "Chicago":
                        Chicago = cite.CiteText; break;
                    case "Harvard":
                        Harvard = cite.CiteText; break;
                    case "MLA":
                        MLA = cite.CiteText; break;
                    case "Vancouver":
                        Vancouver = cite.CiteText; break;
                    default:
                        break;
                }
            }
            if(citation.bibliographies!=null)
                this.BibTeXURL= citation.bibliographies.FirstOrDefault(e => e.Name == "BibTeX").URL;           
        }
    }
}
