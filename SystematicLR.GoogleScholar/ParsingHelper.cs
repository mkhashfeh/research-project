﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SystematicLR.GoogleScholar
{
    internal class ParsingHelper
    {
        public static int ParseInt(string value, int defValue=0)
        {
            if (value == null)
                return defValue;
            int temp;
            if (int.TryParse(value.Trim(), out temp))
                return temp;
            return defValue;
        }
        public static string StripHTML(string input)
        {
            input = HttpUtility.HtmlDecode(input);
            return System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", String.Empty);
        }

    }
}
