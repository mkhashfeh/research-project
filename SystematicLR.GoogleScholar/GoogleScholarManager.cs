﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.IO;
namespace SystematicLR.GoogleScholar
{
    public class GoogleScholarManager
    {
        private static string CallGoogleScholar(string URL)
        {
           
     
            var client = HttpWebRequest.Create(URL);
            client.ContentType = "application/x-www-form-urlencoded";

            var res = client.GetResponse();
            var encoding = System.Text.ASCIIEncoding.ASCII;
            
            string responseText = null;
            using (var reader = new System.IO.StreamReader(res.GetResponseStream(),encoding))
            {
                responseText = reader.ReadToEnd();
            }
            return responseText;
        }
        public static List<GSOutputArticle> SearchArticle(string searchtext)
        {
            List<GSOutputArticle> outputs = new List<GSOutputArticle>();
            var URL = string.Format("https://scholar.google.com/scholar?oi=gsb95&q={0}&output=gsb&hl=en", searchtext);

            //var GSJson =System.IO.File.ReadAllText("C:\\EizFind Logs\\jsonGoogle.txt");
            var GSJson = CallGoogleScholar(URL);

            Articles articles= Articles.FromJson(GSJson);
            if (articles == null || articles.articles==null)
                return null;
            foreach(var art in articles.articles)
            {
                outputs.Add(new GSOutputArticle(art));               
            }
            return outputs;
        }
        public static GSOutputCitation SearchCitation(string CitationId)
        {            
            var URL = string.Format("https://scholar.google.com/scholar?q=info:{0}:scholar.google.com/&output=gsb-cite&hl=en", CitationId);
            var GSJson = CallGoogleScholar(URL);
            //var GSJson = System.IO.File.ReadAllText("C:\\EizFind Logs\\Citation.txt");
            Citation articles = Citation.FromJson(GSJson);
            if (articles == null || articles.Cites == null)
                return null;
            return new GSOutputCitation(articles);                       
        }
        public static GSOutputBibTeX GetBibTeXData(string URL)
        {
            var outputText=CallGoogleScholar(URL);
            if (string.IsNullOrEmpty(outputText))
                return null;
            var results = BibTeXLibrary.BibParser.Parse(new StringReader(outputText));
            if (results == null)
                return null;
            var entry= results.FirstOrDefault();
            return new GSOutputBibTeX(entry);

        }


    }
}
