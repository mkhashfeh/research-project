﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var citation = Citation.FromJson(jsonString);

namespace SystematicLR.GoogleScholar
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Citation
    {
        [JsonProperty("l")]
        public Cite[] Cites { get; set; }

        [JsonProperty("i")]
        public Bibliographie[] bibliographies { get; set; }
    }

    public partial class Bibliographie
    {
        [JsonProperty("l")]
        public string Name { get; set; }

        [JsonProperty("u")]
        public string URL { get; set; }
    }

    public partial class Cite
    {
        [JsonProperty("l")]
        public string CiteType { get; set; }

        [JsonProperty("h")]
        public string CiteText { get; set; }
    }

    public partial class Citation
    {
        public static Citation FromJson(string json) => JsonConvert.DeserializeObject<Citation>(json, SystematicLR.GoogleScholar.Converter.Settings);
    }

   
}
