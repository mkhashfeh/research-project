﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace SystematicLR.GoogleScholar
{
    public class GSOutputArticle
    {
        public GSOutputArticle(Article art)
        {
            this.OrginalArticle = art;
            if (art == null)
                return;
            if(art.Abstract!=null)
                this.Abstarct = ParsingHelper.StripHTML(HttpUtility.HtmlDecode(art.Abstract)).Replace("?…", "").Trim();
            if (art.Title != null)
                this.Title = ParsingHelper.StripHTML(HttpUtility.HtmlDecode(art.Title));
            if (!string.IsNullOrEmpty(art.URL))
            {
                Uri url = new Uri(art.URL);
                this.MainURL = HttpUtility.ParseQueryString(url.Query).Get("url");
            }
            if(art.ReferenceFile!=null)
                this.ReferenceType = art.ReferenceFile;
            if(art.AuthersSection!=null)
            {
                var AuthersSectionSplits = ParsingHelper.StripHTML(HttpUtility.HtmlDecode(art.AuthersSection)).Split('-');
                if(AuthersSectionSplits.Length>0)
                    this.Authers = AuthersSectionSplits[0].Replace("…", "").Replace("?","").Trim();
                if (AuthersSectionSplits.Length > 1)
                {
                    var AuthersSectionSplits2 = AuthersSectionSplits[1].Split(',');
                    if(AuthersSectionSplits2.Length>0)
                        this.Publisher = AuthersSectionSplits2[0].Replace("…", "").Replace("?", "").Trim();
                    if (AuthersSectionSplits2.Length > 1)
                        this.Year = ParsingHelper.ParseInt(AuthersSectionSplits2[1].Replace("…", "").Replace(",", "").Trim());
                }
                    
                
            }
            if (art.ExtraInfo != null)
            {
                if (art.ExtraInfo.Citedby != null)
                {
                    this.CitedBy = ParsingHelper.ParseInt(art.ExtraInfo.Citedby.Label.Replace("Cited by", "").Trim());
                    this.CitedByLink = "https://scholar.google.com/" + art.ExtraInfo.Citedby.URL;
                }
                if (art.ExtraInfo.Related != null)
                {
                    this.RelatedArticlesLink = "https://scholar.google.com/" + art.ExtraInfo.Related.URL;
                }
                if (art.ExtraInfo.Versions != null)
                {
                    this.Versions = ParsingHelper.ParseInt(art.ExtraInfo.Versions.Label.ToLower().Replace("all", "").Replace("versions", "").Trim(),1);
                    this.VersionLink = "https://scholar.google.com/" + art.ExtraInfo.Versions.URL;
                }
                if (art.ExtraInfo.CiteId != null)
                {
                    this.CiteId = art.ExtraInfo.CiteId.URL.Substring(2);
                }
                if (art.ExtraInfo.file != null)
                {
                    var FileTypeSplits = art.ExtraInfo.file.Label.Split(" ".ToCharArray());
                    if(FileTypeSplits.Length>0)
                    {
                        this.FileType = FileTypeSplits[0].Replace("[", "").Replace("]", "").Trim();
                        this.HostSite = string.Join(" ", FileTypeSplits.Skip(1));
                        
                    }
                    this.DownloadURL = HttpUtility.ParseQueryString(new Uri(art.ExtraInfo.file.URL).Query).Get("url");
                }
                //if (!this.CitedBy.HasValue) this.CitedBy = 0;
                //if (!this.Versions.HasValue) this.Versions = 0;
                if(string.IsNullOrEmpty(this.HostSite))
                {
                    if(!string.IsNullOrEmpty(this.MainURL))
                        this.HostSite = new Uri(this.MainURL).Host.ToLower().Replace("www.","");
                }


            }
            
        }

        public Article OrginalArticle { private set; get; }
        public string Authers { set; get; }
        public string CiteId { set; get; }
        public string Title { set; get; }
        public string ReferenceType { set; get; }
        public string MainURL { set; get; }
        public int Year { set; get; }
        public string Publisher { set; get; }
        public string Abstarct { set; get; }
        public int CitedBy { set; get; }
        public string RelatedArticlesLink { set; get; }
        public string CitedByLink { set; get; }
        public int Versions { set; get; }
        public string VersionLink { set; get; }
        public string FileType { set; get; }
        public string DownloadURL { set; get; }        
        public string HostSite { set; get; }


    }
}
