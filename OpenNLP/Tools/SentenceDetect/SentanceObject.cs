﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenNLP.Tools.SentenceDetect
{
    public class SentanceObject
    {
        public int StartIndex { set; get; }
        public int Endindix { get { return StartIndex + text.Length; } }
        public string text { set; get; }
        public override string ToString()
        {
            return string.Format("From[{0}].... To[{1}]", StartIndex, Endindix);
        }
    }
}
