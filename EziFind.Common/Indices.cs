﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public class Indices
    {
        public static Indices GetInstance() { return new Indices(); }
        public static Indices GetInstance(int StartIndex, int EndIndex) { return new Indices(StartIndex, EndIndex); }
        public Indices(int StartIndex, int EndIndex)
        {
            this.StartIndex = StartIndex;
            this.EndIndex = EndIndex;
        }
        public Indices()
        {

        }

        public int StartIndex { set; get; }
        public int EndIndex { set; get; }
    }
}
