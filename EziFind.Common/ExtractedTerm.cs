﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace SystematicLR.Common
{
    public class ExtractedTerm
    {
        public string StemWord { get; private set; }
        public bool isStopWord { get; private set; }
        public bool isValidTerm { get; private set; }
        public TermFont Font { set; get; }
        public int Position;
        public Document ParentDocument { private set; get; }
        public ExtractedTerm(Document parentDocument, string term, int Position, Dictionary<string, string> KeywordsMap = null,bool withStem=true)
        {
            ParentDocument = parentDocument;
            isStopWord = Utilities.IsStopWord(term);
            StemWord = term;
            if(withStem)
                StemWord = Utilities.StemTerm(term);
            if (KeywordsMap != null)
            {
                string temp;
                if (KeywordsMap.TryGetValue(StemWord, out temp))
                    StemWord = temp;
            }
            isValidTerm = Utilities.IsValidTerm(term);
            this.Position = Position;
        }
    }
    public class ExtratedTermPackage
    {
        public ExtratedTermPackage(string stemTerm)
        {
            this.terms = new List<ExtractedTerm>();
            StemTerm = stemTerm;
        }
        public string StemTerm { private set; get; }
        [JsonIgnore]
        public List<ExtractedTerm> terms { set; get; }
        public void Analyze(AnalyzingContext context)
        {
            TermCount = terms.Count;
            this.TermFequancyFactor = Convert.ToDouble(Convert.ToDouble(TermCount) / Convert.ToDouble(context.WordsCount));
            FontFactor = terms.Average(e => context.Fontwieght[e.Font]);
            this.TermFequancyBaseMaxTermFactor = Convert.ToDouble(Convert.ToDouble(TermCount) / Convert.ToDouble(context.MaxTermFrequancy));
            this.TermWeight = FontFactor * TermFequancyFactor;
            this.TermWeightBaseMaxTermFactor = FontFactor * TermFequancyBaseMaxTermFactor;
            terms.Clear();
        }
        public void Add(ExtractedTerm term)
        {
            terms.Add(term);           
        }
        public int TermCount { private set; get; }
        public double TermFequancyFactor { private set; get; }
        public double FontFactor { private set; get; }
        public double TermFequancyBaseMaxTermFactor { private set; get; }
        public double TermWeight { private set; get; }
        public double TermWeightBaseMaxTermFactor { private set; get; }
    }
}
