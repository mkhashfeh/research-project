﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public class Font
    {
        public float FontSize { set; get; }
        public List<Keyword> keywords { set; get; }
        public override bool Equals(object obj)
        {
            
            if(obj != null && obj.GetType().FullName==typeof(Font).FullName)
                return this.FontSize==((Font)obj).FontSize;
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            var hashCode = 352033288;
            hashCode = hashCode * -1521134295 + this.FontSize.GetHashCode();
            return hashCode;
            
        }
    }
    
}
