﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public enum ParagraphTypeEnum
    {
        Normal,
        AbstractParagraph,
        KeywordsParagraph
    }
    public class Paragraph
    {
        public Document document{ set; get; }
        public virtual ParagraphTypeEnum ParagraphType { get { return ParagraphTypeEnum.Normal; } }
        public List<Keyword> keywords { set; get; }
        public string ParagraphText { set; get; }
        public string WordsCount { set; get; }
        public Indices indices { set; get; }

    }
    public class AbstractParagraph : Paragraph
    {
        public override ParagraphTypeEnum ParagraphType
        {
            get
            {
                return ParagraphTypeEnum.AbstractParagraph;
            }
        }
    }
    public class KeywordsParagraph : Paragraph
    {
        public override ParagraphTypeEnum ParagraphType
        {
            get
            {
                return ParagraphTypeEnum.KeywordsParagraph;
            }
        }

    }
}
