﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public class TermFont
    {
        public float FontSize { set; get; }
        public bool Bold { set; get; }
        public override bool Equals(object obj)
        {
            var item = obj as TermFont;
            if (item == null)
                return false;
            return this.FontSize.Equals(item.FontSize) && this.Bold.Equals(item.Bold);
            
        }
        
        public override int GetHashCode()
        {
            return new { FontSize, Bold }.GetHashCode();
            
        }
        public override string ToString()
        {
            return string.Format("FontSize:{0}, IsBold:{1}", FontSize, Bold);

        }
    }

    public class TermsDictionary :IDisposable
    {        
        public TermsDictionary()
        {
             map = new Dictionary<string, ExtratedTermPackage>();
        }
        public void Dispose()
        {
            map.Clear();
        }
        
        public Dictionary<string, ExtratedTermPackage> map { private set; get; }

        public void AddNew(ExtractedTerm term)
        {
            if (!map.ContainsKey(term.StemWord))
                map.Add(term.StemWord, new ExtratedTermPackage(term.StemWord));
            map[term.StemWord].Add(term);                      
        }
        public ExtratedTermPackage GeTermPackage(string stem)
        {
            ExtratedTermPackage temp = null;
            map.TryGetValue(stem, out temp);
            return temp;
        }
        public void Analyze(AnalyzingContext context)
        {           
            foreach(var item in map)
            {
                item.Value.Analyze(context);
            }
        }
    }
    public class FontsDictionary
    {
        public FontsDictionary()
        {
            map = new Dictionary<TermFont, TermsDictionary>();
        }
        public Dictionary<TermFont, TermsDictionary> map { private set; get; }
        public void AddNew(ExtractedTerm term)
        {
            if (!map.ContainsKey(term.Font))
                map.Add(term.Font, new TermsDictionary());
            map[term.Font].AddNew(term);
        }
        public TermsDictionary GetTerms(TermFont font)
        {
            TermsDictionary temp = null;
            map.TryGetValue(font, out temp);
            return temp;

        }
    }
}
