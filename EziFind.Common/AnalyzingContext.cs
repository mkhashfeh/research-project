﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public class AnalyzingContext
    {
        public int WordsCount { set; get; }
        public int MaxTermFrequancy { set; get; }
        public Dictionary<TermFont,double> Fontwieght { set; get; }
    }
}
