﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Net;
using System.IO;
using System.Text;

using System.Text.RegularExpressions;
using System.Data;
using Lucene.Net.Analysis;
using OpenNLP.Tools.Tokenize;
using OpenNLP.Tools.SentenceDetect;
using OpenNLP.Tools.PosTagger;
using OpenNLP.Tools.Chunker;



namespace SystematicLR.Common

{
    public  class Utilities
    {
        //static string mModelPath = @"../../../DocumentParser/Resources/Models/";
        static string mModelPath = @"C:\Users\user\Desktop\Uniten\Resaarch App\Applications\EziFind\DocumentParser\Resources\Models\";
        
        static string stopwordsfile = "stopwords.txt";
        static bool stopwords_loaded = false;        
        static List<string> stopwords = new List<string>();
        static List<string> acronyms = new List<string>();
        static List<string> allcollectiontokens = new List<string>();
        static List<string> vocabulary = new List<string>();
        static List<string> documentids = new List<string>();
        static List<string> allphrases = new List<string>();
        static List<string> querylog = new List<string>();
        static Hashtable invertedlist = new Hashtable(); //global
        static Hashtable idfs = new Hashtable(); //global
        static Hashtable itfs = new Hashtable(); //global
        static Hashtable doc_term_weights_sqr = new Hashtable();
        static Hashtable doc_term_weights = new Hashtable(); //local- for terms in a document
        static Hashtable doc_tokens = new Hashtable();
        static Hashtable doc_tokens_freq = new Hashtable();
        static Hashtable term_vector = new Hashtable();
        static Hashtable max_tok_freq = new Hashtable();
        static Hashtable term_correlation = new Hashtable();
        static Hashtable doc_query = new Hashtable();
        static Hashtable doc_terminology = new Hashtable();
        static Hashtable mle_term_document = new Hashtable();
        static Hashtable mle_term_language = new Hashtable();
        static Hashtable jobdetails = new Hashtable();
        static Dictionary<string, List<int>> termfreq_collection = new Dictionary<string, List<int>>();
        static Analyzer analyzer = new Lucene.Net.Analysis.Standard.StandardAnalyzer();

        static Dictionary<string, Dictionary<string, double>> qe_vectors = new Dictionary<string, Dictionary<string, double>>();

        static EnglishMaximumEntropyTokenizer tokenizer = new EnglishMaximumEntropyTokenizer(mModelPath + "EnglishTok.nbin");
        static EnglishMaximumEntropySentenceDetector sentenceDetector = new EnglishMaximumEntropySentenceDetector(mModelPath + "EnglishSD.nbin");
        static EnglishMaximumEntropyPosTagger posTagger = new EnglishMaximumEntropyPosTagger(mModelPath + "EnglishPOS.nbin");
        static EnglishTreebankChunker chunker = new EnglishTreebankChunker(mModelPath + "EnglishChunk.nbin");

       
       

        const string anchortext_pattern = @"<a.*?href.*?=.*?['""""](?:http:|/)(?:.*?).*?>(?<text>.*?)</a>";

        const string head_content_pattern = @"<head.*?>[\s\S]*?</head>";
        const string style_content_pattern = @"<style[\s\S]*?>[\s\S]*?</style>";
        const string script_content_pattern = @"<script[\s\S]*?>[\s\S]*?</script>";

        

        static readonly object lock1 = new object();
        static readonly object lock_postagger = new object();
        static readonly object loadindex_loc = new object();
        static readonly object loadqevector_loc = new object();

        static Random rnd = new Random();


        #region NLP Functions
        //public static string IsEnglish(string text)
        //{
        //    TranslationClient client = TranslationClient.Create();
        //    var detection = client.DetectLanguage(text);
           
        //        return detection.Language+" "+ detection.Confidence;
            

        //}        
        public static List<string> ExtractNouns(string[] taggedwordscol)
        {
            List<string> noun_words = new List<string>();
            string pos_pattern = @"(?<word>[\S\w.-]+?)(?<pos>/[AVDBPRITNSJWOCZ$]{2,5})";
            foreach (string taggedword in taggedwordscol)
            {
                Match m = Regex.Match(taggedword, pos_pattern, RegexOptions.IgnoreCase);
                if ((m != null) && (m.Success))
                {
                    string pos = m.Groups["pos"].Value.ToUpper().Trim();
                    if ((pos == "/NN") || (pos == "/NNS") || (pos == "/NNP"))
                        noun_words.Add(m.Groups["word"].Value);
                }


            }

            return (noun_words);
        }

        public static List<string> ExtractNounPhrases(string[]tokens, string[] tags)
        {
            EnglishTreebankChunker chunker = new EnglishTreebankChunker(mModelPath + "EnglishChunk.nbin");
            var chunks = chunker.GetChunks(tokens, tags);

            string NP_pattern = @"\[NP(?<phrase>.*?)\]";
            string pos_pattern = @"([\S\w.-]+?)(?<pos>/[AVDBPRITNSJWOCZ$]{2,5})";
            

            List<string> doc_phrases = new List<string>();

            //extract only the NP                
            MatchCollection matches = Regex.Matches(string.Join(" ",chunks.Select(e=>e.ToString()).ToArray()), NP_pattern);
            if ((matches != null) && (matches.Count > 0))
            {
                foreach (Match m in matches)
                {
                    string np_phrase = m.Groups["phrase"].Value.Trim();
                    if (np_phrase.EndsWith("/NN") || np_phrase.EndsWith("/NNS") || np_phrase.EndsWith("/NNP"))
                    {
                        MatchCollection np_phrase_terms = Regex.Matches(np_phrase, pos_pattern);
                        if ((np_phrase_terms != null) && (np_phrase_terms.Count > 0))
                        {
                            StringBuilder termbuffer = new StringBuilder();
                            foreach (Match np_term_match in np_phrase_terms)
                            {
                                string np_term = np_term_match.Value;
                                string np_term_pos = np_term_match.Groups["pos"].Value;
                                if (np_term.EndsWith("/NN") || np_term.EndsWith("/NNS") || np_term.EndsWith("/NNP"))
                                {
                                    string _actual_np_phrase_term = np_term.Replace(np_term_pos, " ");
                                    if (_actual_np_phrase_term.Trim().Length > 2)
                                        termbuffer.Append(_actual_np_phrase_term);
                                }
                            }
                            string _phrase = termbuffer.ToString().Trim();
                            List<string> _phrase_tokens = TokenizeContent(_phrase);
                            StringBuilder tokbuf = new StringBuilder();
                            foreach (string tok in _phrase_tokens)
                            {
                                string norm_tok = tok.ToLower().Trim();

                                if ((norm_tok.Length > 2) && (!stopwords.Contains(norm_tok)))
                                {
                                    tokbuf.Append(norm_tok).Append(" ");

                                    if (!stopwords.Contains(norm_tok))
                                        allcollectiontokens.Add(norm_tok);

                                    if (!vocabulary.Contains(norm_tok) && (!stopwords.Contains(norm_tok)))
                                        vocabulary.Add(norm_tok);

                                }

                            }
                            string _phrase1 = tokbuf.ToString().Trim().ToLower();
                            if (!string.IsNullOrEmpty(_phrase1))
                            {
                                //save it for the current doc
                                doc_phrases.Add(_phrase1);

                                //add to the list of of corpus wide phrases
                                if (!allphrases.Contains(_phrase1))
                                    allphrases.Add(_phrase1);

                            }
                        }
                    }
                }
            }

            return (doc_phrases);
        }
        public static Dictionary<string, int> Getterms(List<string> tokens)
        {
            Dictionary<string,int> results = new Dictionary<string,int>();
            string term = "";
            for (int i = 0; i < tokens.Count; i++)
            {
                if (i + 2 <= tokens.Count)
                {
                    if (tokens[i] == tokens[i + 1])
                        continue;
                    term = tokens[i] + " " + tokens[i + 1];
                    if (!results.ContainsKey(term))
                        results.Add(term, 0);
                    results[term]++;
                }
                if (i + 3 <= tokens.Count)
                {
                    if (tokens[i+2] == tokens[i + 1])
                        continue;
                    term = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
                    if (!results.ContainsKey(term))
                        results.Add(term, 0);
                    results[term]++;
                }
            }
            return results.Where(e => e.Value > 2).OrderByDescending(e=>e.Value).ToDictionary(i => i.Key, i => i.Value);
        }
        public static Dictionary<string, int> Getterms(List<ExtractedTerm> tokens,Dictionary<string,string> ExpressionOptions=null)
        {
            Dictionary<string, int> results = new Dictionary<string, int>();
            string term = "";
            for (int i = 0; i < tokens.Count; i++)
            {
                if (i + 2 <= tokens.Count)
                {
                    if (tokens[i].StemWord == tokens[i + 1].StemWord)
                        continue;
                    term = tokens[i].StemWord + " " + tokens[i + 1].StemWord;
                    if(ExpressionOptions!=null)
                    {
                        if (ExpressionOptions.ContainsKey(term))
                            term = ExpressionOptions[term];
                    }                    
                    if (!results.ContainsKey(term))
                        results.Add(term, 0);
                    results[term]++;
                }
                if (i + 3 <= tokens.Count)
                {
                    if (tokens[i + 2].StemWord == tokens[i + 1].StemWord)
                        continue;
                    term = tokens[i].StemWord + " " + tokens[i + 1].StemWord + " " + tokens[i + 2].StemWord;
                    if (ExpressionOptions != null)
                    {
                        if (ExpressionOptions.ContainsKey(term))
                            term = ExpressionOptions[term];
                    }
                    if (!results.ContainsKey(term))
                        results.Add(term, 0);
                    results[term]++;
                }
            }
            return results.Where(e => e.Value > 2).OrderByDescending(e => e.Value).ToDictionary(i => i.Key, i => i.Value);
        }

        public static List<String> GetNounsFromContent(String _content, bool keepduplicates = false)
        {
            List<string> np = new List<string>();
            string content = _content;
            List<string> doc_terms = new List<string>();
            Dictionary<string, List<int>> termfreq = new Dictionary<string, List<int>>();
            Dictionary<string, double> termweights = new Dictionary<string, double>();
            List<string> tags = new List<string>();
            string[] postags = { };

            string[] sentences = GetSentences(content);
            string[] tokens = TokenizeContent(string.Join(" ", sentences), true).ToArray();

            if (tokens.Length > 0)
                postags = POSTagger(tokens);
           
            for (int i = 0; i < postags.Length; i++)
            {
                if (IsPOSNoun(postags[i]) && !IsStopWord(tokens[i]))
                    np.Add(tokens[i]);
            }

            return (np);
        }

        public static string StemTerm(string _text)
        {
            StringReader stringReader = new StringReader(_text);
            TokenStream tokenStream = analyzer.TokenStream("defaultFieldName", stringReader);
            Lucene.Net.Analysis.PorterStemFilter filter = new Lucene.Net.Analysis.PorterStemFilter(tokenStream);
            Token token = filter.Next();
            string output = "";
            if(token!=null)
            {
                output = token.TermText();
            }
            
            stringReader.Dispose();
            return output.Trim() ;

        }
        public static List<string> SystematicLRTokenize(string _content)
        {

            EnglishMaximumEntropyTokenizer tokenizer = new EnglishMaximumEntropyTokenizer(mModelPath + "EnglishTok.nbin");
            string[] tokens = tokenizer.Tokenize(_content);//TokenizeContent(_content).ToArray(); //
            return tokens.ToList();

        }
        public static string Stem(string text)
        {
            Analyzer analyzer = new Lucene.Net.Analysis.Standard.StandardAnalyzer();
            StringReader stringReader = new StringReader(text);
            TokenStream tokenStream = analyzer.TokenStream("defaultFieldName", stringReader);
            Lucene.Net.Analysis.PorterStemFilter filter = new Lucene.Net.Analysis.PorterStemFilter(tokenStream);
            Token token = filter.Next();

            StringBuilder sb = new StringBuilder();

            while (token != null)
            {
                sb.Append(token.TermText()+" ");
                token = filter.Next();
            }
            stringReader.Dispose();
            return sb.ToString();
            
        }
       

        public static List<String> GetTermsFromContent(String _content, bool keepduplicates = false)
        {
            string content = _content;
            List<string> doc_terms = new List<string>();
            Dictionary<string, List<int>> termfreq = new Dictionary<string, List<int>>();
            Dictionary<string, double> termweights = new Dictionary<string, double>();
            List<string> np = new List<string>();
            string[] postags = { };

            string[] sentences = GetSentences(content);
            StringBuilder _buf_sentences = new StringBuilder();
            foreach (string s in sentences)
                _buf_sentences.Append(s).Append(" ");
            string[] tokens = TokenizeContent(_buf_sentences.ToString(), true).ToArray();

            if (tokens.Length > 0)
                postags = POSTagger(tokens);

            for (int i = 0; i < postags.Length; i++)
            {
                if (IsPOSNoun(postags[i]) && !IsStopWord(postags[i]))
                    np.Add(tokens[i]);
            }



            if (np.Count() > 0)
            {
                //compute the frequency of terms
                termfreq = GetTermFrequencyInContent(np);

                //using a simple weighing scheme - this is essentially indexing on the fly ... doing bare minimum (for lack of fire power) for term weighting
                foreach (string t in termfreq.Keys)
                {
                    double term_weight = ((double)termfreq[t].Sum() / termfreq.Count()) * GetIDF(t);
                    termweights.Add(t, term_weight);
                }

                //this is arbitary - selecting 'all' the top weighed terms
                int topN = termweights.Count();
                var sortedDict = (from entry in termweights where entry.Value > 0.0 orderby entry.Value descending select entry).Take(topN);

                if (!keepduplicates)
                {
                    foreach (KeyValuePair<string, double> kv in sortedDict)
                        doc_terms.Add(kv.Key);
                }
                else
                {
                    foreach (string _t in np)
                        doc_terms.Add(_t);
                }

            }

            return (doc_terms);
        }

        public static string[] GetSentences(string _content)
        {
            string content = _content;
            string[] sentences = { };
            lock (lock1)
            {
                sentences = sentenceDetector.SentenceDetect(content);
            }

            return (sentences);
        }

        public static string[] POSTagger(string[] _tokens)
        {
            string[] tokens = _tokens;
            string[] postags = { };

            lock (lock_postagger)
            {
                postags = posTagger.Tag(tokens);
            }

            return (postags);
        }

        #endregion

        #region Utility Funcitons

        public static List<string> Shuffle(List<string> result)
        {
            //Shuffle the list - may be use Knuth-Fisher-Yates algorithm?             
            //This O(n) algorithm. Since the resultset is pretty small (1 to 20 max) this is an 
            //acceptable performance

            int n = result.Count;
            for (int i = 0; i < n; i++)
            {
                //genreate a random index
                int j = rnd.Next(0, result.Count - 1);
                //swap result[i] <--> result[j]
                string tmp = result[i];
                result[i] = result[j];
                result[j] = tmp;
            }

            return (result);
        }

        public static void LoadStopWords()
        {
            if (!stopwords_loaded)
            {
                try
                {
                    stopwordsfile = mModelPath +"../"+  stopwordsfile;
                    string _stopwords_file_content = ReadFromFile(stopwordsfile);
                    string[] _stopwords_arr = _stopwords_file_content.Split(',');
                    foreach (string sw in _stopwords_arr)
                    {
                        if (!stopwords.Contains(sw.ToLower().Trim()))
                            stopwords.Add(sw.ToLower().Trim());
                    }
                    stopwords_loaded = true;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error:Cann't load StopWords file/n Exception" + ex.Message);
                }

            }

        }


        public static string ReadFromFile(string filename)
        {
            string filecontent = "";
            if (File.Exists(filename))
            {
                TextReader reader = new StreamReader(filename);
                filecontent = reader.ReadToEnd();
                reader.Close();
                reader = null;
            }
            return (filecontent);
        }

        private static bool IsAllUpperCase(string word)
        {
            if (word.ToUpper().GetHashCode() == word.GetHashCode())
                return (true);
            else
                return (false);
        }




        public static string GetFirstNWords(string _input, int _n)
        {
            string input = _input;
            int n = _n;
            string _n_words = string.Empty;
            Regex rx = null;
            Match m = null;

            //first get all the words
            string allwordspattern = @"\w+";
            rx = new Regex(allwordspattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            MatchCollection matches = rx.Matches(input);
            if ((matches != null) && (matches.Count > 0))
            {
                int totalwords = matches.Count;
                if (n > totalwords)
                    n = totalwords;
                string pattern = @"^(\w+\b[\s\S]*?){" + n + "}";
                rx = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                m = rx.Match(input);
                if ((m != null) && (m.Success))
                {
                    _n_words = m.Value.ToString();
                }
            }

            return (_n_words);
        }



        #endregion

        #region Tokenization Functions

        public static List<string> Tokenize(string content)
        {
            List<string> terms = new List<string>();
            string[] words = content.Split(' ');

            foreach (string _term in words)
            {
                string _cleaned_term = _term;
                Match m = Regex.Match(_term, @"\w{2,}");
                if ((m != null) && (m.Success))
                    _cleaned_term = m.Value.ToLower().Trim();
                if (!String.IsNullOrEmpty(_cleaned_term) && !stopwords.Contains(_cleaned_term) && !terms.Contains(_cleaned_term) && (_cleaned_term.Length > 2))
                    terms.Add(_cleaned_term.ToLower().Trim());
            }

            MatchCollection wordscol = Regex.Matches(content, @"\w{2,}");
            foreach (Match m in wordscol)
            {
                string _term = m.Value.Replace(".", "").Replace(",", "").ToLower().Trim();
                if (!terms.Contains(_term) && !stopwords.Contains(_term) && (_term.Length > 2))
                    terms.Add(_term);
            }

            return (terms);
        }
        public static bool IsValidTerm(string _term)
        {
            string p = @"\b[a-zA-Z]{3,}\b";
            Match m = Regex.Match(_term, p);
            return ((m != null) && (m.Success));

        }
        public static List<string> TokenizeContent(string content)
        {
            string p = @"\b[a-zA-Z]{3,}\b";

            List<string> tokens = new List<string>();
            char[] delims = { ' ', '-', '/', '_' };
            string[] _toks = content.Split(delims, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < _toks.Length; i++)
            {
                Regex re1 = new Regex(p);
                MatchCollection _matchcol = re1.Matches(_toks[i]);
                if ((_matchcol != null) && (_matchcol.Count > 0))
                {
                    foreach (Match _match in _matchcol)
                    {
                        if (_match.Success)
                        {
                            string wl = _match.Value.Trim();
                            if (!tokens.Contains(wl.ToLower()) && (!stopwords.Contains(wl.ToLower())))
                                tokens.Add(wl.ToLower());
                        }

                    }
                }
            }

            return (tokens);
        }

        public static List<string> TokenizeContent(string content, bool keepduplicates)
        {
            string p = @"\b[a-zA-Z]{3,}\b";

            List<string> tokens = new List<string>();
            char[] delims = { ' ', '-', '/', '_' };
            string[] _toks = content.Split(delims, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < _toks.Length; i++)
            {
                Regex re1 = new Regex(p);
                MatchCollection _matchcol = re1.Matches(_toks[i]);
                if ((_matchcol != null) && (_matchcol.Count > 0))
                {
                    foreach (Match _match in _matchcol)
                    {
                        if (_match.Success)
                        {
                            string wl = _match.Value.ToLower().Trim();
                            if (!keepduplicates)
                            {
                                if (!tokens.Contains(wl.ToLower()) && (!stopwords.Contains(wl.ToLower())))
                                    tokens.Add(wl);
                            }
                            else
                            {
                                if (!stopwords.Contains(wl.ToLower()))
                                    tokens.Add(wl);
                            }

                        }

                    }
                }
            }

            return (tokens);
        }

        public static List<string> ExtractTokensFromContent(string _content)
        {

            List<string> doc_phrases = new List<string>();
            List<String> doc_tokens = new List<string>();

            EnglishMaximumEntropyTokenizer tokenizer = new EnglishMaximumEntropyTokenizer(mModelPath + "EnglishTok.nbin");
            string[] tokens = tokenizer.Tokenize(_content);//TokenizeContent(_content).ToArray(); //

            EnglishMaximumEntropySentenceDetector sentenceDetector = new EnglishMaximumEntropySentenceDetector(mModelPath + "EnglishSD.nbin");
            string[] sentences = sentenceDetector.SentenceDetect(_content);

            EnglishMaximumEntropyPosTagger posTagger = new EnglishMaximumEntropyPosTagger(mModelPath + "EnglishPOS.nbin");
            string[] tags = posTagger.Tag(tokens);

            EnglishTreebankChunker chunker = new EnglishTreebankChunker(mModelPath + "EnglishChunk.nbin");
            var chunks = chunker.GetChunks(tokens, tags);

            //extract only the NP
            string NP_pattern = @"\[NP(?<phrase>.*?)\]";
            ///Wrong input Mouayad
            MatchCollection matches = Regex.Matches(string.Concat(chunks), NP_pattern);
            if ((matches != null) && (matches.Count > 0))
            {
                foreach (Match m in matches)
                {
                    string np_phrase = m.Groups["phrase"].Value.Trim();
                    if (np_phrase.EndsWith("/NN") || np_phrase.EndsWith("/NNS") || np_phrase.EndsWith("/NNP"))
                    {
                        string pos_pattern = @"([\S\w.-]+?)(?<pos>/[AVDBPRITNSJWOCZ$]{2,5})";
                        MatchCollection np_phrase_terms = Regex.Matches(np_phrase, pos_pattern);
                        if ((np_phrase_terms != null) && (np_phrase_terms.Count > 0))
                        {
                            StringBuilder termbuffer = new StringBuilder();
                            foreach (Match np_term_match in np_phrase_terms)
                            {
                                string np_term = np_term_match.Value;
                                string np_term_pos = np_term_match.Groups["pos"].Value;
                                if (np_term.EndsWith("/NN") || np_term.EndsWith("/NNS") || np_term.EndsWith("/NNP"))
                                {
                                    string _actual_np_phrase_term = np_term.Replace(np_term_pos, " ");
                                    if (_actual_np_phrase_term.Trim().Length > 2)
                                        termbuffer.Append(_actual_np_phrase_term);
                                }
                            }
                            string _phrase = termbuffer.ToString().Trim();
                            List<string> _phrase_tokens = TokenizeContent(_phrase);
                            StringBuilder tokbuf = new StringBuilder();
                            foreach (string tok in _phrase_tokens)
                            {
                                string norm_tok = tok.ToLower().Trim();

                                if ((norm_tok.Length > 2) && (!stopwords.Contains(norm_tok)))
                                {
                                    tokbuf.Append(norm_tok).Append(" ");
                                    if (!doc_tokens.Contains(norm_tok))
                                        doc_tokens.Add(norm_tok);
                                }
                            }
                            string _phrase1 = tokbuf.ToString().Trim().ToLower();

                            if (!string.IsNullOrEmpty(_phrase1))
                                doc_phrases.Add(_phrase1);

                        }
                    }
                }
            }

            return (doc_tokens);
        }

        #endregion

        #region Information Retreival


        
      


        private static bool IsUnknownTerm(string _tok)
        {
            bool _unknown_term = false;
            string tok = _tok;
            if ((GetIDF(tok) == 0.0) && IsTermNoun(tok))
                _unknown_term = true;
            return (_unknown_term);
        }

        public static bool IsPOSNoun(string pos)
        {
            bool _tag_is_noun = false;

            if ((pos == "NN") || (pos == "NNS") || (pos == "NNP"))
                _tag_is_noun = true;

            return (_tag_is_noun);
        }

        public static bool IsTermNoun(string _tok)
        {
            bool _term_is_noun = false;
            string tok = _tok;
            List<string> toklist = GetNounsFromContent(tok);
            if ((toklist != null) && (toklist.Count() > 0))
                _term_is_noun = true;
            return (_term_is_noun);
        }

        public static bool IsStopWord(string _tok)
        {
            string tok = _tok;
            if (stopwords == null || stopwords.Count == 0)
                LoadStopWords();
            return (stopwords.Contains(tok.ToLower()));
        }



        private static bool IsAvgMIWithinErrorThreshhold(double prev_avg_mi, double cur_avg_mi)
        {
            bool _within_threshhold = false;

            double avg_mi_delta = ((prev_avg_mi - cur_avg_mi) / prev_avg_mi) * 100;
            if ((avg_mi_delta > 0) && (avg_mi_delta < 2.0))
                _within_threshhold = true;

            return (_within_threshhold);
        }

        private static List<string> ExpandLowIDFQueryTerms(List<string> terms)
        {
            List<string> _expandedterms = new List<string>();

            return (_expandedterms);
        }




        private static double GetNormTermFreq(string tok, string docid)
        {
            int tf = 0;
            double w_tf = 0.0;
            double norm_tf = 0.0;
            if (invertedlist.ContainsKey(tok))
            {
                Hashtable doc = (Hashtable)invertedlist[tok];
                if ((doc != null) && (doc.ContainsKey(docid)))
                {
                    tf = (Int32)doc[docid];
                    w_tf = Math.Log((double)tf) + 1.0;
                }
            }

            //Get the max(tf,d)
            int max_freq = 1;
            if (doc_tokens_freq.ContainsKey(docid))
            {
                Hashtable freqTbl = (Hashtable)doc_tokens_freq[docid];
                foreach (int f in freqTbl.Values)
                {
                    if (f > max_freq)
                        max_freq = f;
                }
            }
            if (doc_tokens.ContainsKey(docid))
            {
                double a = 0.4;
                int total_words_in_doc = ((List<string>)doc_tokens[docid]).Count;
                //norm_tf = (double)tf / (double)total_words_in_doc;
                norm_tf = a + (1 - a) * ((double)tf / (double)max_freq);
            }
            return (norm_tf);
        }

        private static double GetIDF(string tok)
        {
            double _idf_tok = 0;
            if (idfs.ContainsKey(tok))
                _idf_tok = (double)idfs[tok];
            return (_idf_tok);
        }

        private static double GetITF(string docid)
        {
            double _itf_tok = 1;
            if (itfs.ContainsKey(docid))
                _itf_tok = (double)itfs[docid];
            return (_itf_tok);
        }

        
        public static double GetTokenFrequencyInCollection(string tok, List<string> allcollectiontokens)
        {
            double freq_i_j = 0;

            List<string> tok_list = allcollectiontokens.FindAll(x => x.Trim() == tok.ToLower().Trim());

            if (tok_list != null)
                freq_i_j = tok_list.Count();

            return (freq_i_j);
        }

       

        private static double GetCollectionFrequency(string tok)
        {
            double freq_tok = 0.0;
            if (termfreq_collection.ContainsKey(tok))
                freq_tok = (double)termfreq_collection[tok].Sum();
            return (freq_tok);
        }

        private static double GetMaxTokFrequencyInCollection(string tok)
        {
            double max_f_i_j = 1;

            if (max_tok_freq.ContainsKey(tok))
                max_f_i_j = (double)max_tok_freq[tok];
            else
            {
                foreach (string docid in documentids)
                {
                    if (doc_tokens_freq.ContainsKey(docid))
                    {
                        Hashtable freqTbl = (Hashtable)doc_tokens_freq[docid];
                        if (freqTbl.ContainsKey(tok))
                        {
                            double cure_freq = Convert.ToDouble(freqTbl[tok]);
                            if (cure_freq > max_f_i_j)
                                max_f_i_j = cure_freq;
                        }
                    }
                }

                if (!max_tok_freq.ContainsKey(tok))
                    max_tok_freq.Add(tok, max_f_i_j);
            }

            return (max_f_i_j);
        }


        private static void ComputeMaxTokenFrequency()
        {
            foreach (string v in vocabulary)
            {
                double max_freq_v = GetMaxTokFrequencyInCollection(v);
                if (!max_tok_freq.ContainsKey(v))
                    max_tok_freq.Add(v, max_freq_v);
            }
        }

        private static void ComputeTotalNumberOfTermsInCollection(string docloc)
        {
            string[] _files = Directory.GetFiles(docloc);
            StringBuilder buf = new StringBuilder();
            foreach (string f in _files)
            {
                FileInfo _fi = new FileInfo(f);
                string _docid = _fi.Name.Replace(_fi.Extension, "");
                string _content = ReadFromFile(_fi.FullName);
                buf.Append(_content);
            }
            string collection_content = buf.ToString();

            string p = @"\b[a-zA-Z]{3,}\b";

            char[] delims = { ' ', '-' };
            string[] _toks = collection_content.Split(delims, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < _toks.Length; i++)
            {
                Regex re1 = new Regex(p);
                Match _match = re1.Match(_toks[i]);
                if (_match.Success)
                {
                    string wl = _match.Value.Trim();

                    if (!IsAllUpperCase(wl) && !stopwords.Contains(wl.ToLower()))
                        allcollectiontokens.Add(wl.ToLower());

                    if (!vocabulary.Contains(wl.ToLower()) && !IsAllUpperCase(wl) && (!stopwords.Contains(wl.ToLower())))
                        vocabulary.Add(wl.ToLower());
                }
            }
        }

        private static Hashtable ComputeRawTermFrequency(string content)
        {
            Hashtable _raw_tok_freqTbl = new Hashtable();

            string p = @"\b[a-zA-Z]{3,}\b";

            List<string> tokens = new List<string>();
            char[] delims = { ' ', '-' };
            string[] _toks = content.Split(delims, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < _toks.Length; i++)
            {
                Regex re1 = new Regex(p);
                Match _match = re1.Match(_toks[i]);
                if (_match.Success)
                {
                    string wl = _match.Value.Trim();
                    if (!IsAllUpperCase(wl) && (!stopwords.Contains(wl.ToLower())))
                        tokens.Add(wl.ToLower());
                }
            }

            //Compute the raw frequency of each term(tokens) in the content         
            foreach (string t in tokens)
            {
                if (!_raw_tok_freqTbl.ContainsKey(t))
                    _raw_tok_freqTbl.Add(t, 1);
                else
                {
                    int t_freq = (int)_raw_tok_freqTbl[t];
                    _raw_tok_freqTbl.Remove(t);
                    _raw_tok_freqTbl.Add(t, t_freq + 1);

                }
            }

            return (_raw_tok_freqTbl);
        }

        private static void ComputeRawTermFrequency(string _docid, string content)
        {
            string p = @"\b[a-zA-Z]{3,}\b";

            List<string> tokens = new List<string>();
            char[] delims = { ' ', '-' };
            string[] _toks = content.Split(delims, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < _toks.Length; i++)
            {
                Regex re1 = new Regex(p);
                Match _match = re1.Match(_toks[i]);
                if (_match.Success)
                {
                    string wl = _match.Value.Trim();
                    if (!IsAllUpperCase(wl) && (!stopwords.Contains(wl.ToLower())))
                        tokens.Add(wl.ToLower());
                }
            }

            //Compute the raw frequency of each term(tokens) in the document
            Hashtable _raw_tok_freqTbl = new Hashtable();

            if (!doc_tokens_freq.ContainsKey(_docid))
                doc_tokens_freq.Add(_docid, _raw_tok_freqTbl);
            else
                _raw_tok_freqTbl = (Hashtable)doc_tokens_freq[_docid];

            foreach (string t in tokens)
            {
                if (!_raw_tok_freqTbl.ContainsKey(t))
                    _raw_tok_freqTbl.Add(t, 1);
                else
                {
                    int t_freq = (int)_raw_tok_freqTbl[t];
                    _raw_tok_freqTbl.Remove(t);
                    _raw_tok_freqTbl.Add(t, t_freq + 1);

                }
            }
        }

        //private double GetTermCorrelationFactor(string u, string v)
        //{
        //    double cor_f = 0.0;

        //    if (term_correlation.ContainsKey(u))
        //    {
        //        Hashtable cf_k_u = (Hashtable)term_correlation[u];
        //        if (cf_k_u.ContainsKey(v))
        //            cor_f = (double)cf_k_u[v];
        //    }

        //    return (cor_f);
        //}

        public static Dictionary<string, List<int>> GetTermFrequencyInContent(List<string> _tokcol)
        {
            List<string> tokcol = _tokcol;
            Dictionary<string, List<int>> termfreq = new Dictionary<string, List<int>>();
            foreach (string t in tokcol)
            {
                if (!termfreq.ContainsKey(t))
                    termfreq.Add(t, new List<int>());
                List<int> freq = termfreq[t];
                freq.Add(1);
            }
            return (termfreq);
        }
        public List<SystematicLR.Common.Paragraph> GetParagraphs(string content)
        {
            MaximumEntropySentenceDetector _sentenceDetector;
           string _modelPath = AppDomain.CurrentDomain.BaseDirectory + "../../../Resources/Models/";
            //string[] sentences = SplitSentences(_txtIn.Text);            
            _sentenceDetector = new EnglishMaximumEntropySentenceDetector(_modelPath + "EnglishSD.nbin");            
            var results = _sentenceDetector.SentenceObjectDetect(content);
           return  results.Select(e=>new Paragraph {indices=Indices.GetInstance(e.StartIndex,e.Endindix),ParagraphText=e.text }).ToList();
           
        }
        public static Dictionary<string, List<int>> GetTermFrequencyInContent(string _content)
        {
            string content = _content;
            Dictionary<string, List<int>> termfreq = new Dictionary<string, List<int>>();
            List<string> doc_tokens = TokenizeContent(content, true);
            foreach (string t in doc_tokens)
            {
                if (!termfreq.ContainsKey(t))
                    termfreq.Add(t, new List<int>());
                List<int> freq = termfreq[t];
                freq.Add(1);
            }
            return (termfreq);
        }

        #endregion



    }
}
