﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace SystematicLR.Common
{
    public class DocumentContext
    {
        public int DocumentId { set; get; }
        public Guid DcoumentRef { set; get; }
        public string DocumentName { set; get; }                
        public string DocumentPath { get; set; }
        [JsonIgnore]
        public string Abstract { get; set; }
        [JsonIgnore]
        public string Keywords { get; set; }
        [JsonIgnore]
        public int RepositoryId { get; set; }
        [JsonIgnore]
        public int QId { set; get; }
        [JsonIgnore]
        public Dictionary<string, string> KeywordsOptions { set; get; }
        [JsonIgnore]
        public Dictionary<string, string> ExpressionsOptions { set; get; }
    }
    public class Document :IDisposable
    {
        public void Dispose()
        {
            //termMap.Dispose();
            //termsList = null;
            //fontsCounts = null;
            //ExtractedExpressions = null;
            //ExtractedKeywords = null;
            //GC.SuppressFinalize(this);
        }
        public DocumentContext context { private set; get; }


        // public FontsDictionary fontsMap { private set; get; }
        //public int QId { set; get; }
        public int PagesCount { set; get; }
        public int WordsInDocument { private set; get; }
        public Dictionary<TermFont, int> fontsCounts { set; get; }
        [JsonIgnore]
        public TermsDictionary termMap { private set; get; }
        [JsonIgnore]
        public List<ExtractedTerm> termsList { private set; get; }            
        public bool IsAnalyzed { private set; get; }
        public Dictionary<string, int> ExtractedExpressions { set; get; }
        public List<ExtratedTermPackage> ExtractedKeywords { set; get; }
        
        
        public void AddTerm(ExtractedTerm term)
        {
            WordsInDocument++;
            if(!string.IsNullOrEmpty(term.StemWord) && term.isValidTerm && !term.isStopWord)
            {
                termsList.Add(term);
                termMap.AddNew(term);
            }
            if (!fontsCounts.ContainsKey(term.Font))
                fontsCounts.Add(term.Font, 0);
            fontsCounts[term.Font] += 1;
            //fontsMap.AddNew(term);
           
            
        }
        public Document(DocumentContext context)
        {
            this.context = context;          
            termMap = new TermsDictionary();
            termsList = new List<ExtractedTerm>();
            ExtractedExpressions = new Dictionary<string, int>();
            fontsCounts = new Dictionary<TermFont, int>();
            WordsInDocument = 0;

        }
        public void AnalyzeDocument()
        {           
            if (!IsAnalyzed)
            {
                AnalyzingContext context = new AnalyzingContext()
                {
                    WordsCount = this.WordsInDocument,
                    Fontwieght = new Dictionary<TermFont, double>()
                };
                //var fontterms=fontsMap.map.Select(e => new { font = e.Key, termsCount = e.Value.Length })  ;
                //var maxFontTemp = from x in fontterms where x.termsCount == fontterms.Max(v => v.termsCount) select x.font;


                #region font Factor
                var fontterms = fontsCounts.Select(e => new { font = e.Key, termsCount = e.Value });
                var maxFontTemp = from x in fontterms where x.termsCount == fontterms.Max(v => v.termsCount) select x.font;
                var maxFont = maxFontTemp.FirstOrDefault();
                double boldValue = maxFont.Bold ? 0.00 : 0.5;
                foreach (var itemfont in fontsCounts.Keys.OrderBy(e => e.FontSize))
                {
                    double wieght = itemfont.FontSize <= maxFont.FontSize ? 1 : itemfont.FontSize / maxFont.FontSize;
                    if (itemfont.Bold)
                        wieght = wieght + (wieght * boldValue);
                    context.Fontwieght.Add(itemfont, wieght);
                }
                #endregion

                var termsCount = termMap.map.Max(e => e.Value.terms.Count);
                context.MaxTermFrequancy = termsCount;

                termMap.Analyze(context);

                this.ExtractedKeywords = termMap.map.Select(e => e.Value).OrderByDescending(e => e.TermWeight).ToList();
                termMap.Dispose();

                ExtractedExpressions = Utilities.Getterms(this.termsList,this.context.ExpressionsOptions);

                //check if the keywords contain some expretion options
                if(this.context.ExpressionsOptions!=null)
                {
                    var matchExpressionKeywords=this.ExtractedKeywords.Where(e => this.context.ExpressionsOptions.ContainsKey(e.StemTerm)).Select(e => new { e.StemTerm, e.TermCount }).ToList();
                    foreach(var item in matchExpressionKeywords)
                    {
                        if (!ExtractedExpressions.ContainsKey(item.StemTerm))
                            ExtractedExpressions.Add(this.context.ExpressionsOptions[item.StemTerm], item.TermCount);
                            ExtractedExpressions[item.StemTerm] += item.TermCount;
                    }
                }
                
                
                this.termsList.Clear();
                IsAnalyzed = true;
            }
        }

    }
}
