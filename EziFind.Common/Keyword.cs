﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Common
{
    public class Keyword
    {
        public string OriginalWord { set; get; }
        public StemKeyword stemword { set; get; }
        public bool isStopWord { set; get; }
        public Paragraph BaseParagraph { set; get; }
        public int Position { set; get; }
        public Indices indiced { set; get; }

    }
    public class StemKeyword
    {
        public string StemText { set; get; }
        public List<Keyword> WordsList { set; get; }
    }
}
