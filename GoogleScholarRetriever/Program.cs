﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace GoogleScholarRetriever
{
    class Program
    {
        private static HttpClient client = new HttpClient();
        private static HttpClientHandler handler = new HttpClientHandler()
        {
            AllowAutoRedirect = true
        };
        static void Main(string[] args)
        {
            conn.Open();
          // CallGoogleScholar();
          //  ParseGoogleScholarResponses();
            GetCitationFromGoogleScholar();
          //  StripHTMLAllDocuments();
            conn.Close();
            //https://scholar.google.com/scholar?q=info:2Gtj3PrLYDcJ:scholar.google.com/&output=gsb-cite&hl=en
            //https://scholar.google.com/scholar?q=info:6XtDkH-zEp8J:scholar.google.com/&output=gsb-cite&hl=en
        }
        public static async Task<string> ReadData(string URL)
        {

            var responseString = await client.GetStringAsync(URL);
            return responseString;
        }
        public static DataTable HTMLEncodingList = new DataTable();
        public static SqlConnection conn = new SqlConnection(@"data source=WIN8\sqlexpress;initial catalog=SystematicLR;integrated security=True;MultipleActiveResultSets=True;");
        public static string StripHTML(string input)
        {
            input =  System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", String.Empty);

            if (HTMLEncodingList.Rows.Count == 0)
            {
                SqlCommand cmd = new SqlCommand("select * from HTMLEncoding", conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(HTMLEncodingList);
                adp.Dispose();
                cmd.Dispose();
            }
            foreach (DataRow row in HTMLEncodingList.Rows)
            {
                input = input.Replace(row[1].ToString(), row[0].ToString());
            }
            return input;
        }
        public static void CallGoogleScholar()
        {
            client = new HttpClient(handler);
            SqlCommand cmd = new SqlCommand("select DocumentGuid,Title from Documents where AJAXResponse is null order by Id ASC", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set AjaxRequest=@AjaxRequest, AJAXResponse=@AJAXResponse where DocumentGuid=@DocumentGuid", conn);
            SqlParameter prmAJAXResponse = new SqlParameter() { ParameterName = "@AJAXResponse", SqlDbType = SqlDbType.NVarChar, Value = "No Value" };
            SqlParameter prmAjaxRequest = new SqlParameter() { ParameterName = "@AjaxRequest", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            updateCommand.Parameters.Add(prmAJAXResponse);
            updateCommand.Parameters.Add(prmAjaxRequest);
            updateCommand.Parameters.Add(prmDocumentGuid);
            DataTable dt = new DataTable();

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            string Title = "";
            string URL = "";
            foreach (DataRow row in dt.Rows)
            {
                Title = (string)row["Title"].ToString().Trim();
                URL = string.Format("https://scholar.google.com/scholar?oi=gsb95&q={0}&output=gsb&hl=en", Title);
                var dataText = ReadData(URL);
                dataText.Wait();
                prmAjaxRequest.Value = URL;
                prmAJAXResponse.Value = dataText.Result;
                prmDocumentGuid.Value = row["DocumentGuid"];

                updateCommand.ExecuteNonQuery();

                prmAJAXResponse.Value = "No Value";
                prmDocumentGuid.Value = DBNull.Value;
                prmAjaxRequest.Value = DBNull.Value;
                
            }


        }
        static Random rnd = new Random();
        public static void ParseGoogleScholarResponses()
        {
            SqlCommand cmd = new SqlCommand("select * from [dbo].[Documents] where isnull(CheckParsingGSResponse,0)=0 order by Id ASC", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set Year=isnull(Year,@Year),Publisher=isnull(Publisher,@Publisher),Authers=isnull(Authers,@Authers),ReferenceType=isnull(ReferenceType,@ReferenceType),CiteId=isnull(CiteId,@CiteId),DocumentLink=isnull(DocumentLink,@DocumentLink),Abstract=isnull(Abstract,@Abstract), VersionsCount=isnull(VersionsCount,@VersionsCount) ,CiteCount=isnull(CiteCount,@CiteCount),ResponseNum=isnull(ResponseNum,@ResponseNum), ResponseCount=isnull(ResponseCount,@ResponseCount),CheckParsingGSResponse=1,NotFoundinGS=0 where DocumentGuid=@DocumentGuid", conn);
            string updateStatmentifNoResponse = "update Documents set ResponseCount=@ResponseCount,CheckParsingGSResponse=1,NotFoundinGS=1 where DocumentGuid=@DocumentGuid";
            string UpdateStatmentIfMoreThanOnResponse = "update Documents set ResponseCount=@ResponseCount where DocumentGuid=@DocumentGuid";
            SqlCommand updateCommand2 = new SqlCommand("",conn);

            SqlParameter prmYear = new SqlParameter() { ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmPublisher = new SqlParameter() { ParameterName = "@Publisher", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAuthers = new SqlParameter() { ParameterName = "@Authers", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmReferenceType = new SqlParameter() { ParameterName = "@ReferenceType", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCiteId = new SqlParameter() { ParameterName = "@CiteId", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentLink = new SqlParameter() { ParameterName = "@DocumentLink", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAbstract = new SqlParameter() { ParameterName = "@Abstract", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCiteCount = new SqlParameter() { ParameterName = "@CiteCount", SqlDbType = SqlDbType.Int };
            SqlParameter prmVersionsCount = new SqlParameter() { ParameterName = "@VersionsCount", SqlDbType = SqlDbType.Int };
            SqlParameter prmResponseNum = new SqlParameter() { ParameterName = "@ResponseNum", SqlDbType = SqlDbType.Int };
            SqlParameter prmResponseCount = new SqlParameter() { ParameterName = "@ResponseCount", SqlDbType = SqlDbType.Int };
            SqlParameter prmResponseCount2 = new SqlParameter() { ParameterName = "@ResponseCount", SqlDbType = SqlDbType.Int };
            SqlParameter prmDocumentGuid2 = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            updateCommand.Parameters.Add(prmYear);
            updateCommand.Parameters.Add(prmPublisher);
            updateCommand.Parameters.Add(prmAuthers);
            updateCommand.Parameters.Add(prmReferenceType);            
            updateCommand.Parameters.Add(prmCiteId);
            updateCommand.Parameters.Add(prmDocumentLink);
            updateCommand.Parameters.Add(prmAbstract);
            updateCommand.Parameters.Add(prmDocumentGuid);
            updateCommand.Parameters.Add(prmVersionsCount);
            updateCommand.Parameters.Add(prmResponseNum);
            updateCommand.Parameters.Add(prmResponseCount);
            updateCommand.Parameters.Add(prmCiteCount);
            updateCommand2.Parameters.Add(prmResponseCount2);
            updateCommand2.Parameters.Add(prmDocumentGuid2);

            DataTable dt = new DataTable();

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            string Title = "";
            foreach (DataRow row in dt.Rows)
            {
                Title = (string)row["Title"].ToString().Trim();
                prmDocumentGuid.Value = (string)row["DocumentGuid"];

                var dic = QuickType.Article.FromJson(row["AjaxResponse"].ToString());
                int i = 0;
                prmResponseCount.Value = dic.R.Length;
                prmResponseCount2.Value = dic.R.Length;
                if (dic.R.Length == 0)
                {
                    updateCommand2.CommandText = updateStatmentifNoResponse;
                    prmDocumentGuid2.Value = (string)row["DocumentGuid"];
                    prmResponseCount2.Value = dic.R.Length;
                    updateCommand2.ExecuteNonQuery();
                    prmResponseCount2.Value = DBNull.Value;
                    prmDocumentGuid2.Value = DBNull.Value;
                    continue;
                }
                if (dic.R.Length > 1 )
                {
                    if(row["ResponseNum"] == DBNull.Value)
                    {
                        updateCommand2.CommandText = UpdateStatmentIfMoreThanOnResponse;
                        prmDocumentGuid2.Value = (string)row["DocumentGuid"];
                        prmResponseCount2.Value = dic.R.Length;                   
                        updateCommand2.ExecuteNonQuery();
                        prmResponseCount2.Value = DBNull.Value ;
                        prmDocumentGuid2.Value = DBNull.Value;
                        continue;                  
                    }
                    else
                    {
                        i = int.Parse(row["ResponseNum"].ToString());
                        
                    }
                    
                }
                prmResponseNum.Value = i;
                var spits = dic.R[i].M.Split('-');
                prmAuthers.Value = StripHTML(spits[0].Trim());

                if (spits.Length > 1)
                {
                    var spt2 = spits[1].Split(',');
                    if (spt2.Length > 1)
                    {
                        prmPublisher.Value = StripHTML(spt2[0].Trim());
                        int tempint ;
                        for(int vvl=1;vvl<spt2.Length;vvl++)
                        {

                            if(int.TryParse(StripHTML(spt2[vvl].Trim()),out tempint))
                            {
                                prmYear.Value = tempint;
                                break;
                            }
                            
                        }                       
                    }
                    else
                    {
                        int temp;
                        if (int.TryParse(spt2[0], out temp))
                        {
                            prmYear.Value = StripHTML(spt2[0].Trim());
                        }
                        else
                        {
                            prmPublisher.Value = StripHTML(spt2[0]);
                        }
                    }
                }
                prmAbstract.Value = StripHTML(dic.R[i].S);
                prmCiteId.Value = StripHTML(dic.R[i].L.F.U.Substring(2));
                if(dic.R[i].L.C!=null)
                {
                    prmCiteCount.Value =int.Parse(dic.R[i].L.C.L.Replace("Cited by", "").Trim().ToString());
                }
                else
                {
                    prmCiteCount.Value = 0;
                }
                if (dic.R[i].L.V != null)
                {
                    prmVersionsCount.Value =int.Parse(dic.R[i].L.V.L.Replace("All", "").Replace("versions","").Trim().ToString());
                }
                else
                {
                    prmVersionsCount.Value = 1;
                }

                prmDocumentLink.Value = dic.R[i].U;
                prmReferenceType.Value = dic.R[i].X;
                prmDocumentGuid.Value = (string)row["DocumentGuid"];
                
                updateCommand.ExecuteNonQuery();

                prmYear.Value = DBNull.Value;
                prmPublisher.Value = DBNull.Value;
                prmAuthers.Value = DBNull.Value;
                prmReferenceType.Value = DBNull.Value;
                prmCiteId.Value = DBNull.Value;
                prmDocumentLink.Value = DBNull.Value;
                prmAbstract.Value = DBNull.Value;
                prmDocumentGuid.Value = DBNull.Value;
                prmCiteCount.Value = DBNull.Value;
                prmVersionsCount.Value = DBNull.Value;
                prmDocumentGuid.Value = DBNull.Value;
                prmResponseNum.Value = DBNull.Value;
            }
        }
        public static void GetCitationFromGoogleScholar()
        {
            client = new HttpClient(handler);

            SqlCommand cmd = new SqlCommand("select DocumentGUID, CiteId from[dbo].[Documents] where (CiteId is not null and Cite is null) or checkImportCite=0", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set Cite=@Cite, Authers=isNull(Authers,@Authers),Publisher=isNull(Publisher,@Publisher), AjaxRequestCite=@AjaxRequestCite,CheckImportCite=1 where DocumentGuid=@DocumentGuid", conn);

            SqlParameter prmCite = new SqlParameter() { ParameterName = "@Cite", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCiteId = new SqlParameter() { ParameterName = "@CiteId", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };

            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmAuthers = new SqlParameter() { ParameterName = "@Authers", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmPublisher = new SqlParameter() { ParameterName = "@Publisher", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAjaxRequestCite = new SqlParameter() { ParameterName = "@AjaxRequestCite", SqlDbType = SqlDbType.NVarChar };
            updateCommand.Parameters.Add(prmAuthers);
            updateCommand.Parameters.Add(prmAjaxRequestCite);
            updateCommand.Parameters.Add(prmPublisher);
            updateCommand.Parameters.Add(prmCite);
            updateCommand.Parameters.Add(prmCiteId);
            updateCommand.Parameters.Add(prmDocumentGuid);

            SqlCommand insertCommand = new SqlCommand("insert into Citation values(@DocumentGuid,@CitationType,@CiteText)", conn);
            SqlParameter prmDocumentGuid1 = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCitationType = new SqlParameter() { ParameterName = "@CitationType", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCiteText = new SqlParameter() { ParameterName = "@CiteText", SqlDbType = SqlDbType.NVarChar };
            insertCommand.Parameters.Add(prmDocumentGuid1);
            insertCommand.Parameters.Add(prmCitationType);
            insertCommand.Parameters.Add(prmCiteText);

            DataTable dt = new DataTable();

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            string Title = "";
            string URL = "";
            foreach (DataRow row in dt.Rows)
            {
                prmCiteId.Value = row["CiteId"].ToString();
                URL = string.Format("https://scholar.google.com/scholar?q=info:{0}:scholar.google.com/&output=gsb-cite&hl=en", prmCiteId.Value);
                var dataText = ReadData(URL);
                dataText.Wait();
                var cite = QuickType.Citation.FromJson(dataText.Result);
                prmAjaxRequestCite.Value = URL;

                prmDocumentGuid1.Value = (string)row["DocumentGuid"];
                prmDocumentGuid.Value = (string)row["DocumentGuid"];
                foreach (var c in cite.L)
                {
                    prmCitationType.Value = c.LL;
                    prmCiteText.Value = StripHTML(c.H);
                    insertCommand.ExecuteNonQuery();
                    if (c.LL == "Harvard")
                    {
                        prmCite.Value = prmCiteText.Value;
                    }
                    if (c.LL == "Vancouver")
                    {
                        var spt = prmCiteText.Value.ToString().Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        prmAuthers.Value = spt[0];
                        if (spt.Length > 1)
                        {
                            if (prmPublisher.Value == DBNull.Value)
                                prmPublisher.Value = string.Join(" ", spt.Skip(2).ToArray());
                        }

                    }
                    prmCitationType.Value = DBNull.Value;
                    prmCiteText.Value = DBNull.Value;
                }

                updateCommand.ExecuteNonQuery();

                prmPublisher.Value = DBNull.Value;
                prmAuthers.Value = DBNull.Value;

                prmCite.Value = DBNull.Value;
                prmCiteId.Value = DBNull.Value;
                prmDocumentGuid.Value = DBNull.Value;
                prmAjaxRequestCite.Value = DBNull.Value; 

            }
        }
        public static void RunAndCallGoogleScholar()
        {
            client = new HttpClient(handler);

            SqlCommand cmd = new SqlCommand("select * from Documents where AJAXResponse is null order by Id ASC", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set Year=@Year,Publisher=@Publisher,Authers=@Authers,ReferenceType=@ReferenceType,Cite=@Cite,CiteId=@CiteId,DocumentLink=@DocumentLink,Abstract=@Abstract where DocumentGuid=@DocumentGuid", conn);
            SqlParameter prmYear = new SqlParameter() { ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmPublisher = new SqlParameter() { ParameterName = "@Publisher", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAuthers = new SqlParameter() { ParameterName = "@Authers", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmReferenceType = new SqlParameter() { ParameterName = "@ReferenceType", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCite = new SqlParameter() { ParameterName = "@Cite", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCiteId = new SqlParameter() { ParameterName = "@CiteId", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentLink = new SqlParameter() { ParameterName = "@DocumentLink", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAbstract = new SqlParameter() { ParameterName = "@Abstract", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            updateCommand.Parameters.Add(prmYear);
            updateCommand.Parameters.Add(prmPublisher);
            updateCommand.Parameters.Add(prmAuthers);
            updateCommand.Parameters.Add(prmReferenceType);
            updateCommand.Parameters.Add(prmCite);
            updateCommand.Parameters.Add(prmCiteId);
            updateCommand.Parameters.Add(prmDocumentLink);
            updateCommand.Parameters.Add(prmAbstract);
            updateCommand.Parameters.Add(prmDocumentGuid);

            SqlCommand insertCommand = new SqlCommand("insert into Citation values(@DocumentGuid,@CitationType,@CiteText)", conn);
            SqlParameter prmDocumentGuid1 = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCitationType = new SqlParameter() { ParameterName = "@CitationType", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCiteText = new SqlParameter() { ParameterName = "@CiteText", SqlDbType = SqlDbType.NVarChar };
            insertCommand.Parameters.Add(prmDocumentGuid1);
            insertCommand.Parameters.Add(prmCitationType);
            insertCommand.Parameters.Add(prmCiteText);

            DataTable dt = new DataTable();

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            string Title = "";
            string URL = "";
            foreach (DataRow row in dt.Rows)
            {
                Title = (string)row["Title"].ToString().Trim();
                URL = string.Format("https://scholar.google.com/scholar?oi=gsb95&q={0}&output=gsb&hl=en", Title);
                var dataText = ReadData(URL);
                dataText.Wait();
                var dic = QuickType.Article.FromJson(dataText.Result);
                if (dic.R.Length == 1)
                {
                    var spits = dic.R[0].M.Split('-');
                    prmAuthers.Value = spits[0].Trim();
                    if (spits.Length > 1)
                    {
                        var spt2 = spits[1].Split(',');
                        if (spt2.Length > 1)
                        {
                            prmPublisher.Value = spt2[0].Trim();
                            prmYear.Value = spt2[1].Trim();
                        }
                        else
                        {
                            int temp;
                            if (int.TryParse(spt2[0], out temp))
                            {
                                prmYear.Value = spt2[0].Trim();
                            }
                            else
                            {
                                prmPublisher.Value = spt2[0];
                            }
                        }
                    }
                    prmAbstract.Value = dic.R[0].S;
                    prmCiteId.Value = dic.R[0].L.F.U.Substring(2);
                    prmDocumentLink.Value = dic.R[0].U;
                    prmReferenceType.Value = dic.R[0].X;
                    URL = string.Format("https://scholar.google.com/scholar?q=info:{0}:scholar.google.com/&output=gsb-cite&hl=en", prmCiteId.Value);
                    dataText = ReadData(URL);
                    dataText.Wait();
                    var cite = QuickType.Citation.FromJson(dataText.Result);

                    prmDocumentGuid1.Value = (string)row["DocumentGuid"];
                    prmDocumentGuid.Value = (string)row["DocumentGuid"];
                    foreach (var c in cite.L)
                    {
                        prmCitationType.Value = c.LL;
                        prmCiteText.Value = StripHTML(c.H);
                        insertCommand.ExecuteNonQuery();
                        if (c.LL == "Harvard")
                        {
                            prmCite.Value = prmCiteText.Value;
                        }
                        if (c.LL == "Vancouver")
                        {
                            var spt = prmCiteText.Value.ToString().Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            prmAuthers.Value = spt[0];
                            if (spt.Length > 1)
                            {
                                if (prmPublisher.Value == DBNull.Value)
                                    prmPublisher.Value = string.Join(" ", spt.Skip(2).ToArray());
                            }

                        }
                        prmCitationType.Value = DBNull.Value;
                        prmCiteText.Value = DBNull.Value;
                    }
                }
                else
                {
                    prmCite.Value = "Ignored Multi Results";
                }
                updateCommand.ExecuteNonQuery();

                prmYear.Value = DBNull.Value;
                prmPublisher.Value = DBNull.Value;
                prmAuthers.Value = DBNull.Value;
                prmReferenceType.Value = DBNull.Value;
                prmCite.Value = DBNull.Value;
                prmCiteId.Value = DBNull.Value;
                prmDocumentLink.Value = DBNull.Value;
                prmAbstract.Value = DBNull.Value;
                prmDocumentGuid.Value = DBNull.Value;
                System.Threading.Thread.Sleep(5000);
            }


        }
        public static void StripHTMLAllDocuments()
        {            
            SqlCommand cmd = new SqlCommand("select DocumentGUID, Cite,Authers,Publisher from[dbo].[Documents] order by id", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set Cite=@Cite, Authers=@Authers,Publisher=@Publisher where DocumentGuid=@DocumentGuid", conn);

            SqlParameter prmCite = new SqlParameter() { ParameterName = "@Cite", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };            

            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmAuthers = new SqlParameter() { ParameterName = "@Authers", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmPublisher = new SqlParameter() { ParameterName = "@Publisher", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            updateCommand.Parameters.Add(prmAuthers);
            updateCommand.Parameters.Add(prmPublisher);
            updateCommand.Parameters.Add(prmCite);
            updateCommand.Parameters.Add(prmDocumentGuid);
           

            DataTable dt = new DataTable();

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);       
            foreach (DataRow row in dt.Rows)
            {
              
                prmDocumentGuid.Value = (string)row["DocumentGuid"];                
                if(row["Authers"]!=DBNull.Value)
                {
                    prmAuthers.Value = StripHTML(row["Authers"].ToString());
                }
                if (row["Publisher"] != DBNull.Value)
                {
                    prmPublisher.Value = StripHTML(row["Publisher"].ToString());
                }
                if (row["Cite"] != DBNull.Value)
                {
                    prmCite.Value = StripHTML(row["Cite"].ToString());
                }
                
                updateCommand.ExecuteNonQuery();

                prmPublisher.Value = DBNull.Value;
                prmAuthers.Value = DBNull.Value;
                prmCite.Value = DBNull.Value;               
                prmDocumentGuid.Value = DBNull.Value;

            }
        }
        public void Run()
        {
            client = new HttpClient(handler);
            SqlConnection conn = new SqlConnection(@"data source=WIN8\sqlexpress;initial catalog=SystematicLR;integrated security=True;MultipleActiveResultSets=True;");
            SqlCommand cmd = new SqlCommand("select * from Documents where Id>=124 order by Id ASC", conn);
            SqlCommand updateCommand = new SqlCommand("update Documents set Year=@Year,Publisher=@Publisher,Authers=@Authers,ReferenceType=@ReferenceType,Cite=@Cite,CiteId=@CiteId,DocumentLink=@DocumentLink,Abstract=@Abstract where DocumentGuid=@DocumentGuid", conn);
            SqlParameter prmYear = new SqlParameter() { ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmPublisher = new SqlParameter() { ParameterName = "@Publisher", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAuthers = new SqlParameter() { ParameterName = "@Authers", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmReferenceType = new SqlParameter() { ParameterName = "@ReferenceType", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCite = new SqlParameter() { ParameterName = "@Cite", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmCiteId = new SqlParameter() { ParameterName = "@CiteId", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentLink = new SqlParameter() { ParameterName = "@DocumentLink", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmAbstract = new SqlParameter() { ParameterName = "@Abstract", SqlDbType = SqlDbType.NVarChar, Value = DBNull.Value };
            SqlParameter prmDocumentGuid = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            updateCommand.Parameters.Add(prmYear);
            updateCommand.Parameters.Add(prmPublisher);
            updateCommand.Parameters.Add(prmAuthers);
            updateCommand.Parameters.Add(prmReferenceType);
            updateCommand.Parameters.Add(prmCite);
            updateCommand.Parameters.Add(prmCiteId);
            updateCommand.Parameters.Add(prmDocumentLink);
            updateCommand.Parameters.Add(prmAbstract);
            updateCommand.Parameters.Add(prmDocumentGuid);

            SqlCommand insertCommand = new SqlCommand("insert into Citation values(@DocumentGuid,@CitationType,@CiteText)", conn);
            SqlParameter prmDocumentGuid1 = new SqlParameter() { ParameterName = "@DocumentGuid", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCitationType = new SqlParameter() { ParameterName = "@CitationType", SqlDbType = SqlDbType.NVarChar };
            SqlParameter prmCiteText = new SqlParameter() { ParameterName = "@CiteText", SqlDbType = SqlDbType.NVarChar };
            insertCommand.Parameters.Add(prmDocumentGuid1);
            insertCommand.Parameters.Add(prmCitationType);
            insertCommand.Parameters.Add(prmCiteText);

            DataTable dt = new DataTable();
            conn.Open();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            string Title = "";
            string URL = "";
            foreach (DataRow row in dt.Rows)
            {
                Title = (string)row["Title"].ToString().Trim();
                URL = string.Format("https://scholar.google.com/scholar?oi=gsb95&q={0}&output=gsb&hl=en", Title);
                var dataText = ReadData(URL);
                dataText.Wait();
                var dic = QuickType.Article.FromJson(dataText.Result);
                if (dic.R.Length == 1)
                {
                    var spits = dic.R[0].M.Split('-');
                    prmAuthers.Value = spits[0].Trim();
                    if (spits.Length > 1)
                    {
                        var spt2 = spits[1].Split(',');
                        if (spt2.Length > 1)
                        {
                            prmPublisher.Value = spt2[0].Trim();
                            prmYear.Value = spt2[1].Trim();
                        }
                        else
                        {
                            int temp;
                            if (int.TryParse(spt2[0], out temp))
                            {
                                prmYear.Value = spt2[0].Trim();
                            }
                            else
                            {
                                prmPublisher.Value = spt2[0];
                            }
                        }
                    }
                    prmAbstract.Value = dic.R[0].S;
                    prmCiteId.Value = dic.R[0].L.F.U.Substring(2);
                    prmDocumentLink.Value = dic.R[0].U;
                    prmReferenceType.Value = dic.R[0].X;
                    URL = string.Format("https://scholar.google.com/scholar?q=info:{0}:scholar.google.com/&output=gsb-cite&hl=en", prmCiteId.Value);
                    dataText = ReadData(URL);
                    dataText.Wait();
                    var cite = QuickType.Citation.FromJson(dataText.Result);

                    prmDocumentGuid1.Value = (string)row["DocumentGuid"];
                    prmDocumentGuid.Value = (string)row["DocumentGuid"];
                    foreach (var c in cite.L)
                    {
                        prmCitationType.Value = c.LL;
                        prmCiteText.Value = StripHTML(c.H);
                        insertCommand.ExecuteNonQuery();
                        if (c.LL == "Harvard")
                        {
                            prmCite.Value = prmCiteText.Value;
                        }
                        if (c.LL == "Vancouver")
                        {
                            var spt = prmCiteText.Value.ToString().Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            prmAuthers.Value = spt[0];
                            if (spt.Length > 1)
                            {
                                if (prmPublisher.Value == DBNull.Value)
                                    prmPublisher.Value = string.Join(" ", spt.Skip(2).ToArray());
                            }

                        }
                        prmCitationType.Value = DBNull.Value;
                        prmCiteText.Value = DBNull.Value;
                    }
                }
                else
                {
                    prmCite.Value = "Ignored Multi Results";
                }
                updateCommand.ExecuteNonQuery();

                prmYear.Value = DBNull.Value;
                prmPublisher.Value = DBNull.Value;
                prmAuthers.Value = DBNull.Value;
                prmReferenceType.Value = DBNull.Value;
                prmCite.Value = DBNull.Value;
                prmCiteId.Value = DBNull.Value;
                prmDocumentLink.Value = DBNull.Value;
                prmAbstract.Value = DBNull.Value;
                prmDocumentGuid.Value = DBNull.Value;
                System.Threading.Thread.Sleep(5000);
            }

        }
    }

}
