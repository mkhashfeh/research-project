﻿namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Article
    {
        [JsonProperty("h")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long H { get; set; }

        [JsonProperty("r")]
        public R[] R { get; set; }
    }

    public partial class R
    {
        [JsonProperty("t")]
        public string T { get; set; }

        [JsonProperty("u")]
        public string U { get; set; }

        [JsonProperty("x")]
        public string X { get; set; }

        [JsonProperty("m")]
        public string M { get; set; }

        [JsonProperty("s")]
        public string S { get; set; }

        [JsonProperty("l")]
        public L L { get; set; }
    }

    public partial class L
    {
        [JsonProperty("v")]
        public F V { get; set; }

        [JsonProperty("f")]
        public F F { get; set; }

        [JsonProperty("g")]
        public F G { get; set; }

        [JsonProperty("c")]
        public F C { get; set; }

        [JsonProperty("r")]
        public F R { get; set; }
    }

    public partial class F
    {
        [JsonProperty("l")]
        public string L { get; set; }

        [JsonProperty("u")]
        public string U { get; set; }
    }
    


    public partial class Article
    {
        public static Article FromJson(string json) => JsonConvert.DeserializeObject<Article>(json, QuickType.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Article self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}