﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var citation = Citation.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Citation
    {
        [JsonProperty("l")]
        public L[] L { get; set; }

        [JsonProperty("i")]
        public I[] I { get; set; }
    }

    public partial class I
    {
        [JsonProperty("l")]
        public string L { get; set; }

        [JsonProperty("u")]
        public string U { get; set; }
    }

    public partial class L
    {
        [JsonProperty("l")]
        public string LL { get; set; }

        [JsonProperty("h")]
        public string H { get; set; }
    }

    public partial class Citation
    {
        public static Citation FromJson(string json) => JsonConvert.DeserializeObject<Citation>(json, QuickType.Converter.Settings);
    }

   
}
