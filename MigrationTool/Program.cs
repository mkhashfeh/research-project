﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities;
using SystematicLR.GoogleScholar;
using System.Data.SqlClient;
using System.Data;

namespace MigrationTool
{
    class Program
    {
        public static SqlConnection conn = new SqlConnection(@"data source=WIN8\sqlexpress;initial catalog=SystematicLR;integrated security=True;MultipleActiveResultSets=True;");

        static void Main(string[] args)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from Documents where not exists(select top 1 1 from [dbo].[References] where ReferenceGUID=DocumentGUID)", conn);
            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            conn.Close();
            foreach(DataRow dr in dt.Rows)
            {
                string CitedByLink = null;
                string DownloadURL = null;
                string GoogleFileType = null;
                string HostSite = null;
                string MainURL = null;
                
                string RelatedArticlesLink = null;
                string VersionLink = null;
                int? issueYear = dr["Year"] != DBNull.Value ? int.Parse(dr["Year"].ToString()) : (int?) null;
                string country = dr["Country"] != DBNull.Value ? dr["Country"].ToString() : null;
                string ReferenceType = dr["ReferenceType"] != DBNull.Value ? dr["ReferenceType"].ToString() : null;
                int? CiteCount = dr["CiteCount"] != DBNull.Value ? int.Parse(dr["CiteCount"].ToString()) :(int?) null;
                int? VersionCount = dr["VersionsCount"] != DBNull.Value ? int.Parse(dr["VersionsCount"].ToString()) : (int?)null;
                bool FoundGS = dr["NotFoundinGS"] == DBNull.Value ? false : bool.Parse(dr["NotFoundinGS"].ToString());
                Articles articles = null;
                try
                {
                    articles = Articles.FromJson(dr["AJAXResponse"].ToString());
                }
                catch(Exception ex)
                {

                }               

                if(articles!=null && articles.articles.Length>0)
                {
                    if(dr["ResponseCount"]!=DBNull.Value)
                    {
                        var art = articles.articles[int.Parse(dr["ResponseNum"].ToString())];
                        GSOutputArticle newart = new GSOutputArticle(art);
                        CitedByLink = newart.CitedByLink;
                        DownloadURL = newart.DownloadURL;
                        GoogleFileType = newart.FileType;
                        HostSite = newart.HostSite;
                        MainURL = newart.MainURL;
                        RelatedArticlesLink=newart.RelatedArticlesLink;
                        VersionLink = newart.VersionLink; 
                        
                                              
                    }
                   
                }
                DBServices.AddNewReference(dr["Title"] as string, issueYear, dr["Authers"] as string, dr["Publisher"] as string, country, ReferenceType, CiteCount, VersionCount, dr["Abstract"] as string, null, dr["FullPath"] as string, DownloadURL, 1, FoundGS, dr["CiteId"] as string, null, HostSite, MainURL, GoogleFileType, RelatedArticlesLink, CitedByLink, VersionLink, 1, dr["Category"] as string, dr["FullPath"] as string, true, HostSite, null, null, null, null, null, Guid.Parse(dr["DocumentGuid"].ToString()));
            }
            

        }
    }
}
