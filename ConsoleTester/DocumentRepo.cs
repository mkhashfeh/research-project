﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
namespace ConsoleTester
{
    public class DocumentRepo
    {
        private StringBuilder AllData = new StringBuilder();
        public DocumentRepo()
        {

        }

        private void createMetadate(FileInfo fileinfo)
        {
            StringBuilder str = new StringBuilder("");
            string format = "{0}: {1}";
            string Id = Guid.NewGuid().ToString();
            str.AppendLine(string.Format(format, "DocumentGUID", Id));
            str.AppendLine(string.Format(format, "DocumentName", fileinfo.Name));
            str.AppendLine(string.Format(format, "DocumentPath", fileinfo.FullName));
            str.AppendLine(string.Format(format, "Title", fileinfo.Directory.Name));
            str.AppendLine(string.Format(format, "Category", fileinfo.Directory.Parent.Name));
            str.AppendLine(string.Format(format, "IssueYear", ""));
            str.AppendLine(string.Format(format, "Authers", ""));
            str.AppendLine(string.Format(format, "Publisher", ""));
            str.AppendLine(string.Format(format, "ReferenceType", "Paper"));
            AllData.AppendLine(string.Format("{0}${1}${2}${3}${4}${5}${6}${7}${8}", Id, fileinfo.Directory.Name, fileinfo.Directory.Parent.Name, "", "", "", "Paper", fileinfo.Name, fileinfo.FullName));
            File.WriteAllText(fileinfo.DirectoryName + "\\" + "MetaData " + Id + ".txt", str.ToString());
            File.Create(fileinfo.DirectoryName + "\\" + "Abstract.txt");
            File.Create(fileinfo.DirectoryName + "\\" + "Keywords.txt");


        }
        private void createMetadateWithDB( FileInfo fileinfo)
        {
            string DocumentGUId = Guid.NewGuid().ToString();

            SqlConnection conn = new SqlConnection(@"data source=WIN8\sqlexpress;initial catalog=SystematicLR;integrated security=True;MultipleActiveResultSets=True;");
            SqlCommand cmd = new SqlCommand(@"Declare @Id int; select @Id =isnull(max(id),0)+1 from [dbo].[Documents]
INSERT INTO [dbo].[Documents] ([Id] ,[DocumentGUID],[Title],[Category],[ReferenceType],[FileName],[FullPath])
     VALUES(@Id, @DocumentGUID, @Title, @Category, 'Paper', @FileName, @FullPath)", conn);
            //SqlParameter prmId = new SqlParameter() { ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = Id };
            SqlParameter prmDocumentGUID = new SqlParameter() { ParameterName = "@DocumentGUID", SqlDbType = SqlDbType.NVarChar, Value = DocumentGUId };
            SqlParameter prmTitle = new SqlParameter() { ParameterName = "@Title", SqlDbType = SqlDbType.NVarChar, Value = fileinfo.Directory.Name };
            SqlParameter prmCategory = new SqlParameter() { ParameterName = "@Category", SqlDbType = SqlDbType.NVarChar, Value = fileinfo.Directory.Parent.Name };
            SqlParameter prmFileName = new SqlParameter() { ParameterName = "@FileName", SqlDbType = SqlDbType.NVarChar, Value = fileinfo.Name };
            SqlParameter prmFullPath = new SqlParameter() { ParameterName = "@FullPath", SqlDbType = SqlDbType.NVarChar, Value = fileinfo.FullName };

            cmd.Parameters.Add(prmDocumentGUID);
            cmd.Parameters.Add(prmTitle);
            cmd.Parameters.Add(prmCategory);
            cmd.Parameters.Add(prmFileName);
            cmd.Parameters.Add(prmFullPath);

            StringBuilder str = new StringBuilder("");
            string format = "{0}: {1}";
            
            str.AppendLine(string.Format(format, "DocumentGUID", DocumentGUId));
            str.AppendLine(string.Format(format, "DocumentName", fileinfo.Name));
            str.AppendLine(string.Format(format, "DocumentPath", fileinfo.FullName));
            str.AppendLine(string.Format(format, "Title", fileinfo.Directory.Name));
            str.AppendLine(string.Format(format, "Category", fileinfo.Directory.Parent.Name));
            str.AppendLine(string.Format(format, "IssueYear", ""));
            str.AppendLine(string.Format(format, "Authers", ""));
            str.AppendLine(string.Format(format, "Publisher", ""));
            str.AppendLine(string.Format(format, "ReferenceType", "Paper"));
            File.WriteAllText(fileinfo.DirectoryName + "\\" + "MetaData " + DocumentGUId + ".txt", str.ToString());
            File.Create(fileinfo.DirectoryName + "\\" + "Abstract.txt");
            File.Create(fileinfo.DirectoryName + "\\" + "Keywords.txt");
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
        }
        public void CreateRepositryForEachDocument()
        {
            string mainPath = @"C:\Users\user\Desktop\Uniten\Research\Data\Systimatic LR\";
            string NewPath = @"C:\Users\user\Desktop\Uniten\Research\DataRepo\Systimatic LR\";
            var dirs = Directory.GetDirectories(mainPath);
            foreach (var dir in dirs)
            {
                ReadFolder(dir, NewPath);
            }
            
        }
        private void ReadFolder(string folderPath, string NewPath)
        {

            DirectoryInfo dir = new DirectoryInfo(folderPath);
            var files = dir.GetFiles("*.pdf");
            string NewFolderPath = NewPath + dir.Name;
            DirectoryInfo newDirectory = new DirectoryInfo(NewFolderPath);
            if (!newDirectory.Exists)
            {
                newDirectory = Directory.CreateDirectory(NewFolderPath);
            }
            foreach (var file in files)
            {
                var DocumentFolder = Directory.CreateDirectory(NewFolderPath + "\\" + System.IO.Path.GetFileNameWithoutExtension(file.Name));
                FileInfo newFileinfo;
                if ((DocumentFolder.Name + "\\" + file.Name).Length > 175)
                {
                    newFileinfo = file.CopyTo(DocumentFolder.FullName + "\\" + file.Name.Substring(0, Math.Abs(DocumentFolder.FullName.Length - 170)) + ".pdf");
                }
                else
                    newFileinfo = file.CopyTo(DocumentFolder.FullName + "\\" + file.Name);
                createMetadateWithDB(newFileinfo);
            }
        }
    }
}
