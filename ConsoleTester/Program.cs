﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SystematicLR.DocumentParser;
using System.IO;
namespace ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();            
            string pdfText = p.Run();        
        }
        private static void Log(string text)
        {
            string path = @"C:\EizFind Logs\EizFind.txt";
            
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(text);
                Console.WriteLine(text);
                writer.Close();
            }
        }
        


        
        private string Run()
        {
            // here is the output file containing the final results
            string path = @"C:\EizFind Logs\EizFind.txt";
            if (File.Exists(path))
                File.Delete(path);

            // Document PDF file name
            string inputdocument = "A Conceptual Framework for Searching Relevant Scientific Publications in Large Databases using Agent-based Text Mining.pdf";
            PdfReader reader = new PdfReader(@"TestFiles\"+ inputdocument);
            // create an instance from our parsing strategy 
            //TextWithFontExtractionStategy EziFindStrategy = new TextWithFontExtractionStategy();
            SystematicLR.Common.Document doc = new SystematicLR.Common.Document(1, Guid.NewGuid(), inputdocument);
            SystematicLRTextExtractionStategy EziFindStrategy = new SystematicLRTextExtractionStategy(doc);
            EziRenderListener sss = new EziRenderListener();

            // getting the document's pages count using iTextSharp
            int pageCount = reader.NumberOfPages;
            doc.PagesCount = pageCount;
            // passing the pages one by one to itextSharp engine along with our own parsing strategy
            for (int i = 1; i <= pageCount; i++)
            {
                //string text1= iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(reader, i);
                // iTextSharp.text.pdf.parser.PdfReaderContentParser ss = new PdfReaderContentParser(reader);
                //var sdds= ss.ProcessContent(i, sss);
               // EziFindStrategy = new TextWithFontExtractionStategy();
                string text = iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(reader, i, EziFindStrategy);
            //    var r=EziFindStrategy.getFactory().Analayize(ExtractingMode.ExtractKeywordsWithStemming);

                //string langName= DocumentParser.Utilities.IsEnglish(text);
            }
            doc.AnalyzeDocument();
            
            // getting the factory containing all texts and fonts, in order to start Analayzing and applying the algorithm 
            //var factory = EziFindStrategy.getFactory();
            //// call the factory with ExtractKeywords ExtractingMode
            //var results =factory.Analayize(ExtractingMode.ExtractKeywords);
            //Log(string.Format("input Document file Name: {0}", inputdocument));
            //Log("-----------------------------------------------------------");
            //Log("\n\n");
            //Log("1- First Experiment Extracting Top 10 KeyWords:");        
            //Log("\n");
            //int cnt = 1;
            //// printing the output to console and write them in the output file
            //foreach (var i in results.Take(10))
            //{
            //    Log(string.Format("\t{2}- {0} => {1}", i.Key, i.Value,cnt++));
            //}
            //// call the factory with ExtractKeywordsWithStemming ExtractingMode
            //results = factory.Analayize(ExtractingMode.ExtractKeywordsWithStemming);
            //Log("-----------------------------------------------------------");
            //Log("2- Second Experiment Extracting Top 10 KeyWords whith stemming:");
            //Log("\n");
            //cnt = 1;
            //foreach (var i in results.Take(10))
            //{
            //    Log(string.Format("\t{2}- {0} => {1}", i.Key, i.Value, cnt++));
            //}
            //// call the factory with ExtractTerms ExtractingMode
            //results = factory.Analayize(ExtractingMode.ExtractTerms);
            //Log("-----------------------------------------------------------");
            //Log("3- third Experiment Extracting Top 10 terms from the document:");
            //Log("\n");
            //cnt = 1;
            //foreach (var i in results.Take(10))
            //{
            //    Log(string.Format("\t{2}- {0} => {1}", i.Key, i.Value, cnt++));
            //}
            //Console.Read();
            //return factory.ToString();
            return null;
        }
        /// <summary>
        /// Reading text from PDF
        /// </summary>
        /// <returns></returns>
        private string GetTextFromPDF()
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(@"C:\Users\user\Desktop\Lucene_VS2012_Demo_App\Lucene\SimpleLuceneSearch\bin\Debug\Lucene\ICCCI 2014.pdf"))
            {
                List<object[]> strFonts = BaseFont.GetDocumentFonts(reader);

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }

            return text.ToString();
        }
        public void fff()
        {
            PdfReader reader = new PdfReader(@"C:\Users\user\Desktop\Lucene_VS2012_Demo_App\Lucene\SimpleLuceneSearch\bin\Debug\Lucene\ICCCI 2014.pdf");
            HashSet<String> names = new HashSet<string>();
            PdfDictionary resources;
            for (int p = 1; p <= reader.NumberOfPages; p++)
            {
                PdfDictionary dic = reader.GetPageN(p);
                resources = dic.GetAsDict(PdfName.RESOURCES);

                if (resources != null)
                {
                    //get fonts dictionary
                    PdfDictionary fonts = resources.GetAsDict(PdfName.FONT);
                    if (fonts != null)
                    {
                        PdfDictionary font;
                        foreach (PdfName key in fonts.Keys)
                        {
                            font = fonts.GetAsDict(key);
                            String name = font.GetAsName(PdfName.BASEFONT).ToString();

                            //check for prefix subsetted font
                            if (name.Length > 8 && name.ToCharArray()[7] == '+')
                            {
                                name = String.Format("%s subset (%s)", name.Substring(8), name.Substring(1, 7));

                            }
                            else
                            {
                                //get type of fully embeded fonts
                                name = name.Substring(1);
                                PdfDictionary desc = font.GetAsDict(PdfName.FONTDESCRIPTOR);
                                if (desc == null)
                                    name += " no font descriptor";
                                else if (desc.Get(PdfName.FONTFILE) != null)
                                    name += " (Type 1) embedded";
                                else if (desc.Get(PdfName.FONTFILE2) != null)
                                    name += " (TrueType) embedded";
                                else if (desc.Get(PdfName.FONTFILE3) != null)
                                    name += " (" + font.GetAsName(PdfName.SUBTYPE).ToString().Substring(1) + ") embedded";
                            }
                            names.Add(name);
                        }

                    }
                }
            }
            var collections = from name in names
                              select name;
            foreach (String fname in collections)
            {
                Console.WriteLine(fname);
            }
            Console.Read();

        }

        /// <summary>
        /// Reading Text from Word document
        /// </summary>
        /// <returns></returns>
        private string GetTextFromWord()
        {
            StringBuilder text = new StringBuilder();
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            object path = @"C:\Users\user\Desktop\Lucene_VS2012_Demo_App\Lucene\SimpleLuceneSearch\bin\Debug\Lucene\ICIMU-2017.docx";
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(" \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString());
            }

            return text.ToString();
        }

        /// <summary>
        /// Reading text from text files
        /// </summary>
        /// <returns></returns>
        private string GetTextFromText()
        {
            string text = System.IO.File.ReadAllText(@"D:\Articles2.txt");

            return text.ToString();
        }
    }
}
