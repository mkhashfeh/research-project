//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystematicLR.Entities.Model
{
    using System;
    
    public partial class GetReferencesByRepositoryGUID_Result
    {
        public System.Guid ReferenceGUID { get; set; }
        public string Title { get; set; }
        public Nullable<int> IssueYear { get; set; }
        public string Authers { get; set; }
        public string Publisher { get; set; }
        public string Country { get; set; }
        public string ReferenceType { get; set; }
        public Nullable<int> CiteCount { get; set; }
        public Nullable<int> VarsionsCount { get; set; }
        public string Category { get; set; }
        public string FromDatabase { get; set; }
        public Nullable<byte> UserRating { get; set; }
        public Nullable<byte> SystemRating { get; set; }
    }
}
