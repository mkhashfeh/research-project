﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystematicLR.Entities.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class StoredProcedure : DbContext
    {
        public StoredProcedure()
            : base("name=StoredProcedure")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual ObjectResult<LoginUser_Result> LoginUser(string email, string password)
        {
            var emailParameter = email != null ?
                new ObjectParameter("email", email) :
                new ObjectParameter("email", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<LoginUser_Result>("LoginUser", emailParameter, passwordParameter);
        }
    
        public virtual ObjectResult<GetRepositories_Result> GetRepositories(Nullable<int> userId)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRepositories_Result>("GetRepositories", userIdParameter);
        }
    
        public virtual ObjectResult<string> GetOptions(Nullable<int> repositoryId, string optionType, Nullable<int> top)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var optionTypeParameter = optionType != null ?
                new ObjectParameter("OptionType", optionType) :
                new ObjectParameter("OptionType", typeof(string));
    
            var topParameter = top.HasValue ?
                new ObjectParameter("top", top) :
                new ObjectParameter("top", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetOptions", repositoryIdParameter, optionTypeParameter, topParameter);
        }
    
        public virtual ObjectResult<string> GetReferencePathByGuid(Nullable<System.Guid> referenceGUID, Nullable<int> userId, Nullable<System.Guid> reposotoryGUID)
        {
            var referenceGUIDParameter = referenceGUID.HasValue ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(System.Guid));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var reposotoryGUIDParameter = reposotoryGUID.HasValue ?
                new ObjectParameter("ReposotoryGUID", reposotoryGUID) :
                new ObjectParameter("ReposotoryGUID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetReferencePathByGuid", referenceGUIDParameter, userIdParameter, reposotoryGUIDParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> AddNewReference(Nullable<System.Guid> referenceGUID, string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, Nullable<int> createdBy, Nullable<bool> foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, Nullable<bool> isAnalysed, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, Nullable<int> repositoryId, string category, string documentLocalPath, Nullable<bool> needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver)
        {
            var referenceGUIDParameter = referenceGUID.HasValue ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(System.Guid));
    
            var titleParameter = title != null ?
                new ObjectParameter("Title", title) :
                new ObjectParameter("Title", typeof(string));
    
            var issueYearParameter = issueYear.HasValue ?
                new ObjectParameter("IssueYear", issueYear) :
                new ObjectParameter("IssueYear", typeof(int));
    
            var authersParameter = authers != null ?
                new ObjectParameter("Authers", authers) :
                new ObjectParameter("Authers", typeof(string));
    
            var publisherParameter = publisher != null ?
                new ObjectParameter("Publisher", publisher) :
                new ObjectParameter("Publisher", typeof(string));
    
            var countryParameter = country != null ?
                new ObjectParameter("Country", country) :
                new ObjectParameter("Country", typeof(string));
    
            var referenceTypeParameter = referenceType != null ?
                new ObjectParameter("ReferenceType", referenceType) :
                new ObjectParameter("ReferenceType", typeof(string));
    
            var citeCountParameter = citeCount.HasValue ?
                new ObjectParameter("CiteCount", citeCount) :
                new ObjectParameter("CiteCount", typeof(int));
    
            var varsionsCountParameter = varsionsCount.HasValue ?
                new ObjectParameter("VarsionsCount", varsionsCount) :
                new ObjectParameter("VarsionsCount", typeof(int));
    
            var abstractParameter = @abstract != null ?
                new ObjectParameter("Abstract", @abstract) :
                new ObjectParameter("Abstract", typeof(string));
    
            var keywordsParameter = keywords != null ?
                new ObjectParameter("Keywords", keywords) :
                new ObjectParameter("Keywords", typeof(string));
    
            var documentPathParameter = documentPath != null ?
                new ObjectParameter("DocumentPath", documentPath) :
                new ObjectParameter("DocumentPath", typeof(string));
    
            var documentLinkParameter = documentLink != null ?
                new ObjectParameter("DocumentLink", documentLink) :
                new ObjectParameter("DocumentLink", typeof(string));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            var foundGSParameter = foundGS.HasValue ?
                new ObjectParameter("FoundGS", foundGS) :
                new ObjectParameter("FoundGS", typeof(bool));
    
            var gSCiteIdParameter = gSCiteId != null ?
                new ObjectParameter("GSCiteId", gSCiteId) :
                new ObjectParameter("GSCiteId", typeof(string));
    
            var gSSearchTextParameter = gSSearchText != null ?
                new ObjectParameter("GSSearchText", gSSearchText) :
                new ObjectParameter("GSSearchText", typeof(string));
    
            var gSHostSiteParameter = gSHostSite != null ?
                new ObjectParameter("GSHostSite", gSHostSite) :
                new ObjectParameter("GSHostSite", typeof(string));
    
            var gSDocumentLinkParameter = gSDocumentLink != null ?
                new ObjectParameter("GSDocumentLink", gSDocumentLink) :
                new ObjectParameter("GSDocumentLink", typeof(string));
    
            var isAnalysedParameter = isAnalysed.HasValue ?
                new ObjectParameter("IsAnalysed", isAnalysed) :
                new ObjectParameter("IsAnalysed", typeof(bool));
    
            var gSDocumentTypeParameter = gSDocumentType != null ?
                new ObjectParameter("GSDocumentType", gSDocumentType) :
                new ObjectParameter("GSDocumentType", typeof(string));
    
            var gSRelatedLinkParameter = gSRelatedLink != null ?
                new ObjectParameter("GSRelatedLink", gSRelatedLink) :
                new ObjectParameter("GSRelatedLink", typeof(string));
    
            var gSCiteLinkParameter = gSCiteLink != null ?
                new ObjectParameter("GSCiteLink", gSCiteLink) :
                new ObjectParameter("GSCiteLink", typeof(string));
    
            var gSVersionsParameter = gSVersions != null ?
                new ObjectParameter("GSVersions", gSVersions) :
                new ObjectParameter("GSVersions", typeof(string));
    
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var categoryParameter = category != null ?
                new ObjectParameter("Category", category) :
                new ObjectParameter("Category", typeof(string));
    
            var documentLocalPathParameter = documentLocalPath != null ?
                new ObjectParameter("DocumentLocalPath", documentLocalPath) :
                new ObjectParameter("DocumentLocalPath", typeof(string));
    
            var needAnalyseParameter = needAnalyse.HasValue ?
                new ObjectParameter("NeedAnalyse", needAnalyse) :
                new ObjectParameter("NeedAnalyse", typeof(bool));
    
            var fromDatabaseParameter = fromDatabase != null ?
                new ObjectParameter("FromDatabase", fromDatabase) :
                new ObjectParameter("FromDatabase", typeof(string));
    
            var aPAParameter = aPA != null ?
                new ObjectParameter("APA", aPA) :
                new ObjectParameter("APA", typeof(string));
    
            var chicagoParameter = chicago != null ?
                new ObjectParameter("Chicago", chicago) :
                new ObjectParameter("Chicago", typeof(string));
    
            var harvardParameter = harvard != null ?
                new ObjectParameter("Harvard", harvard) :
                new ObjectParameter("Harvard", typeof(string));
    
            var mLAParameter = mLA != null ?
                new ObjectParameter("MLA", mLA) :
                new ObjectParameter("MLA", typeof(string));
    
            var vancouverParameter = vancouver != null ?
                new ObjectParameter("Vancouver", vancouver) :
                new ObjectParameter("Vancouver", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("AddNewReference", referenceGUIDParameter, titleParameter, issueYearParameter, authersParameter, publisherParameter, countryParameter, referenceTypeParameter, citeCountParameter, varsionsCountParameter, abstractParameter, keywordsParameter, documentPathParameter, documentLinkParameter, createdByParameter, foundGSParameter, gSCiteIdParameter, gSSearchTextParameter, gSHostSiteParameter, gSDocumentLinkParameter, isAnalysedParameter, gSDocumentTypeParameter, gSRelatedLinkParameter, gSCiteLinkParameter, gSVersionsParameter, repositoryIdParameter, categoryParameter, documentLocalPathParameter, needAnalyseParameter, fromDatabaseParameter, aPAParameter, chicagoParameter, harvardParameter, mLAParameter, vancouverParameter);
        }
    
        public virtual ObjectResult<GetReferenceByGuid_Result> GetReferenceByGuid(Nullable<System.Guid> referenceGUID, Nullable<int> userId, Nullable<System.Guid> reposotoryGUID)
        {
            var referenceGUIDParameter = referenceGUID.HasValue ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(System.Guid));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var reposotoryGUIDParameter = reposotoryGUID.HasValue ?
                new ObjectParameter("ReposotoryGUID", reposotoryGUID) :
                new ObjectParameter("ReposotoryGUID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetReferenceByGuid_Result>("GetReferenceByGuid", referenceGUIDParameter, userIdParameter, reposotoryGUIDParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> ChangeBookMark(Nullable<System.Guid> referenceGuid, Nullable<System.Guid> repositoryGuid, Nullable<bool> value)
        {
            var referenceGuidParameter = referenceGuid.HasValue ?
                new ObjectParameter("ReferenceGuid", referenceGuid) :
                new ObjectParameter("ReferenceGuid", typeof(System.Guid));
    
            var repositoryGuidParameter = repositoryGuid.HasValue ?
                new ObjectParameter("RepositoryGuid", repositoryGuid) :
                new ObjectParameter("RepositoryGuid", typeof(System.Guid));
    
            var valueParameter = value.HasValue ?
                new ObjectParameter("value", value) :
                new ObjectParameter("value", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("ChangeBookMark", referenceGuidParameter, repositoryGuidParameter, valueParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> ChangeIsInUse(Nullable<System.Guid> referenceGuid, Nullable<System.Guid> repositoryGuid, Nullable<bool> value)
        {
            var referenceGuidParameter = referenceGuid.HasValue ?
                new ObjectParameter("ReferenceGuid", referenceGuid) :
                new ObjectParameter("ReferenceGuid", typeof(System.Guid));
    
            var repositoryGuidParameter = repositoryGuid.HasValue ?
                new ObjectParameter("RepositoryGuid", repositoryGuid) :
                new ObjectParameter("RepositoryGuid", typeof(System.Guid));
    
            var valueParameter = value.HasValue ?
                new ObjectParameter("value", value) :
                new ObjectParameter("value", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("ChangeIsInUse", referenceGuidParameter, repositoryGuidParameter, valueParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> EditReference(Nullable<System.Guid> referenceGUID, string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, Nullable<int> createdBy, Nullable<bool> foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, Nullable<bool> isAnalysed, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, Nullable<int> repositoryId, string category, string documentLocalPath, Nullable<bool> needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver, Nullable<bool> updateFile)
        {
            var referenceGUIDParameter = referenceGUID.HasValue ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(System.Guid));
    
            var titleParameter = title != null ?
                new ObjectParameter("Title", title) :
                new ObjectParameter("Title", typeof(string));
    
            var issueYearParameter = issueYear.HasValue ?
                new ObjectParameter("IssueYear", issueYear) :
                new ObjectParameter("IssueYear", typeof(int));
    
            var authersParameter = authers != null ?
                new ObjectParameter("Authers", authers) :
                new ObjectParameter("Authers", typeof(string));
    
            var publisherParameter = publisher != null ?
                new ObjectParameter("Publisher", publisher) :
                new ObjectParameter("Publisher", typeof(string));
    
            var countryParameter = country != null ?
                new ObjectParameter("Country", country) :
                new ObjectParameter("Country", typeof(string));
    
            var referenceTypeParameter = referenceType != null ?
                new ObjectParameter("ReferenceType", referenceType) :
                new ObjectParameter("ReferenceType", typeof(string));
    
            var citeCountParameter = citeCount.HasValue ?
                new ObjectParameter("CiteCount", citeCount) :
                new ObjectParameter("CiteCount", typeof(int));
    
            var varsionsCountParameter = varsionsCount.HasValue ?
                new ObjectParameter("VarsionsCount", varsionsCount) :
                new ObjectParameter("VarsionsCount", typeof(int));
    
            var abstractParameter = @abstract != null ?
                new ObjectParameter("Abstract", @abstract) :
                new ObjectParameter("Abstract", typeof(string));
    
            var keywordsParameter = keywords != null ?
                new ObjectParameter("Keywords", keywords) :
                new ObjectParameter("Keywords", typeof(string));
    
            var documentPathParameter = documentPath != null ?
                new ObjectParameter("DocumentPath", documentPath) :
                new ObjectParameter("DocumentPath", typeof(string));
    
            var documentLinkParameter = documentLink != null ?
                new ObjectParameter("DocumentLink", documentLink) :
                new ObjectParameter("DocumentLink", typeof(string));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            var foundGSParameter = foundGS.HasValue ?
                new ObjectParameter("FoundGS", foundGS) :
                new ObjectParameter("FoundGS", typeof(bool));
    
            var gSCiteIdParameter = gSCiteId != null ?
                new ObjectParameter("GSCiteId", gSCiteId) :
                new ObjectParameter("GSCiteId", typeof(string));
    
            var gSSearchTextParameter = gSSearchText != null ?
                new ObjectParameter("GSSearchText", gSSearchText) :
                new ObjectParameter("GSSearchText", typeof(string));
    
            var gSHostSiteParameter = gSHostSite != null ?
                new ObjectParameter("GSHostSite", gSHostSite) :
                new ObjectParameter("GSHostSite", typeof(string));
    
            var gSDocumentLinkParameter = gSDocumentLink != null ?
                new ObjectParameter("GSDocumentLink", gSDocumentLink) :
                new ObjectParameter("GSDocumentLink", typeof(string));
    
            var isAnalysedParameter = isAnalysed.HasValue ?
                new ObjectParameter("IsAnalysed", isAnalysed) :
                new ObjectParameter("IsAnalysed", typeof(bool));
    
            var gSDocumentTypeParameter = gSDocumentType != null ?
                new ObjectParameter("GSDocumentType", gSDocumentType) :
                new ObjectParameter("GSDocumentType", typeof(string));
    
            var gSRelatedLinkParameter = gSRelatedLink != null ?
                new ObjectParameter("GSRelatedLink", gSRelatedLink) :
                new ObjectParameter("GSRelatedLink", typeof(string));
    
            var gSCiteLinkParameter = gSCiteLink != null ?
                new ObjectParameter("GSCiteLink", gSCiteLink) :
                new ObjectParameter("GSCiteLink", typeof(string));
    
            var gSVersionsParameter = gSVersions != null ?
                new ObjectParameter("GSVersions", gSVersions) :
                new ObjectParameter("GSVersions", typeof(string));
    
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var categoryParameter = category != null ?
                new ObjectParameter("Category", category) :
                new ObjectParameter("Category", typeof(string));
    
            var documentLocalPathParameter = documentLocalPath != null ?
                new ObjectParameter("DocumentLocalPath", documentLocalPath) :
                new ObjectParameter("DocumentLocalPath", typeof(string));
    
            var needAnalyseParameter = needAnalyse.HasValue ?
                new ObjectParameter("NeedAnalyse", needAnalyse) :
                new ObjectParameter("NeedAnalyse", typeof(bool));
    
            var fromDatabaseParameter = fromDatabase != null ?
                new ObjectParameter("FromDatabase", fromDatabase) :
                new ObjectParameter("FromDatabase", typeof(string));
    
            var aPAParameter = aPA != null ?
                new ObjectParameter("APA", aPA) :
                new ObjectParameter("APA", typeof(string));
    
            var chicagoParameter = chicago != null ?
                new ObjectParameter("Chicago", chicago) :
                new ObjectParameter("Chicago", typeof(string));
    
            var harvardParameter = harvard != null ?
                new ObjectParameter("Harvard", harvard) :
                new ObjectParameter("Harvard", typeof(string));
    
            var mLAParameter = mLA != null ?
                new ObjectParameter("MLA", mLA) :
                new ObjectParameter("MLA", typeof(string));
    
            var vancouverParameter = vancouver != null ?
                new ObjectParameter("Vancouver", vancouver) :
                new ObjectParameter("Vancouver", typeof(string));
    
            var updateFileParameter = updateFile.HasValue ?
                new ObjectParameter("updateFile", updateFile) :
                new ObjectParameter("updateFile", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("EditReference", referenceGUIDParameter, titleParameter, issueYearParameter, authersParameter, publisherParameter, countryParameter, referenceTypeParameter, citeCountParameter, varsionsCountParameter, abstractParameter, keywordsParameter, documentPathParameter, documentLinkParameter, createdByParameter, foundGSParameter, gSCiteIdParameter, gSSearchTextParameter, gSHostSiteParameter, gSDocumentLinkParameter, isAnalysedParameter, gSDocumentTypeParameter, gSRelatedLinkParameter, gSCiteLinkParameter, gSVersionsParameter, repositoryIdParameter, categoryParameter, documentLocalPathParameter, needAnalyseParameter, fromDatabaseParameter, aPAParameter, chicagoParameter, harvardParameter, mLAParameter, vancouverParameter, updateFileParameter);
        }
    
        public virtual ObjectResult<AggByCites_Result> AggByCites(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByCites_Result>("AggByCites", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<AggByCountry_Result> AggByCountry(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByCountry_Result>("AggByCountry", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<AggByPublisher_Result> AggByPublisher(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByPublisher_Result>("AggByPublisher", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<AggByReferanceType_Result> AggByReferanceType(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByReferanceType_Result>("AggByReferanceType", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<AggByVersions_Result> AggByVersions(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByVersions_Result>("AggByVersions", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<AggByYear_Result> AggByYear(Nullable<int> repositoryId, Nullable<int> userId)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<AggByYear_Result>("AggByYear", repositoryIdParameter, userIdParameter);
        }
    
        public virtual ObjectResult<GetReferencesByRepositoryGUID_Result> GetReferencesByRepositoryGUID(Nullable<int> userId, Nullable<System.Guid> reposotoryGUID, string title, string category, Nullable<System.Guid> referenceGUID, Nullable<bool> bookmark, Nullable<bool> inUse)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var reposotoryGUIDParameter = reposotoryGUID.HasValue ?
                new ObjectParameter("ReposotoryGUID", reposotoryGUID) :
                new ObjectParameter("ReposotoryGUID", typeof(System.Guid));
    
            var titleParameter = title != null ?
                new ObjectParameter("Title", title) :
                new ObjectParameter("Title", typeof(string));
    
            var categoryParameter = category != null ?
                new ObjectParameter("Category", category) :
                new ObjectParameter("Category", typeof(string));
    
            var referenceGUIDParameter = referenceGUID.HasValue ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(System.Guid));
    
            var bookmarkParameter = bookmark.HasValue ?
                new ObjectParameter("bookmark", bookmark) :
                new ObjectParameter("bookmark", typeof(bool));
    
            var inUseParameter = inUse.HasValue ?
                new ObjectParameter("InUse", inUse) :
                new ObjectParameter("InUse", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetReferencesByRepositoryGUID_Result>("GetReferencesByRepositoryGUID", userIdParameter, reposotoryGUIDParameter, titleParameter, categoryParameter, referenceGUIDParameter, bookmarkParameter, inUseParameter);
        }
    
        public virtual ObjectResult<GenerateReference_Result> GenerateReference(Nullable<int> userId, Nullable<System.Guid> reposotoryGUID)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var reposotoryGUIDParameter = reposotoryGUID.HasValue ?
                new ObjectParameter("ReposotoryGUID", reposotoryGUID) :
                new ObjectParameter("ReposotoryGUID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GenerateReference_Result>("GenerateReference", userIdParameter, reposotoryGUIDParameter);
        }
    
        public virtual ObjectResult<GetAnalyzeQ_Result> GetAnalyzeQ()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAnalyzeQ_Result>("GetAnalyzeQ");
        }
    
        public virtual ObjectResult<GetReferenceByGuid_Result> GetReferenceByShortGuid(string referenceGUID, Nullable<int> userId, Nullable<System.Guid> reposotoryGUID)
        {
            var referenceGUIDParameter = referenceGUID != null ?
                new ObjectParameter("ReferenceGUID", referenceGUID) :
                new ObjectParameter("ReferenceGUID", typeof(string));
    
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var reposotoryGUIDParameter = reposotoryGUID.HasValue ?
                new ObjectParameter("ReposotoryGUID", reposotoryGUID) :
                new ObjectParameter("ReposotoryGUID", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetReferenceByGuid_Result>("GetReferenceByShortGuid", referenceGUIDParameter, userIdParameter, reposotoryGUIDParameter);
        }
    
        public virtual ObjectResult<GetSimilarity_Result> GetSimilarity(Nullable<int> repositoryId, string keyword1, string keyword2, string keyword3, string keyword4, string keyword5, Nullable<double> threshold1, Nullable<double> threshold2, Nullable<double> threshold3, Nullable<double> threshold4, Nullable<double> threshold5)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var keyword1Parameter = keyword1 != null ?
                new ObjectParameter("keyword1", keyword1) :
                new ObjectParameter("keyword1", typeof(string));
    
            var keyword2Parameter = keyword2 != null ?
                new ObjectParameter("keyword2", keyword2) :
                new ObjectParameter("keyword2", typeof(string));
    
            var keyword3Parameter = keyword3 != null ?
                new ObjectParameter("keyword3", keyword3) :
                new ObjectParameter("keyword3", typeof(string));
    
            var keyword4Parameter = keyword4 != null ?
                new ObjectParameter("keyword4", keyword4) :
                new ObjectParameter("keyword4", typeof(string));
    
            var keyword5Parameter = keyword5 != null ?
                new ObjectParameter("keyword5", keyword5) :
                new ObjectParameter("keyword5", typeof(string));
    
            var threshold1Parameter = threshold1.HasValue ?
                new ObjectParameter("Threshold1", threshold1) :
                new ObjectParameter("Threshold1", typeof(double));
    
            var threshold2Parameter = threshold2.HasValue ?
                new ObjectParameter("Threshold2", threshold2) :
                new ObjectParameter("Threshold2", typeof(double));
    
            var threshold3Parameter = threshold3.HasValue ?
                new ObjectParameter("Threshold3", threshold3) :
                new ObjectParameter("Threshold3", typeof(double));
    
            var threshold4Parameter = threshold4.HasValue ?
                new ObjectParameter("Threshold4", threshold4) :
                new ObjectParameter("Threshold4", typeof(double));
    
            var threshold5Parameter = threshold5.HasValue ?
                new ObjectParameter("Threshold5", threshold5) :
                new ObjectParameter("Threshold5", typeof(double));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSimilarity_Result>("GetSimilarity", repositoryIdParameter, keyword1Parameter, keyword2Parameter, keyword3Parameter, keyword4Parameter, keyword5Parameter, threshold1Parameter, threshold2Parameter, threshold3Parameter, threshold4Parameter, threshold5Parameter);
        }
    
        public virtual ObjectResult<GetSimilarityTFBMTF_Result> GetSimilarityTFBMTF(Nullable<int> repositoryId, string keyword1, string keyword2, string keyword3, string keyword4, string keyword5, string keyword6, Nullable<double> threshold1, Nullable<double> threshold2, Nullable<double> threshold3, Nullable<double> threshold4, Nullable<double> threshold5, Nullable<double> threshold6)
        {
            var repositoryIdParameter = repositoryId.HasValue ?
                new ObjectParameter("RepositoryId", repositoryId) :
                new ObjectParameter("RepositoryId", typeof(int));
    
            var keyword1Parameter = keyword1 != null ?
                new ObjectParameter("keyword1", keyword1) :
                new ObjectParameter("keyword1", typeof(string));
    
            var keyword2Parameter = keyword2 != null ?
                new ObjectParameter("keyword2", keyword2) :
                new ObjectParameter("keyword2", typeof(string));
    
            var keyword3Parameter = keyword3 != null ?
                new ObjectParameter("keyword3", keyword3) :
                new ObjectParameter("keyword3", typeof(string));
    
            var keyword4Parameter = keyword4 != null ?
                new ObjectParameter("keyword4", keyword4) :
                new ObjectParameter("keyword4", typeof(string));
    
            var keyword5Parameter = keyword5 != null ?
                new ObjectParameter("keyword5", keyword5) :
                new ObjectParameter("keyword5", typeof(string));
    
            var keyword6Parameter = keyword6 != null ?
                new ObjectParameter("keyword6", keyword6) :
                new ObjectParameter("keyword6", typeof(string));
    
            var threshold1Parameter = threshold1.HasValue ?
                new ObjectParameter("Threshold1", threshold1) :
                new ObjectParameter("Threshold1", typeof(double));
    
            var threshold2Parameter = threshold2.HasValue ?
                new ObjectParameter("Threshold2", threshold2) :
                new ObjectParameter("Threshold2", typeof(double));
    
            var threshold3Parameter = threshold3.HasValue ?
                new ObjectParameter("Threshold3", threshold3) :
                new ObjectParameter("Threshold3", typeof(double));
    
            var threshold4Parameter = threshold4.HasValue ?
                new ObjectParameter("Threshold4", threshold4) :
                new ObjectParameter("Threshold4", typeof(double));
    
            var threshold5Parameter = threshold5.HasValue ?
                new ObjectParameter("Threshold5", threshold5) :
                new ObjectParameter("Threshold5", typeof(double));
    
            var threshold6Parameter = threshold6.HasValue ?
                new ObjectParameter("Threshold6", threshold6) :
                new ObjectParameter("Threshold6", typeof(double));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSimilarityTFBMTF_Result>("GetSimilarityTFBMTF", repositoryIdParameter, keyword1Parameter, keyword2Parameter, keyword3Parameter, keyword4Parameter, keyword5Parameter, keyword6Parameter, threshold1Parameter, threshold2Parameter, threshold3Parameter, threshold4Parameter, threshold5Parameter, threshold6Parameter);
        }
    }
}
