﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Entities
{
    public class AggResult
    {
        public AggResult(int value,string name)
        {
            this.Value = value;
            this.Name = name;

        }
        public int Value { get; set; }
        public string Name { get; set; }
    }
}
