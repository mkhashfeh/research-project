﻿namespace SystematicLR.Entities.Model
{
    using System;

    public partial class GetSimilarityHeaders
    {
        public Header Header1 { set; get; }
        public Header Header2 { set; get; }
        public Header Header3 { set; get; }
        public Header Header4 { set; get; }
        public Header Header5 { set; get; }
        public Header Header6 { set; get; }        
    }

    public class Header
    {
        public Header()
        {

        }
        public Header(string OriginalName, string stemName, double? threshold)
        {
            this.OriginalName = OriginalName;
            this.stemName = stemName;
            this.threshold = threshold;
        }
        public string OriginalName { set; get; }
        public string stemName { set; get; }
        public double? threshold { set; get; }
    }
}


