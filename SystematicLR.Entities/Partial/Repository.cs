﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Entities.Model
{
  

    public partial class Repository
    {
        internal Repository(GetRepositories_Result results)
        {
            this.CreatedByUser = results.DisplayName;
            this.CreationDate = results.CreationDate;
            this.RepositoryDesc = results.RepositoryDesc;
            this.RepositoryId = results.RepositoryId;
            this.RepositoryName = results.RepositoryName;
            this.ReposotoryGUID = results.ReposotoryGUID;
           
        }
        public string CreatedByUser { set; get; }        
    }
}
