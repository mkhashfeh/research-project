﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SystematicLR.Entities.Model
{
    public partial class User
    {
        
        public User(LoginUser_Result logininfo)
        {
            this.DisplayName = logininfo.DisplayName;
            this.Email = logininfo.Email;
            this.LastLogin = logininfo.LastLogin;                       
            this.IsAdmin = logininfo.IsAdmin;
            this.UserId = logininfo.UserId;
            this.IsUserLocked = logininfo.isUserLocked;
            this.Status = logininfo.Status;
            
        }        
        public bool IsUserAdmin
        {
            get
            {
                return this.IsAdmin.HasValue && this.IsAdmin.Value ;
            }
        }
    }
}
