﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.Entities.Model
{
    public class SimilarityRow
    {       
            public Nullable<int> DocId { get; set; }
            public List<double?> keywords { get; set; }            
            public Nullable<double> Length { get; set; }
            public string Title { get; set; }
            public int ReferenceId { get; set; }
            public Guid ReferenceGUID { get; set; }        
    }
    public interface ISimilarityResults
    {
        List<Header> GetHeaders();
        List<SimilarityRow> GetRows();

    }
    public class SimilarityResults :ISimilarityResults
    {
        public SimilarityResults()
        {
            Headers = new GetSimilarityHeaders();            
        }
        public List<Header> GetHeaders()
        {
            if (Keywords != null)
                return Keywords;
            Keywords = new List<Header>();
            if (Headers.Header1.OriginalName != null)
                Keywords.Add(Headers.Header1);
            if (Headers.Header2.OriginalName != null)
                Keywords.Add(Headers.Header2);
            if (Headers.Header3.OriginalName != null)
                Keywords.Add(Headers.Header3);
            if (Headers.Header4.OriginalName != null)
                Keywords.Add(Headers.Header4);
            if (Headers.Header5.OriginalName != null)
                Keywords.Add(Headers.Header5);          
            return Keywords;
        }
        public List<SimilarityRow> GetRows()
        {
            if (rows != null)
                return rows;
            rows = new List<SimilarityRow>();
            rows.AddRange(Data.Select(e => new SimilarityRow() { DocId = e.DocId, Length = e.Length, ReferenceGUID = e.ReferenceGUID, ReferenceId = e.ReferenceId, Title = e.Title, keywords = new List<double?> { e.keyword1, e.keyword2, e.keyword3, e.keyword4, e.keyword5} }));
            return rows;          
        }
        private List<SimilarityRow> rows;
        private List<Header> Keywords { set; get; }
        public List<GetSimilarity_Result> Data{set;get;}
        public GetSimilarityHeaders Headers { set; get; }
    }
    public class SimilarityTFBMTF_Results : ISimilarityResults
    {
        public SimilarityTFBMTF_Results()
        {
            Headers = new GetSimilarityHeaders();
        }
        public List<Header> GetHeaders()
        {
            if (Keywords != null)
                return Keywords;
            Keywords = new List<Header>();
            if (Headers.Header1.OriginalName != null)
                Keywords.Add(Headers.Header1);
            if (Headers.Header2.OriginalName != null)
                Keywords.Add(Headers.Header2);
            if (Headers.Header3.OriginalName != null)
                Keywords.Add(Headers.Header3);
            if (Headers.Header4.OriginalName != null)
                Keywords.Add(Headers.Header4);
            if (Headers.Header5.OriginalName != null)
                Keywords.Add(Headers.Header5);
            if (Headers.Header6.OriginalName != null)
                Keywords.Add(Headers.Header6);
            return Keywords;
        }
        public List<GetSimilarityTFBMTF_Result> Data { set; get; }
        public GetSimilarityHeaders Headers { set; get; }
        private List<SimilarityRow> rows;
        private List<Header> Keywords { set; get; }
        public List<SimilarityRow> GetRows()
        {
            if (rows != null)
                return rows;
            rows = new List<SimilarityRow>();
            rows.AddRange(Data.Select(e => new SimilarityRow() { DocId = e.DocId, Length = e.Length, ReferenceGUID = e.ReferenceGUID, ReferenceId = e.ReferenceId, Title = e.Title, keywords = new List<double?> { e.keyword1, e.keyword2, e.keyword3, e.keyword4, e.keyword5,e.keyword6 } }));
            return rows;
        }
    }
}
