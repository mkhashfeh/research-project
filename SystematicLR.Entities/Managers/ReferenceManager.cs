﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities.Model;

namespace SystematicLR.Core.Managers
{
    public class ReferenceManager
    {
        public static string AddNewReference(string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, int createdBy, bool foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, int repositoryId, string category, string documentLocalPath, bool needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver) =>  SystematicLR.Entities.DBServices.AddNewReference(title, issueYear, authers, publisher, country, referenceType, citeCount, varsionsCount, @abstract, keywords, documentPath, documentLink, createdBy, foundGS, gSCiteId, gSSearchText, gSHostSite, gSDocumentLink, gSDocumentType, gSRelatedLink, gSCiteLink, gSVersions, repositoryId, category, documentLocalPath, needAnalyse, fromDatabase,  aPA,  chicago,  harvard,  mLA,  vancouver);

        public static GetReferenceByGuid_Result GetReferenceByGuid(Guid referenceGUID, int userId, Guid reposotoryGUID)
        => SystematicLR.Entities.DBServices.GetReferenceByGuid(referenceGUID, userId, reposotoryGUID);



        public static string EditReference(Guid ReferenceGuid, string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, int createdBy, bool foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, int repositoryId, string category, string documentLocalPath, bool needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver,bool UpdateFile) => SystematicLR.Entities.DBServices.EditReference(ReferenceGuid,title, issueYear, authers, publisher, country, referenceType, citeCount, varsionsCount, @abstract, keywords, documentPath, documentLink, createdBy, foundGS, gSCiteId, gSSearchText, gSHostSite, gSDocumentLink, gSDocumentType, gSRelatedLink, gSCiteLink, gSVersions, repositoryId, category, documentLocalPath, needAnalyse, fromDatabase, aPA, chicago, harvard, mLA, vancouver, UpdateFile);


    }
}
