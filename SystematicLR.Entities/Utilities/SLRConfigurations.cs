﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystematicLR.Entities.Model;
namespace SystematicLR.Entities.Utilities
{
   
    public class SLRConfigurations
    {
        private object parse(Configuration config)
        {
            bool tempbool;
            int tempint;
            TimeSpan tempTime;
            switch(config.ConfigType)
            {
                case "bool": { if (!bool.TryParse(config.Value, out tempbool)) return (Nullable<bool>)null; return tempbool; }
                case "int": { if (!int.TryParse(config.Value, out tempint)) return (Nullable<int>)null; return tempint; }
                case "string": { return config.Value; }
                case "TimeSpan": { if (!TimeSpan.TryParse(config.Value, out tempTime)) return (Nullable<TimeSpan>)null; return tempTime; }
                default:
                    {
                        return config.Value;
                    }
            }
        }
        public void AssignValue(Configuration config)
        {
            switch (config.Name)
            {
                case "EmailClientPort": { EmailClientPort = (int)parse(config); break; }
                case "EmailEnableSSL": { EmailEnableSSL = (bool)parse(config); break; }
                case "EmailSmtpClient": { EmailSmtpClient = (string)parse(config); break; }
                
                
                case "LoginAttempt": { LoginAttempt = (int)parse(config); break; }
                
                
                case "SendFromEmail": { SendFromEmail = (string)parse(config); break; }
                case "SendFromEmailPassword": { SendFromEmailPassword = (string)parse(config); break; }
                
                case "EmailTemplatesPath": { EmailTemplatesPath = (string)parse(config); break; }
                
                default:
                    {
                        break;
                    }
            }
        }
       
        
        public int EmailClientPort { set; get; }
        public bool EmailEnableSSL { set; get; }
        public string EmailSmtpClient { set; get; }
        
        
        public int LoginAttempt { set; get; }
        
        
        public string SendFromEmail { set; get; }
        public string SendFromEmailPassword { set; get; }
        
        
        public string EmailTemplatesPath { set; get; }
        
        
    }
}
