﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Data.SqlClient;
using SystematicLR.Entities.Model;
using SystematicLR.Entities.Utilities;



namespace SystematicLR.Entities
{
    public class DBServices
    {

        public static SmtpClient GetEmailConfiguration(SLRConfigurations configs = null)
        {
            if (configs == null)
            {
                configs = DBServices.GetConfigurations();
            }
            var smtp = new SmtpClient();
            var UserNameConfig = configs.SendFromEmail;
            var PasswordConfig = configs.SendFromEmailPassword;
            string Error = "";
            if (string.IsNullOrEmpty(UserNameConfig))
            {
                Error += "Email UserName ,";
            }
            if (string.IsNullOrEmpty(PasswordConfig))
            {
                Error += "Email Password ,";
            }

            var credential = new System.Net.NetworkCredential
            {
                UserName = UserNameConfig,
                Password = PasswordConfig,

            };
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = credential;
            var EmailSmtpClientConfig = configs.EmailSmtpClient;
            var EmailClientPortConfig = configs.EmailClientPort;
            if (string.IsNullOrEmpty(EmailSmtpClientConfig))
            {
                Error += "Email SmtpClient ,";
            }

            if (EmailClientPortConfig == default(int))
            {
                Error += "Email Client Port ,";
            }
            smtp.Host = EmailSmtpClientConfig;
            smtp.Port = EmailClientPortConfig;
            var EnableSslConfig = configs.EmailEnableSSL;


            smtp.EnableSsl = EnableSslConfig;
            if (string.IsNullOrEmpty(Error))
                return smtp;
            else
            {
                Error += " are invalid";
                throw new Exception(Error);
            }
        }
        private static SLRConfigurations configs;
        public static SLRConfigurations GetConfigurations()
        {
            if (configs != null)
                return configs;
            SystematicLR.Entities.Model.SystematicLREntities context = new SystematicLR.Entities.Model.SystematicLREntities();
            var listConfig = context.Configurations.ToList();

            configs = new SLRConfigurations();
            foreach (var item in listConfig)
            {
                configs.AssignValue(item);
            }
            return configs;

        }
        public static void ClearConfigs()
        {
            configs = null;
        }



        public static User LoginUser(string Email, string Password)
        {
            User results = null;
            using (StoredProcedure db = new StoredProcedure())
            {

                var logininfo = db.LoginUser(Email, Password).FirstOrDefault();
                if (logininfo != null)
                {
                    results = new User(logininfo);
                }
            }
            return results;
        }
        public static List<Repository> GetRepositories(int userId)
        {
            List<Repository> results = new List<Repository>();
            using (StoredProcedure db = new StoredProcedure())
            {

                db.GetRepositories(userId).ToList().ForEach((e) => { results.Add(new Repository(e)); });
            }
            return results;
        }
        public static string[] GetOptions(int repositoryId, string type, int top = 0)
        {
            string[] results = null;
            using (StoredProcedure db = new StoredProcedure())
            {

                results = db.GetOptions(repositoryId, type, top).ToArray();
            }
            return results;
        }
        public static string AddNewReference(string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, int createdBy, bool foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, int repositoryId, string category, string documentLocalPath, bool needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver,Guid? ReferenceGUID = null)
        {
            ReferenceGUID = ReferenceGUID==null? Guid.NewGuid():ReferenceGUID;
            int? referenceId = null;
            using (StoredProcedure db = new StoredProcedure())
            {

                referenceId = db.AddNewReference(ReferenceGUID, title, issueYear, authers, publisher, country, referenceType, citeCount, varsionsCount, @abstract, keywords, documentPath, documentLink, createdBy, foundGS, gSCiteId, gSSearchText, gSHostSite, gSDocumentLink, false, gSDocumentType, gSRelatedLink, gSCiteLink, gSVersions, repositoryId, category, documentLocalPath, needAnalyse, fromDatabase, aPA,  chicago,  harvard, mLA, vancouver).FirstOrDefault();
            }
            return referenceId.HasValue ? ReferenceGUID.ToString() : null;

        }
        public static string EditReference(Guid ReferenceGUID,  string title, Nullable<int> issueYear, string authers, string publisher, string country, string referenceType, Nullable<int> citeCount, Nullable<int> varsionsCount, string @abstract, string keywords, string documentPath, string documentLink, int createdBy, bool foundGS, string gSCiteId, string gSSearchText, string gSHostSite, string gSDocumentLink, string gSDocumentType, string gSRelatedLink, string gSCiteLink, string gSVersions, int repositoryId, string category, string documentLocalPath, bool needAnalyse, string fromDatabase, string aPA, string chicago, string harvard, string mLA, string vancouver,bool updateFile)
        {            
            int? referenceId = null;
            using (StoredProcedure db = new StoredProcedure())
            {

                referenceId = db.EditReference(ReferenceGUID, title, issueYear, authers, publisher, country, referenceType, citeCount, varsionsCount, @abstract, keywords, documentPath, documentLink, createdBy, foundGS, gSCiteId, gSSearchText, gSHostSite, gSDocumentLink, false, gSDocumentType, gSRelatedLink, gSCiteLink, gSVersions, repositoryId, category, documentLocalPath, needAnalyse, fromDatabase, aPA, chicago, harvard, mLA, vancouver, updateFile).FirstOrDefault();
            }
            return referenceId.HasValue ? ReferenceGUID.ToString() : null;


        }
        public static GetReferenceByGuid_Result GetReferenceByGuid(Guid referenceGUID, int userId, Guid reposotoryGUID)
        {
            GetReferenceByGuid_Result result = new GetReferenceByGuid_Result();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.GetReferenceByGuid(referenceGUID, userId, reposotoryGUID).FirstOrDefault();
            }
            return result;
        }
        public static GetReferenceByGuid_Result GetReferenceByGuid(string referenceGUID, int userId, Guid reposotoryGUID)
        {
            GetReferenceByGuid_Result result = new GetReferenceByGuid_Result();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.GetReferenceByShortGuid(referenceGUID, userId, reposotoryGUID).FirstOrDefault();
            }
            return result;
        }
        public static string GetReferencePathByGuid(Guid referenceGUID, int userId, Guid reposotoryGUID)
        {
            string result = null;
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.GetReferencePathByGuid(referenceGUID, userId, reposotoryGUID).FirstOrDefault();
            }
            return result;
        }
        public static List<GetReferencesByRepositoryGUID_Result> GetReferencesByRepositoryGUID( int userId, Guid reposotoryGUID, string title, string category, Guid? referenceGUID,bool? inUse,bool? bookmark)
        {
            List <GetReferencesByRepositoryGUID_Result> result = new List<GetReferencesByRepositoryGUID_Result>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.GetReferencesByRepositoryGUID(userId, reposotoryGUID,title,category,referenceGUID,bookmark,inUse).ToList();
            }
            return result;
        }

        public static bool ChangeBookMark(Guid referenceGuid, Guid repositoryGuid, bool value)
        {
            bool? result = null;
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.ChangeBookMark(referenceGuid, repositoryGuid, value).FirstOrDefault();
            }
            return result.Value;
        }
        public static bool ChangeIsInUse(Guid referenceGuid, Guid repositoryGuid, bool value)
        {
            bool? result = null;
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.ChangeIsInUse(referenceGuid, repositoryGuid, value).FirstOrDefault();
            }
            return result.Value;
        }
        public static List<AggResult> AggByCites(int RepositoryId,int UserId)
        {
            List<AggResult> result = new List<AggResult>() ;
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByCites(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue ? e.CNT.Value : 0, e.Cites)).ToList();
            }
            return result;

        }
        public static List<AggResult> AggByCountry(int RepositoryId, int UserId)
        {
            List<AggResult> result = new List<AggResult>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByCountry(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue ? e.CNT.Value : 0, e.Country)).ToList();
            }
            return result;

        }
        public static List<AggResult> AggByPublisher(int RepositoryId, int UserId)
        {
            List<AggResult> result = new List<AggResult>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByPublisher(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue ? e.CNT.Value : 0, e.Publisher)).ToList();
            }
            return result;

        }
        public static List<AggResult> AggByReferanceType(int RepositoryId, int UserId)
        {
            List<AggResult> result = new List<AggResult>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByReferanceType(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue ? e.CNT.Value : 0, e.ReferenceType)).ToList();
            }
            return result;

        }
        public static List<AggResult> AggByVersions(int RepositoryId, int UserId)
        {
            List<AggResult> result = new List<AggResult>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByVersions(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue? e.CNT.Value:0, e.Versions)).ToList();
            }
            return result;

        }
        public static List<AggResult> AggByYear(int RepositoryId, int UserId)
        {
            List<AggResult> result = new List<AggResult>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.AggByYear(RepositoryId, UserId).Select(e => new AggResult(e.CNT.HasValue ? e.CNT.Value : 0, e.Years)).ToList();
            }
            return result;
        }
        public static List<GenerateReference_Result> GenerateReference(int userId, Guid reposotoryGUID)
        {
            List<GenerateReference_Result> result = new List<GenerateReference_Result>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = db.GenerateReference(userId, reposotoryGUID).ToList();
            }
            return result;
        }
        public static List<GetAnalyzeQ_Result> GetAnalyzeQ()
        {
            List<GetAnalyzeQ_Result> result = new List<GetAnalyzeQ_Result>();
            using (StoredProcedure db = new StoredProcedure())
            {
                result = new List<GetAnalyzeQ_Result>(db.GetAnalyzeQ());
            }
            return result;
        }
        public static List<Keyword> GetKeywords(int repositoryId)
        {
            List<Keyword> results;
            using (Entities.Model.SystematicLREntities ent = new SystematicLREntities())
            {
                results = ent.Keywords.Where(e => e.RepositoryId == repositoryId).ToList();
            }
            return results;
            
        }
        public static ISimilarityResults GetSimilarity(int RepositoryId, Header header1, Header header2, Header header3, Header header4, Header header5)
        {
            SimilarityResults results = new SimilarityResults();
            results.Headers.Header1 = header1;
            results.Headers.Header2 = header2;
            results.Headers.Header3 = header3;
            results.Headers.Header4 = header4;
            results.Headers.Header5 = header5;
            
            using (StoredProcedure db = new StoredProcedure())
            {
                results.Data = new List<GetSimilarity_Result>(db.GetSimilarity(RepositoryId, header1.stemName, header2.stemName, header3.stemName, header4.stemName, header5.stemName, header1.threshold, header2.threshold, header3.threshold, header4.threshold, header5.threshold));
            }
            return results;
        }
        public static ISimilarityResults GetSimilarityTFBMTF(int RepositoryId, Header header1, Header header2, Header header3, Header header4, Header header5, Header header6)
        {

            SimilarityTFBMTF_Results results = new SimilarityTFBMTF_Results();
            results.Headers.Header1 = header1;
            results.Headers.Header2 = header2;
            results.Headers.Header3 = header3;
            results.Headers.Header4 = header4;
            results.Headers.Header5 = header5;
            results.Headers.Header6 = header6;
            using (StoredProcedure db = new StoredProcedure())
            {
                results.Data = new List<GetSimilarityTFBMTF_Result>(db.GetSimilarityTFBMTF(RepositoryId, header1.stemName, header2.stemName, header3.stemName, header4.stemName, header5.stemName, header6.stemName, header1.threshold, header2.threshold, header3.threshold, header4.threshold, header5.threshold, header6.threshold));
            }
            return results;
        }
    }
}