﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SystematicLR.GlobalFramework
{


    [Serializable]
    public class ResourceError
    {
        public string LogErrorMessage { get; set; }
        public string EndUserMessage { get; set; }

        //public bool MyProperty { get; set; }
    }
    public enum GlobalExceptionType { LogicError, Warning, runtimeError }
    [Serializable]
    public class GlobalException : Exception
    {


        public ResourceError Error { get; set; }

        public bool IsDisplayEndUserMessage { get; set; }

        public GlobalExceptionType ExceptionType { set; get; }
        public int ExceptionId { set; get; }

        public GlobalException(Exception ex)
        {
            this.ExceptionType = GlobalExceptionType.runtimeError;


        }
        public GlobalException(GlobalExceptionType ExceptionType, string messageError)
        {
            this.ExceptionType = ExceptionType;
        }
        public GlobalMessageType getMessageType()
        {
            if (this.ExceptionType == GlobalExceptionType.LogicError)
            {
                return GlobalMessageType.error;
            }
            if (this.ExceptionType == GlobalExceptionType.Warning)
            {
                return GlobalMessageType.warning;
            }
            return GlobalMessageType.nothing; ;
        }

        public GlobalException(string message)
        {

            Error = ErrorManager.CreateError(message);
        }

        public GlobalException(string logErrorMessage, string endUserMessage, bool isDisplayEndUserMessage = false)
        {

            IsDisplayEndUserMessage = isDisplayEndUserMessage;

            Error = new ResourceError() { LogErrorMessage = logErrorMessage, EndUserMessage = endUserMessage };
        }
    }

    public static class ErrorManager
    {
        #region Properties

        private class ModelException
        {
            public int Order { get; set; }
            public string Source { get; set; }
            public string Message { get; set; }
        }

        #endregion

        #region public functions

        public static string GetError(Exception ex)
        {
            int level = 1;
            StringBuilder sb = new StringBuilder();
            var userError = GetInnerException(ex, level);

            return userError + "|| " + GetErrorStackSourceDetails(ex);
        }

        static string GetErrorStackSourceDetails(Exception ex)
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            StringBuilder sb = new StringBuilder();
            sb.Append(" Details:");
            for (int i = 0; i < trace.FrameCount; i++)
            {
                System.Diagnostics.StackFrame sf = trace.GetFrame(i);
                string method = sf.GetMethod().Name;
                if (!method.Contains("GetErrorStackSourceDetails"))
                {
                    int line = sf.GetFileLineNumber();
                    string file = sf.GetFileName();
                    if (line != 0 && !string.IsNullOrEmpty(file))
                    {
                        sb.AppendFormat("[File:{0} | Method:{1} | Line:{2}]", file, method, line);
                        sb.Append(Environment.NewLine);
                    }
                }
            }
            return sb.ToString();
        }

        public static string GetModelErrors(this System.Web.Mvc.ModelStateDictionary modelStateCollection)
        {
            // we can customize to serialize Errors to JSON format
            StringBuilder sb = new StringBuilder();

            foreach (var model in modelStateCollection.Values)
            {
                foreach (var item in model.Errors)
                {
                    sb.AppendFormat("{0} | ", string.IsNullOrEmpty(item.ErrorMessage) ? ErrorManager.GetError(item.Exception) : item.ErrorMessage);
                }
            }
            sb.Remove(sb.Length - 3, 3);
            return sb.ToString();
        }

        public static ResourceError CreateError(string resourceValue)
        {
            ResourceError error = new ResourceError();
            if (string.IsNullOrEmpty(resourceValue))
            {
                error.LogErrorMessage = "Empty Resource Value";
                error.EndUserMessage = "";
            }
            else
            {
                //e.g.
                //Invalid user credentials
                //Invalid user credentials#Message to User 
                var errorParts = resourceValue.Split('#');
                if (errorParts.Length == 2)
                {
                    error.LogErrorMessage = errorParts[0];
                    error.EndUserMessage = errorParts[1];
                }
                else
                {
                    error.LogErrorMessage = errorParts[0];
                    error.EndUserMessage = errorParts[0];
                }
            }
            return error;
        }

        #endregion

        #region Privates Functions

        private static string GetInnerException(Exception ex, int level)
        {
            if (ex is GlobalException)
            {
                var evenGlobalEx = ex as GlobalException;
                return evenGlobalEx.Error.LogErrorMessage;
            }
            else
                if (ex is System.Data.Entity.Validation.DbEntityValidationException)
            {
                var errorEx = ex as System.Data.Entity.Validation.DbEntityValidationException;
                return string.Join(";", errorEx.EntityValidationErrors.SelectMany(d => d.ValidationErrors).Select(d => string.Format("{0}:{1}", d.PropertyName, d.ErrorMessage)).ToList());
            }
            else
                    if (ex.InnerException != null)
            {
                var sb = new StringBuilder();

                var userErrorSB = new StringBuilder();

                var innerUserError = GetInnerException(ex.InnerException, ++level);

                userErrorSB.AppendFormat("{0} | {1}", ex.Message, innerUserError);

                return userErrorSB.ToString();
            }
            return ex.Message;
        }
        #endregion
    }

}
