﻿using System;
using System.Linq;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Collections.Generic;
using System.IO;

namespace SystematicLR.GlobalFramework
{
    
    public enum EmailTemplates { traineeDailymail, MilestonEmail , CreateUser,RestPassword, FreeEmail };
 
    public class Email
    {
        public static string ParseEmail(Dictionary<string, string> keys,string filecontent)
        {            
            foreach (var keyvalue in keys)
            {
                filecontent = filecontent.Replace("@" + keyvalue.Key, keyvalue.Value);
            }
            return filecontent;
        }
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidateEmailList(string emails)
        {
            try
            {
                foreach (var email in emails.Split(','))
                {
                    if (!IsValidEmail(email))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static List<MailAddress> fillEmailList(string contacts)
        {
            List<MailAddress> results = new List<MailAddress>();
            string[] namesArray = contacts.Split(',');
            List<string> namesList = new List<string>(namesArray.Length);
            namesList.AddRange(namesArray);
            namesList.Reverse();

            {
                for (int i = 0; i < namesList.Count; i++)
                {
                    results.Add(new MailAddress(namesList[i]));  // replace with valid value 
                }
            }
            return results;
        }
        public static bool sendMail(string To, string messagetext, string Subject, string CC = null, string BCC = null)
        {
         

            var message = new MailMessage();
            var listTo = fillEmailList(To);
            foreach (var m in listTo)
            {
                message.To.Add(m);
            }
            message.From = new MailAddress("lmsnoreply2018@gmail.com");
            message.Subject = Subject;
            message.Body = messagetext;
            if (CC != null)
            {
                listTo = fillEmailList(CC);
                foreach (var m in listTo)
                {
                    message.CC.Add(m);
                }
            }

            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new System.Net.NetworkCredential
                {
                    UserName = "lmsnoreply2018@gmail.com",  // replace with valid value
                    Password = "lms1234567"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Send(message);
                //return RedirectToAction("Sent");
            }

            return true;
        }

        public static string SendMailSync(SmtpClient mailClient, string to, string subject, string message, string cc = null, string bcc = null, bool isBodyHtml = true)
        {
                   
            try
            {

                if (string.IsNullOrEmpty(to) && string.IsNullOrEmpty(cc) && string.IsNullOrEmpty(bcc))
                    throw new Exception("Recipient email address should not be empty");

                if (string.IsNullOrEmpty(subject))
                    throw new Exception("Email subject can not be empty");
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                #region to emails

                if (!string.IsNullOrEmpty(to))
                {
                    foreach (var email in to.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.To.Add(email);
                    }
                }
                #endregion

                #region cc emails

                if (!string.IsNullOrEmpty(cc))
                {
                    foreach (var email in cc.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.CC.Add(email);
                    }
                }
                #endregion

                #region bcc emails

                if (!string.IsNullOrEmpty(bcc))
                {
                    foreach (var email in bcc.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.Bcc.Add(email);
                    }
                }

                #endregion


                var Credential = (System.Net.NetworkCredential)mailClient.Credentials;
                mailMessage.From = new System.Net.Mail.MailAddress(Credential.UserName);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = isBodyHtml;
                
                //using (mailClient)
                {
                    //mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //mailClient.UseDefaultCredentials = false;
                    mailClient.Send(mailMessage);
                    mailMessage.Dispose();
                    return null;
                }


            }
            catch (Exception ex)
            {

               string ErrorLog= ActivityLogger.Save(new ActivityLog() { Action = "SendEmail", FailReason = ErrorManager.GetError(ex), IsSuccess = false, LogDate = Configurations.Now, LogTime = Configurations.Now.TimeOfDay, LogCategory = 'W' });
                return ErrorLog;
            }
        }
        public static async Task<GlobalMessage> SendMailAsync(SmtpClient mailClient, string to, string subject, string message, string cc = null, string bcc = null, bool isBodyHtml = true)
        {
            #region validation

            if (string.IsNullOrEmpty(to) && string.IsNullOrEmpty(cc) && string.IsNullOrEmpty(bcc))
            {
                throw new Exception("Recipient email address should not be empty");
            }                
            //if (string.IsNullOrEmpty(subject))
            //{
            //    return new LMSMessage() { Message = "Subject Is Required",MessageType=LMSMessageType.warning };
            //}

            #endregion            
            try
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                #region to emails

                if (!string.IsNullOrEmpty(to))
                {
                    foreach (var email in to.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.To.Add(email);
                    }
                }
                #endregion

                #region cc emails

                if (!string.IsNullOrEmpty(cc))
                {
                    foreach (var email in cc.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.CC.Add(email);
                    }
                }
                #endregion

                #region bcc emails

                if (!string.IsNullOrEmpty(bcc))
                {
                    foreach (var email in bcc.Split(','))
                    {
                        if (IsValidEmail(email))
                            mailMessage.Bcc.Add(email);
                    }
                }

                #endregion


                var Credential = (System.Net.NetworkCredential)mailClient.Credentials;
                mailMessage.From = new System.Net.Mail.MailAddress(Credential.UserName);
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = isBodyHtml;
                using (mailClient)
                {
                    //mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //mailClient.UseDefaultCredentials = false;
                    await mailClient.SendMailAsync(mailMessage);
                    mailMessage.Dispose();
                    return new GlobalMessage() {Message="The Message has been sent Successfully" ,MessageType= GlobalMessageType.success};
                }


            }
            catch (Exception ex)
            {

                string ErrorCode=ActivityLogger.Save(new ActivityLog() { Action = "SendEmail", FailReason = ErrorManager.GetError(ex), IsSuccess = false, LogDate = Configurations.Now, LogTime = Configurations.Now.TimeOfDay, LogCategory = 'W' });
                return new GlobalMessage() { Message =string.Format(SystematicLR.GlobalFramework.Configurations.GeneralError,ErrorCode), MessageType = GlobalMessageType.error };
            }
        }


    }
    
    public class EmailParser
    {
        private Dictionary<string, string> keys { set; get; }
        public EmailTemplates template { set; get; }
        public string templatesPath { set; get; }

        private string GetTemplate(EmailTemplates template)
        {
            
            return File.ReadAllText(templatesPath+"//"+ template.ToString());
        }
        public void AddKey(string Name,string Value)
        {
            if (keys.ContainsKey(Name))
                keys[Name] = Value;
            else
                keys.Add(Name, Value);
        }
        public EmailParser()
        {
            keys = new Dictionary<string, string>();
        }
        public void ClearKeys()
        {
            keys.Clear();
        }
        public string parse()
        {
            string filecontent = GetTemplate(template);
            foreach (var keyvalue in this.keys)
            {
                filecontent = filecontent.Replace("@" + keyvalue.Key, keyvalue.Value);
            }
            return filecontent;
        }

    }


}
