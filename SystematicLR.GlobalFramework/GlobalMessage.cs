﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystematicLR.GlobalFramework
{
    public enum GlobalMessageType { error='e', warning='w', success='s', info='i', validation='v',nothing='n' }
    public class GlobalMessage
    {
        public string Message { set; get; }
        public GlobalMessageType MessageType { set; get; }
        public HtmlString RenderMessage()
        {
            return new HtmlString(string.Format("<div class='{0}' >{1}</div>", MessageType.ToString()+ "MSG", Message));
        }
        public bool WithRedirect { set; get; }
        public bool isHasMessage()
        {
            return !string.IsNullOrEmpty(Message);
        }
        public string MessageTypeText { get { return MessageType.ToString(); } }
        public bool IsError
        {
            get
            {
                return MessageType == GlobalMessageType.error ? true : false;
            }
        }
        public bool IsWarning
        {
            get
            {
                return MessageType == GlobalMessageType.warning ? true : false;
            }
        }
        public bool isSuccess
        {
            get
            {
                return MessageType == GlobalMessageType.success ? true : false;
            }
            
        }
        public bool IsInfo
        {
            get
            {
                return MessageType == GlobalMessageType.info ? true : false;
            }
        }
       
    }
}