﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystematicLR.GlobalFramework
{
    public class Configurations
    {
        public static  DateTime getCurrenctDate()
        {
            return DateTime.Now;
        }
        public static DateTime getToday()
        {
            return DateTime.Now.Date;
        }
        public static DateTime Today
        {
            get
            {
                return getToday();
            }
        }
        public static DateTime Now
        {
            get
            {
                return getCurrenctDate();
            }
        }

        public static string GeneralError
        {
            get
            {
                return "Sorry, there is an Error  [{1}]";
            }
        }
        public static string ErrorMessageWithCodeFormat
        {
            get
            {
                return "{0} [Code={1}]";
            }
        }
    }
}
