﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SystematicLR.GlobalFramework
{
    public class JSONResponse
    {
        public bool isSuccess { set; get; }
        public object returndValue { set; get; }
        public string message { set; get; }
        public bool showMessage { set; get; }
        public string PartialView { set; get; }
    }
}