﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace SystematicLR.GlobalFramework
{
    public enum ActivityLogCategoryEnum
    {
       WebSite='W',
       Dashboard='A',
       WindowsService='S'
    }
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class LogAttribute : ActionFilterAttribute, IExceptionFilter
    {
      

        private string SerializedActionParameter { get; set; }
        private string AreaName { get; set; }
        private string ActionName { get; set; }
        private string ControllerName { get; set; }
        private ActivityLogCategoryEnum activityLogCategory;
        public ActivityLogCategoryEnum ActivityLogCategory
        {
            get
            {
                if ((char)activityLogCategory == default(char))
                    activityLogCategory = ActivityLogCategoryEnum.WebSite;
                return activityLogCategory;
            }
            set
            {
                activityLogCategory = value;
            }
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // we can customize to serialize Included parameters
            // e.g. [Bind(Include = "Id,Name,Order,ResourceMasterId")]
            SerializedActionParameter = new JavaScriptSerializer().Serialize(filterContext.ActionParameters);
            AreaName = filterContext.RouteData.Values.Keys.Contains("area") ? filterContext.RouteData.Values["area"].ToString() : string.Empty;
            ActionName = filterContext.RouteData.Values["action"].ToString();
            ControllerName = filterContext.RouteData.Values["controller"].ToString();
            base.OnActionExecuting(filterContext);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
            base.OnActionExecuted(filterContext);
        }
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string failReason = null;
                string endUserMessage = null;
                if (filterContext.Exception is GlobalException)
                {
                    var ex = filterContext.Exception as GlobalException;
                    failReason = ex.Message;
                    //endUserMessage = ex.Error.EndUserMessage;
                }
                else
                {
                    failReason = ErrorManager.GetError(filterContext.Exception);
                    endUserMessage = "";
                }

                // save exception in database (ActivityLog Table)
                var code = ActivityLogger.Save(new ActivityLog()
                {
                    Area = AreaName,
                    Controller = ControllerName,
                    Action = ActionName,
                    ActionParameters = SerializedActionParameter,
                   LogCategory=(char)ActivityLogCategory,
                    FailReason = failReason,
                    IsSuccess = false
                });

                

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                    new
                    {
                        controller = "Home",
                        action = "ErrorMessage",
                        message = endUserMessage,
                        code = code,
                        isAjaxRequet = filterContext.HttpContext.Request.IsAjaxRequest()
                    }));

                //}

                filterContext.ExceptionHandled = true;

            }
        }
    }
  
    public sealed class LoginAuthAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SessionManager.ClientUser == null)
            {
                filterContext.Result = new RedirectResult("~/Home/login");
            }            
            base.OnActionExecuting(filterContext);
        }

    }
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class AdminAuth : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (SessionManager.ClientUser == null )
            {
                    filterContext.Result = new RedirectResult("~/Home/login");
            }
            else if (!SessionManager.ClientUser.IsUserAdmin)
            {
                var controller = (BaseController)filterContext.Controller;
                filterContext.Result = controller.RedirectToNoAccess;
            }

            base.OnActionExecuting(filterContext);
        }

    }
    public sealed class ActiveAttribute : ActionFilterAttribute
    {
        private string treevalue;
        private string subtree;
        public ActiveAttribute(TreeViewEnum tree)
        {
            treevalue = tree.ToString();
        
        }
        public ActiveAttribute(TreeViewEnum tree, SubTreeEnum subtree)
        {
            treevalue = tree.ToString();
            
                this.subtree = subtree.ToString();
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.ActiveTree = treevalue;            
            filterContext.Controller.ViewBag.ActiveSubTree = subtree;
            base.OnActionExecuting(filterContext);
        }

    }
    public sealed class RepositoryAttribute : ActionFilterAttribute
    {
        
      
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Guid repId = Guid.Empty;
            if (!string.IsNullOrEmpty(filterContext.HttpContext.Request.QueryString["repId"]))
            {
                
                Guid.TryParse(filterContext.HttpContext.Request.QueryString["repId"], out repId);                
            }
            if (SessionManager.CurrentRepository == null && repId == Guid.Empty)
            {
                filterContext.Result = new RedirectResult("~/Home/Repositories");
            }
            else
            {
                if(repId!= Guid.Empty)
                {
                    if (SessionManager.CurrentRepository == null || repId != SessionManager.CurrentRepository.ReposotoryGUID)
                        SessionManager.CurrentRepository = SessionManager.Repositories.FirstOrDefault(e => e.ReposotoryGUID == repId);
                }
                
            }

            base.OnActionExecuting(filterContext);
        }

    }

}
