﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace LMS.GlobalFramework
{

    /// <summary>
    /// Summary description for EncDec
    /// </summary>
    public class Encryption
    {
        public Encryption()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string GenerateKey()
        {
            // Create an instance of Symetric Algorithm. Key and IV is generated automatically.
            DESCryptoServiceProvider desCrypto = (DESCryptoServiceProvider)DESCryptoServiceProvider.Create();

            // Use the Automatically generated key for Encryption. 
            return ASCIIEncoding.ASCII.GetString(desCrypto.Key);
        }
        public void EncryptFile(string sInputFilename,
             string sOutputFilename,
             string sKey)
        {
            FileStream fsInput = new FileStream(sInputFilename,
               FileMode.Open,
               FileAccess.Read);

            FileStream fsEncrypted = new FileStream(sOutputFilename,
               FileMode.Create,
               FileAccess.Write);
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            ICryptoTransform desencrypt = DES.CreateEncryptor();
            CryptoStream cryptostream = new CryptoStream(fsEncrypted, desencrypt, CryptoStreamMode.Write);

            byte[] bytearrayinput = new byte[fsInput.Length];
            fsInput.Read(bytearrayinput, 0, bytearrayinput.Length);
            cryptostream.Write(bytearrayinput, 0, bytearrayinput.Length);
            cryptostream.Close();
            fsInput.Close();
            fsEncrypted.Close();
        }

        public void DecryptFile(string sInputFilename, string sOutputFilename, string sKey)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            //A 64 bit key and IV is required for this provider.
            //Set secret key For DES algorithm.
            DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            //Set initialization vector.
            DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Create a file stream to read the encrypted file back.
            FileStream fsread = new FileStream(sInputFilename,
               FileMode.Open,
               FileAccess.Read);
            //Create a DES decryptor from the DES instance.
            ICryptoTransform desdecrypt = DES.CreateDecryptor();
            //Create crypto stream set to read and do a 
            //DES decryption transform on incoming bytes.
            CryptoStream cryptostreamDecr = new CryptoStream(fsread,
               desdecrypt,
               CryptoStreamMode.Read);
            //Print the contents of the decrypted file.
            StreamWriter fsDecrypted = new StreamWriter(sOutputFilename);
            fsDecrypted.Write(new StreamReader(cryptostreamDecr).ReadToEnd());
            fsDecrypted.Flush();
            fsDecrypted.Close();
        }
        public string HashString(string Value)
        {
            HashAlgorithm mhash = new MD5CryptoServiceProvider();

            //Convert the original string to array of Bytes
            byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(Value);

            // Compute the Hash, returns an array of Bytes
            byte[] bytHash = mhash.ComputeHash(bytValue);

            mhash.Clear();

            // Return a base 64 encoded string of the Hash value
            return Convert.ToBase64String(bytHash);
        }
        public static string EncryptString(string Value)
        {
            SymmetricAlgorithm mCSP = new TripleDESCryptoServiceProvider();
            ICryptoTransform ct;
            MemoryStream ms;
            CryptoStream cs;
            byte[] byt;
            mCSP.Key = Encoding.UTF8.GetBytes("L@w@Y@d@ITDepartment2015");
            mCSP.IV = Encoding.UTF8.GetBytes("BaNkErSy");
            ct = mCSP.CreateEncryptor(mCSP.Key, mCSP.IV);
            byt = Encoding.UTF8.GetBytes(Value);
            ms = new MemoryStream();
            cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();

            cs.Close();

            return Convert.ToBase64String(ms.ToArray());
        }
        public static string DecryptString(string Value)
        {
            SymmetricAlgorithm mCSP = new TripleDESCryptoServiceProvider();
            ICryptoTransform ct;
            MemoryStream ms;
            CryptoStream cs;
            byte[] byt;
            mCSP.Key = Encoding.UTF8.GetBytes("L@w@Y@d@ITDepartment2015");
            mCSP.IV = Encoding.UTF8.GetBytes("BaNkErSy");
            ct = mCSP.CreateDecryptor(mCSP.Key, mCSP.IV);
            byt = Convert.FromBase64String(Value);
            ms = new MemoryStream();
            cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();

            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());
        }
        public void EncryptImage(string FileName, string FileName1)
        {
            FileInfo file = new FileInfo(FileName);
            FileInfo file1 = new FileInfo(FileName1);
            byte[] img = new byte[file.Length];
            FileStream fs = new FileStream(FileName, FileMode.Open);
            BinaryReader f = new BinaryReader(fs);
            img = f.ReadBytes(Int32.Parse(file.Length.ToString()));
            FileStream fs1 = new FileStream(FileName1, FileMode.CreateNew);
            BinaryWriter w1 = new BinaryWriter(fs1);
            foreach (byte b in img)
            {
                w1.Write((byte)((int)(b + 10)));
            }

            f.Close();
            fs.Close();
            fs1.Close();
            w1.Close();
        }
        public void DecryptImage(string FileName, string FileName1)
        {
            FileInfo file = new FileInfo(FileName);
            FileInfo file1 = new FileInfo(FileName1);
            byte[] img = new byte[file.Length];
            FileStream fs = new FileStream(FileName, FileMode.Open);
            BinaryReader f = new BinaryReader(fs);
            img = f.ReadBytes(Int32.Parse(file.Length.ToString()));
            FileStream fs1 = new FileStream(FileName1, FileMode.CreateNew);
            BinaryWriter w1 = new BinaryWriter(fs1);
            foreach (byte b in img)
                w1.Write((byte)((int)(b - 10)));
            f.Close();
            fs.Close();
            fs1.Close();
            w1.Close();
        }
        public void SplitFile(string FileName, string pathFiles, char prefix, int SplitCount)
        {
            FileInfo file = new FileInfo(FileName);
            byte[] img = new byte[file.Length];
            FileStream fs = new FileStream(FileName, FileMode.Open);
            BinaryReader f = new BinaryReader(fs);
            img = f.ReadBytes(Int32.Parse(file.Length.ToString()));
            int count = 0;
            int num = 0;
            FileStream fs3 = new FileStream(pathFiles + "\\" + prefix.ToString() + num.ToString() + ".jpg", FileMode.CreateNew);
            BinaryWriter w3 = new BinaryWriter(fs3);
            for (int i = 0; i < img.Length; i++)
            {
                if (count == ((int)file.Length) / SplitCount)
                {
                    w3.Close();
                    fs3.Close();
                    num += 1;
                    fs3 = new FileStream(pathFiles + "\\" + prefix.ToString() + num.ToString() + ".jpg", FileMode.CreateNew);
                    w3 = new BinaryWriter(fs3);
                    count = 0;
                }
                w3.Write(img[i]);
                count += 1;
            }
            try
            { w3.Close(); fs3.Close(); }
            catch { }
            fs.Close();
            f.Close();
        }
        public void CombineFiles(string FolderName, string ResultFile)
        {
            DirectoryInfo Folder = new DirectoryInfo(FolderName);
            FileInfo[] Files = Folder.GetFiles();
            FileStream StreamReader;
            BinaryReader Reader;
            FileStream StreamWriter = new FileStream(ResultFile, FileMode.CreateNew);
            BinaryWriter Writer = new BinaryWriter(StreamWriter);
            byte[] img;
            for (int i = 0; i < Files.Length; i++)
            {
                StreamReader = new FileStream(Files[i].FullName, FileMode.Open);
                Reader = new BinaryReader(StreamReader);
                img = new byte[Files[i].Length];
                img = Reader.ReadBytes(Int32.Parse(Files[i].Length.ToString()));
                foreach (byte b in img)
                {
                    Writer.Write(b);

                }
                StreamReader.Close();
                Reader.Close();
            }
            StreamWriter.Close();
            Writer.Close();

        }
        public void EncryptFileViaPassword(string FilePath, string Password)
        {
            FileInfo File = new FileInfo(FilePath);
            byte[] pass = Encoding.ASCII.GetBytes(Password);
            FileStream StreamReader = new FileStream(FilePath, FileMode.Open);
            BinaryReader Reader = new BinaryReader(StreamReader);
            byte[] img = Reader.ReadBytes(Int32.Parse(File.Length.ToString()));
            StreamReader.Close();
            Reader.Close();
            File.Delete();
            FileStream StreamWriter = new FileStream(FilePath, FileMode.CreateNew);
            BinaryWriter Writer = new BinaryWriter(StreamWriter);
            for (int im = 0; im < img.Length; im++)
            {
                if (im == ((int)pass[0]))
                {
                    foreach (byte b in pass)
                    {
                        Writer.Write(b);
                    }
                }
                Writer.Write(img[im]);
            }
            StreamReader.Close();
            Reader.Close();
            StreamWriter.Close();
            Writer.Close();
        }
        public void DecryptFileViaPassword(string FilePath, string Password)
        {
            FileInfo File = new FileInfo(FilePath);
            byte[] pass = Encoding.ASCII.GetBytes(Password);
            FileStream StreamReader = new FileStream(FilePath, FileMode.Open);
            BinaryReader Reader = new BinaryReader(StreamReader);
            byte[] img = Reader.ReadBytes(Int32.Parse(File.Length.ToString()));
            StreamReader.Close();
            Reader.Close();
            File.Delete();
            FileStream StreamWriter = new FileStream(FilePath, FileMode.CreateNew);
            BinaryWriter Writer = new BinaryWriter(StreamWriter);
            int im;
            for (im = 0; im < img.Length; im++)
            {
                if (im == ((int)pass[0])) break;
                Writer.Write(img[im]);
            }
            int p = 0;
            for (; im < img.Length; im++)
            {
                if (pass.Length != p)
                {
                    if (pass[p] != img[im])
                        Writer.Write(0);
                    p++;
                }
                else break;
            }
            for (; im < img.Length; im++)
            {
                Writer.Write(img[im]);
            }
            StreamReader.Close();
            Reader.Close();
            StreamWriter.Close();
            Writer.Close();
        }

        public void EncryptTextToFile(String Data, String FileName, byte[] Key, byte[] IV)
        {
            try
            {
                // Create or open the specified file.
                FileStream fStream = File.Open(FileName, FileMode.OpenOrCreate);

                // Create a new RC2 object.
                RC2 RC2alg = RC2.Create();

                // Create a CryptoStream using the FileStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(fStream,
                    RC2alg.CreateEncryptor(Key, IV),
                    CryptoStreamMode.Write);

                // Create a StreamWriter using the CryptoStream.
                StreamWriter sWriter = new StreamWriter(cStream);

                // Write the data to the stream 
                // to encrypt it.
                sWriter.WriteLine(Data);

                // Close the streams and
                // close the file.
                sWriter.Close();
                cStream.Close();
                fStream.Close();
            }
            catch (CryptographicException e)
            {
                //Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                // Console.WriteLine("A file error occurred: {0}", e.Message);
            }

        }

        public string DecryptTextFromFile(String FileName, byte[] Key, byte[] IV)
        {
            try
            {
                // Create or open the specified file. 
                FileStream fStream = File.Open(FileName, FileMode.OpenOrCreate);

                // Create a new RC2 object.
                RC2 RC2alg = RC2.Create();

                // Create a CryptoStream using the FileStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(fStream,
                    RC2alg.CreateDecryptor(Key, IV),
                    CryptoStreamMode.Read);

                // Create a StreamReader using the CryptoStream.
                StreamReader sReader = new StreamReader(cStream);

                // Read the data from the stream 
                // to decrypt it.
                string val = sReader.ReadLine();

                // Close the streams and
                // close the file.
                sReader.Close();
                cStream.Close();
                fStream.Close();

                // Return the string. 
                return val;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("A file error occurred: {0}", e.Message);
                return null;
            }
        }

        

    }

}