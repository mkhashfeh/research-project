﻿using SystematicLR.Entities.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace SystematicLR.GlobalFramework
{
    
    public class BaseController : Controller
    {
        protected SystematicLR.Entities.Model.SystematicLREntities db = new SystematicLREntities();
        
        protected void SendMessageToView(GlobalMessage message)
        {
            ViewBag.Message = message;

        }
        public ActionResult RedirectToNoAccess
        {
            get
            {
                return RedirectToAction("NoAccess", "Home");
            }
        }
        public string RenderPartialToString(string ViewPath, object model)
        {
            this.ViewData.Model = model;

            // initialize a string builder
            using (StringWriter sw = new StringWriter())
            {
                // find and load the view or partial view, pass it through the controller factory
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, ViewPath);
                ViewContext viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);

                // render it
                viewResult.View.Render(viewContext, sw);

                //return the razorized view/partial-view as a string
                return sw.ToString();
            }
        }
        protected string LogError(Exception exception, Dictionary<string, object> Parameters)
        {


            string failReason = null;
            
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (exception is GlobalException)
            {
                var ex = exception as GlobalException;
                failReason = ex.Message;
                //endUserMessage = ex.Error.EndUserMessage;
            }
            else
            {
                failReason = ErrorManager.GetError(exception);
                
            }

            // save exception in database (ActivityLog Table)
            var code = ActivityLogger.Save(new ActivityLog()
            {
                Area = null,
                Controller = controllerName,
                Action = actionName,
                ActionParameters = new JavaScriptSerializer().Serialize(Parameters),
                LogCategory = 'W',
                FailReason = failReason,
                IsSuccess = false
            });
            return code;

        }
        protected User ClientUser
        {
            
            set
            {
                SessionManager.ClientUser = value;
            }
            get
            {
                return SessionManager.ClientUser;

            }
        }
        protected List<Repository> UserRepositories
        {

            set
            {
                SessionManager.Repositories = value;
            }
            get
            {
                return SessionManager.Repositories;

            }
        }
        
        protected Repository CurrentRepository
        {

            set
            {
                SessionManager.CurrentRepository = value;
            }
            get
            {               

                return SessionManager.CurrentRepository;
            }
        }
     



    }
}