﻿using SystematicLR.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SystematicLR.GlobalFramework
{
    public class SessionManager
    {

        
        public static User ClientUser
        {

            set
            {
                HttpContext.Current.Session["Client"] = value;
            }
            get
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["Client"] != null)
                {
                    return (User)HttpContext.Current.Session["Client"];
                }
                return null;

            }
        }
        public static List<Repository> Repositories
        {

            set
            {
                HttpContext.Current.Session["Repositories"] = value;
            }
            get
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["Repositories"] != null)
                {
                    return (List<Repository>)HttpContext.Current.Session["Repositories"];
                }
                return null;
            }
        }
        public static Repository CurrentRepository
        {

            set
            {
                HttpContext.Current.Session["CurrentRepository"] = value;
            }
            get
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["CurrentRepository"] != null)
                {
                    return (Repository)HttpContext.Current.Session["CurrentRepository"];
                }
                return null;
            }
        }
    }
}
