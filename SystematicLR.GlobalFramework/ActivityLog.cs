﻿using System;

using System.Data;
using System.Data.SqlClient;

using System.Web;

namespace SystematicLR.GlobalFramework
{
    public class ActivityLog
    {
        public long Id { get; set; }
        public System.DateTime LogDate { get; set; }
        public System.TimeSpan LogTime { get; set; }
        public int UserId { get; set; }
        public string UserIP { get; set; }
        public string UserAgent { get; set; }
        public string URL { get; set; }
        public string ReferrerURL { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string ActionParameters { get; set; }
        public string ExtraData { get; set; }
        public bool IsSuccess { get; set; }
        public string FailReason { get; set; }
        public char LogCategory { set; get; }
        
        public ActivityLog()
        {
            var tempDate = Configurations.Now;
            LogDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day);
            LogTime = new TimeSpan(tempDate.Hour, tempDate.Minute, tempDate.Second);
            
            
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Request != null)
                {
                    UserIP = HttpContext.Current.Request.UserHostAddress;
                    UserAgent = HttpContext.Current.Request.UserAgent;
                    URL = HttpContext.Current.Request.Url.AbsoluteUri;
                    ReferrerURL = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri : string.Empty;
                }
                UserId = SessionManager.ClientUser == null ? -1 : SessionManager.ClientUser.UserId;
            }
        }

    }
    public class ActivityLogger
    {
        public static string Save(ActivityLog activityData)
        {
            SystematicLR.Entities.Model.SystematicLREntities db = new SystematicLR.Entities.Model.SystematicLREntities();

            using (SqlConnection conn = new SqlConnection(db.Database.Connection.ConnectionString))
            using (SqlCommand cmd = new SqlCommand("dbo.ActivityLogInsert", conn))
            {
                var Id = Guid.NewGuid();

                

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "Id",
                        SqlDbType = System.Data.SqlDbType.UniqueIdentifier,
                        Direction = ParameterDirection.Input,
                        Value = Id
                    });
                //cmd.Parameters.Add(
                //   new SqlParameter
                //   {
                //       ParameterName = "Code",
                //       SqlDbType = System.Data.SqlDbType.VarChar,
                //       Size = 32,
                //       Direction = ParameterDirection.Input,
                //       Value = code
                //   });
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "LogDate",
                        SqlDbType = System.Data.SqlDbType.Date,
                        Value = activityData.LogDate,
                        Direction = ParameterDirection.Input
                    });               
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "UserId",
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Value = activityData.UserId == 0 ? (object)System.DBNull.Value : activityData.UserId,
                        Size = 128,
                        Direction = ParameterDirection.Input
                    });
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "UserIP",
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Value = string.IsNullOrEmpty(activityData.UserIP) ? (object)System.DBNull.Value : activityData.UserIP,
                        Size = 64,
                        Direction = ParameterDirection.Input
                    });

                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "UserAgent",
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Value = string.IsNullOrEmpty(activityData.UserAgent) ? (object)System.DBNull.Value : activityData.UserAgent,
                        Size = 256,
                        Direction = ParameterDirection.Input
                    });
                cmd.Parameters.Add(
                   new SqlParameter
                   {
                       ParameterName = "URL",
                       SqlDbType = System.Data.SqlDbType.NVarChar,
                       Value = string.IsNullOrEmpty(activityData.URL) ? (object)System.DBNull.Value : activityData.URL,
                       Size = 1024,
                       Direction = ParameterDirection.Input
                   });
                cmd.Parameters.Add(
                   new SqlParameter
                   {
                       ParameterName = "ReferrerURL",
                       SqlDbType = System.Data.SqlDbType.NVarChar,
                       Value = string.IsNullOrEmpty(activityData.ReferrerURL) ? (object)System.DBNull.Value : activityData.ReferrerURL,
                       Size = 1024,
                       Direction = ParameterDirection.Input
                   });
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "Area",
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Value = string.IsNullOrEmpty(activityData.Area) ? (object)System.DBNull.Value : activityData.Area,
                        Size = 128,
                        Direction = ParameterDirection.Input
                    });

                cmd.Parameters.Add(
                   new SqlParameter
                   {
                       ParameterName = "Controller",
                       SqlDbType = System.Data.SqlDbType.NVarChar,
                       Value = string.IsNullOrEmpty(activityData.Controller) ? (object)System.DBNull.Value : activityData.Controller,
                       Size = 128,
                       Direction = ParameterDirection.Input
                   });
                cmd.Parameters.Add(
                 new SqlParameter
                 {
                     ParameterName = "Action",
                     SqlDbType = System.Data.SqlDbType.NVarChar,
                     Value = string.IsNullOrEmpty(activityData.Action) ? (object)System.DBNull.Value : activityData.Action,
                     Size = 256,
                     Direction = ParameterDirection.Input
                 });
                cmd.Parameters.Add(
                new SqlParameter
                {
                    ParameterName = "ActionParameters",
                    SqlDbType = System.Data.SqlDbType.NVarChar,
                    Value = string.IsNullOrEmpty(activityData.ActionParameters) ? (object)System.DBNull.Value : activityData.ActionParameters,
                    Size = 4000,
                    Direction = ParameterDirection.Input
                });
                cmd.Parameters.Add(
                   new SqlParameter
                   {
                       ParameterName = "ExtraData",
                       SqlDbType = System.Data.SqlDbType.NText,
                       Value = string.IsNullOrEmpty(activityData.ExtraData) ? (object)System.DBNull.Value : activityData.ExtraData,
                       Direction = ParameterDirection.Input
                   });
                cmd.Parameters.Add(
                   new SqlParameter
                   {
                       ParameterName = "IsSuccess",
                       SqlDbType = System.Data.SqlDbType.Bit,
                       Value = activityData.IsSuccess,
                       Direction = ParameterDirection.Input
                   });
                cmd.Parameters.Add(
                    new SqlParameter
                    {
                        ParameterName = "FailReason",
                        SqlDbType = System.Data.SqlDbType.NVarChar,
                        Value = string.IsNullOrEmpty(activityData.FailReason) ? (object)System.DBNull.Value : activityData.FailReason,
                        Size = 4000,
                        Direction = ParameterDirection.Input
                    });
                cmd.Parameters.Add(
                  new SqlParameter
                  {
                      ParameterName = "LogCategory",
                      SqlDbType = System.Data.SqlDbType.Char,
                      Value = activityData.LogCategory,
                      Size = 1,
                      Direction = ParameterDirection.Input
                  });




                var output = new SqlParameter
                {
                    ParameterName = "Code",
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Size = 32,
                    Direction = ParameterDirection.Output,
                    //Value = code
                };
                cmd.Parameters.Add(output);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
                //var codeTemp = (string)output.Value;
                //var insertedRowId = cmd.Parameters["Id"].Value;
                return (string)output.Value;
            }
        }
    }
}
