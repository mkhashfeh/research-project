﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystematicLR.GlobalFramework
{
    public enum TreeViewEnum
    {
        treeRepos,
        treeDocuments,
        treeAggregations,       

    }
    public enum SubTreeEnum
    {
        RepContent, NewRef, AggYear,no, Aggcountry, AggReferanceType, AggCitation, AggVarsion, AggPublisher, GenerateReference,Search
    }
}