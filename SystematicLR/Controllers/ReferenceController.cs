﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using SystematicLR.GlobalFramework;
using SystematicLR.Entities;
using System.Net;
using SystematicLR.GoogleScholar;
using SystematicLR.Models;
using SystematicLR.Core.Managers;
using SystematicLR.Entities.Model;

namespace SystematicLR.Controllers
{
    [LoginAuth]
    public class ReferenceController : BaseController
    {

        // GET: Reference
        [Active(TreeViewEnum.treeDocuments, SubTreeEnum.NewRef)]
        [Repository]
        public ActionResult Index(string refId)
        {
            if (refId == null)
            {
                ViewBag.SubTitle = "Add New";
                return View();
            }
            else
            {
                ViewBag.SubTitle = "Read";
                ViewBag.ReferenceGUID = refId.ToUpper();
            }
            return View();
        }
        private byte[] GetBytesFromFile(string fullFilePath)
        {
            System.IO.FileStream fs = null;
            try
            {
                fs = System.IO.File.OpenRead(fullFilePath);
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                return bytes;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
        }
        [Active(TreeViewEnum.treeDocuments, SubTreeEnum.RepContent)]
        [Repository]
        public ActionResult RepositoryContent()
        {
            var Categories = DBServices.GetOptions(CurrentRepository.RepositoryId, "Category");
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "-- Any --", Value = "" });
            items.AddRange(Categories.Select(e => new SelectListItem() { Text = e, Value = e }).ToList());
            ViewBag.Repository = CurrentRepository.RepositoryName;
            ViewBag.Category = items;
            return View();
        }

        public ActionResult SearchReferences(string title, string Category, string referenceId, string isUse, string Bookmark)
        {
            try
            {
                Guid referenceGUIDTemp;
                Guid? referenceGUIDValue = null;
                bool tmp;
                bool? inUse = null;
                bool? bookmark = null;
                if (bool.TryParse(isUse, out tmp))
                    inUse = tmp;
                if (bool.TryParse(Bookmark, out tmp))
                    bookmark = tmp;
                if (Guid.TryParse(referenceId, out referenceGUIDTemp))
                    referenceGUIDValue = referenceGUIDTemp;

                var results = DBServices.GetReferencesByRepositoryGUID(this.ClientUser.UserId, this.CurrentRepository.ReposotoryGUID, string.IsNullOrEmpty(title.Trim()) ? null : title.Trim(), string.IsNullOrEmpty(Category.Trim()) ? null : Category.Trim(), referenceGUIDValue, inUse, bookmark);
                return PartialView("_referencesResults", results);
            }
            catch (Exception ex)
            {
                SystematicLR.GlobalFramework.GlobalMessage msg = new GlobalMessage()
                {
                    Message = "Error During Search Process : <br/> " + ex.Message,
                    MessageType = GlobalMessageType.error
                };
                return Content(msg.RenderMessage().ToString());

            }

        }
        public ActionResult DownloadReference(string refId)
        {
            try
            {
                Guid refGUID;

                if (Guid.TryParse(refId, out refGUID))
                {
                    var document = DBServices.GetReferencePathByGuid(refGUID, this.ClientUser.UserId, this.CurrentRepository.ReposotoryGUID);
                    var parentpath = Server.MapPath("~/App_Data/" + document);
                    if (System.IO.File.Exists(parentpath))
                    {

                        byte[] pdfByte = GetBytesFromFile(parentpath);
                        return File(pdfByte, "application/pdf");
                    }
                    else
                    {
                        return Content("File Not Found");
                    }

                }
            }
            catch (Exception ex)
            {

                return Content(ErrorManager.GetError(ex));

            }
            return null;
        }
        [HttpGet]
        public JsonResult SlidesClick(string Slide, string SlideValue, string refId)
        {
            JSONResponse response = new JSONResponse();
            try
            {
                Guid refGUID;
                bool slideValue = bool.Parse(SlideValue);
                if (Guid.TryParse(refId, out refGUID))
                {
                    if (Slide == "InUse")
                    {
                        DBServices.ChangeIsInUse(refGUID, this.CurrentRepository.ReposotoryGUID, slideValue);
                        if (slideValue)
                            response.message = "The reference has been marked as 'in Use'";
                        else
                            response.message = "The reference has been marked as 'not in Use'";


                    }
                    else if (Slide == "Bookmark")
                    {
                        DBServices.ChangeBookMark(refGUID, this.CurrentRepository.ReposotoryGUID, slideValue);
                        if (slideValue)
                            response.message = "the bookmark has been added to the reference";
                        else
                            response.message = "the bookmark has been Removed from the reference";

                    }
                    else
                    {
                        response.isSuccess = false;
                        response.message = "Invalid request";
                        response.showMessage = true;

                    }
                    response.isSuccess = true;
                    response.showMessage = true;

                }
                else
                {
                    response.isSuccess = false;
                    response.message = "Invalid Reference Id";
                    response.showMessage = true;
                }
            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.message = "Error";
                response.showMessage = true;
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetReferenceData(string refId, string repId)
        {
            JSONResponse response = new JSONResponse();
            try
            {
                Guid refGUID;

                if (Guid.TryParse(refId, out refGUID) || refId.Length == 8)
                {
                    Guid repository = this.CurrentRepository.ReposotoryGUID;
                    Entities.Model.GetReferenceByGuid_Result document = null;
                    document = refGUID != Guid.Empty ? DBServices.GetReferenceByGuid(refGUID, this.ClientUser.UserId, repository) : DBServices.GetReferenceByGuid(refId, this.ClientUser.UserId, repository);
                    if (document == null)
                    {
                        response.isSuccess = false;
                        response.message = "Reference not found or invalid Id";
                        response.showMessage = true;

                    }
                    else
                    {
                        response.isSuccess = true;
                        response.returndValue = document;
                    }

                }

                else
                {
                    response.isSuccess = false;
                    response.message = "Invalid Reference Id";
                    response.showMessage = true;
                }

            }
            catch (Exception ex)
            {
                response.isSuccess = false;
                response.message = "Reference not found or invalid Id";
                response.showMessage = true;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCountries()
        {
            var countries = DBServices.GetOptions(CurrentRepository.RepositoryId, "Country");
            return Json(countries, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetPublishers()
        {
            var options = DBServices.GetOptions(CurrentRepository.RepositoryId, "Publisher", 100);
            return Json(options, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetReferenceTypes()
        {
            var options = DBServices.GetOptions(CurrentRepository.RepositoryId, "ReferenceType");
            return Json(options, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetCategories()
        {
            var options = DBServices.GetOptions(CurrentRepository.RepositoryId, "Category");
            return Json(options, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetDatabases()
        {
            var options = DBServices.GetOptions(CurrentRepository.RepositoryId, "Database");
            return Json(options, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GoogleScholarSearch(string SearchText)
        {
            var output = GoogleScholarManager.SearchArticle(SearchText);
            return PartialView("_GoogleScholarResults", output);
        }
        [HttpGet]
        public ActionResult GoogleScholarCitation(string citaionId)
        {
            var citations = GoogleScholarManager.SearchCitation(citaionId);
            JSONResponse response = new JSONResponse();
            CitationResponse returendDate = new CitationResponse();
            if (citations != null)
            {
                citations.BibTeXURL = null;
                string url = citations.BibTeXURL;
                citations.BibTeXURL = null;
                if (!string.IsNullOrEmpty(url))
                {
                    var bibData = GoogleScholarManager.GetBibTeXData(url);
                    if (bibData != null)
                        returendDate.bibData = bibData;
                }
                returendDate.citations = citations;
            }
            response.isSuccess = true;
            response.returndValue = returendDate;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveDocument(DocumentData coll)
        {
            string path = null;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var InputFile = System.Web.HttpContext.Current.Request.Files["file"];

                if (InputFile != null && InputFile.ContentLength > 0)
                {
                    if (InputFile.ContentLength > (5 * 1000 * 1000))
                    {
                        JSONResponse res = new JSONResponse() { isSuccess = false, message = "Maximum size of the file is 8 MB", showMessage = true };
                        return Json(res, JsonRequestBehavior.AllowGet);
                    }
                    var filename = System.IO.Path.GetFileName(InputFile.FileName);
                    var ext = System.IO.Path.GetExtension(InputFile.FileName);
                    var newName = System.IO.Path.GetRandomFileName() + ext;
                    var parentpath = Server.MapPath("~/App_Data/Uploads/" + this.CurrentRepository.ReposotoryGUID);
                    if (!System.IO.Directory.Exists(parentpath))
                        System.IO.Directory.CreateDirectory(parentpath);
                    path = System.IO.Path.Combine(parentpath, newName);
                    InputFile.SaveAs(path);
                    path = "Uploads/" + this.CurrentRepository.ReposotoryGUID + "/" + newName;
                }
            }
            if (coll.ReferenceGuid != null)
            {
                try
                {
                    bool updateFile = coll.IsFileDeleted == "ture" || path != null;

                    Guid refId = Guid.Parse(coll.ReferenceGuid);
                    var ouput = ReferenceManager.EditReference(refId, coll.Title, coll.Year, coll.Authers, coll.Publisher, coll.Country, coll.ReferenceType, coll.CiteCount, coll.Versions, coll.Abstract, coll.Keywords, path, coll.OnlineLink, this.ClientUser.UserId, coll.FoundGS, coll.GSCitationId, null, coll.HostSite, coll.GSDocumentLink, coll.GSDocumentType, coll.lnkRelated, coll.CitedBy, coll.lnkVersions, this.CurrentRepository.RepositoryId, coll.Category, coll.FileLocalPath, coll.ckAnalyze, coll.Database, coll.APA, coll.Chicago, coll.Harvard, coll.MLA, coll.Vancouver, updateFile);
                    JSONResponse res2 = new JSONResponse() { isSuccess = true, returndValue = coll.ReferenceGuid };
                    return Json(res2, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    JSONResponse res2 = new JSONResponse() { isSuccess = false, showMessage = true, message = ex.Message };
                    return Json(res2, JsonRequestBehavior.AllowGet);

                }


            }
            else
            {
                var Id = ReferenceManager.AddNewReference(coll.Title, coll.Year, coll.Authers, coll.Publisher, coll.Country, coll.ReferenceType, coll.CiteCount, coll.Versions, coll.Abstract, coll.Keywords, path, coll.OnlineLink, this.ClientUser.UserId, coll.FoundGS, coll.GSCitationId, null, coll.HostSite, coll.GSDocumentLink, coll.GSDocumentType, coll.lnkRelated, coll.CitedBy, coll.lnkVersions, this.CurrentRepository.RepositoryId, coll.Category, coll.FileLocalPath, coll.ckAnalyze, coll.Database, coll.APA, coll.Chicago, coll.Harvard, coll.MLA, coll.Vancouver);
                JSONResponse res2 = new JSONResponse() { isSuccess = true, returndValue = Id };
                return Json(res2, JsonRequestBehavior.AllowGet);
            }



        }
        [Active(TreeViewEnum.treeDocuments, SubTreeEnum.GenerateReference)]
        public ActionResult GenerateReference()
        {
            var results = DBServices.GenerateReference(this.ClientUser.UserId, CurrentRepository.ReposotoryGUID);
            ViewBag.Link = System.Configuration.ConfigurationManager.AppSettings["linkPrefix"] + "repId=" + CurrentRepository.ReposotoryGUID.ToString() + "";
            return View(results);
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reference/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reference/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reference/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Reference/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reference/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Reference/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public static string StripHTML(string input)
        {
            return System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", " ").Replace("&quot", " ").Replace("&#353", " ");
        }
        [Active(TreeViewEnum.treeDocuments, SubTreeEnum.Search)]
        public ActionResult Search()
        {
            return View();
        }
        public ActionResult Similarity(string str1, string str2, string str3, double threshold1, Nullable<double> threshold2, Nullable<double> threshold3,bool isTFBMTF)
        {
            try
            {
                
                var lst1= Common.Utilities.TokenizeContent(str1);
                var lst2 = Common.Utilities.TokenizeContent(str2);
                var lst3 = Common.Utilities.TokenizeContent(str3);
                List<Header> keys = new List<Header>();
                foreach(var val in lst1)
                {
                    if(Common.Utilities.IsValidTerm(val) && !Common.Utilities.IsStopWord(val) &&
                    !string.IsNullOrEmpty(val.Trim()))
                    {
                        keys.Add(new Header(val,Common.Utilities.StemTerm(val), threshold1));
                    }
                    if (keys.Count == 6)
                        break;
                }
                foreach (var val in lst2)
                {
                    if (Common.Utilities.IsValidTerm(val) && !Common.Utilities.IsStopWord(val) &&
                    !string.IsNullOrEmpty(val.Trim()))
                    {
                        keys.Add(new Header(val,Common.Utilities.StemTerm(val), threshold2));
                    }
                    if (keys.Count == 6)
                        break;
                }
                foreach (var val in lst3)
                {
                    if (Common.Utilities.IsValidTerm(val) && !Common.Utilities.IsStopWord(val) &&
                    !string.IsNullOrEmpty(val.Trim()))
                    {
                        keys.Add(new Header(val,Common.Utilities.StemTerm(val), threshold3));
                    }
                    if (keys.Count == 6)
                        break;
                }
                while(keys.Count < 6)
                {
                    keys.Add(new Header());
                }
                if (isTFBMTF)
                {
                    var results = DBServices.GetSimilarityTFBMTF(this.CurrentRepository.RepositoryId,
                        keys[0],keys[1], keys[2], keys[3], keys[4], keys[5]);
                    return PartialView("_SimilarityResults", results);
                }
                var results2 = DBServices.GetSimilarity(this.CurrentRepository.RepositoryId,
                        keys[0], keys[1], keys[2], keys[3], keys[4]);
                return PartialView("_SimilarityResults", results2);
            }
            catch (Exception ex)
            {
                SystematicLR.GlobalFramework.GlobalMessage msg = new GlobalMessage()
                {
                    Message = "Error During Search Process : <br/> " + ex.Message,
                    MessageType = GlobalMessageType.error
                };
                return Content(msg.RenderMessage().ToString());

            }

        }

    }
}
