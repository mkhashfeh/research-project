﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystematicLR.Entities;
using SystematicLR.GlobalFramework;


namespace SystematicLR.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        
        public ActionResult Login()
        {
            if(this.ClientUser!=null)
                return ClientUser.IsUserAdmin ? RedirectToAction("Home", "Admin") : RedirectToAction("Index");
            return View();
        }
        [HttpPost]
        public ActionResult Login(string Email, string password)
        {
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(password))
            {
                ViewBag.ErrorMessage = "Please enter Email and Password";
                return View();
            }

            string encPassword = GlobalFramework.Security.Encrypt(password, true);

            var user = DBServices.LoginUser(Email, encPassword);
            if (user == null)
            {
                ViewBag.ErrorMessage = "Invalid Email or password";
                return View();
            }
            if (user.Status == "I")
            {
                ViewBag.DeactivationMessage = "The user has email \"" + Email + "\" has been deactivated<br/> Cantact Admin to reactivate";
                return View();
            }
            if (user.IsUserLocked && user.IsUserLocked)
            {
                ViewBag.DeactivationMessage = "The user has email \"" + Email + "\" has been Locked<br/> Cantact Admin to unlock";
                return View();
            }
            
            this.ClientUser = user;
            var repo = DBServices.GetRepositories(this.ClientUser.UserId);
            this.UserRepositories = repo;            
            return user.IsUserAdmin ? RedirectToAction("Home", "Admin") : RedirectToAction("Repositories");
        }
       
        [LoginAuth]
        public ActionResult NoAccess()
        {
            return View();
        }
        public ActionResult ErrorMessage(string message, string code, bool isAjaxRequet = false)
        {
            if (isAjaxRequet)
            {
                if (string.IsNullOrEmpty(message))
                    message = Configurations.GeneralError;
                //return Json(new JsonResponse { IsSuccess = false, Code = code, Message = message }, JsonRequestBehavior.AllowGet);


                return Json(new GlobalMessage()
                {
                    Message = string.IsNullOrEmpty(code) ? message : string.Format(Configurations.ErrorMessageWithCodeFormat, message, code),
                    MessageType = GlobalMessageType.error
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (string.IsNullOrEmpty(message))
                    ViewBag.Message = new GlobalMessage()
                    { Message = string.Format(Configurations.GeneralError, message, code), MessageType = GlobalMessageType.error };
                else
                    if (string.IsNullOrEmpty(code))
                    ViewBag.Message = new GlobalMessage()
                    { Message = message, MessageType = GlobalMessageType.error };
                else
                    ViewBag.Message = new GlobalMessage()
                    { Message = string.Format(Configurations.ErrorMessageWithCodeFormat, message, code), MessageType = GlobalMessageType.error };

                return View();
            }
        }
        [LoginAuth]
        [ActiveAttribute(TreeViewEnum.treeRepos)]
        public ActionResult Repositories()
        {
            var repo = DBServices.GetRepositories(this.ClientUser.UserId);
            return View(repo);
        }
        [LoginAuth]
        public ActionResult SetRepository(string Id)
        {
            if(!string.IsNullOrEmpty(Id))
            {
                if(this.UserRepositories!=null)
                {
                    var tmp = this.UserRepositories.FirstOrDefault(e => e.ReposotoryGUID.ToString() == Id);
                    if(tmp!=null)
                    this.CurrentRepository = tmp;
                }
                return RedirectToAction("RepositoryContent", "Reference");
            }
            return View("NoAccess");
        }
        public ActionResult Signout()
        {
            this.CurrentRepository = null;
            this.ClientUser = null;
            this.UserRepositories = null;
            return RedirectToAction("Login");
        }

    }
}