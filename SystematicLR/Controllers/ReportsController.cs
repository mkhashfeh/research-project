﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystematicLR.GlobalFramework;
using SystematicLR.Entities;
namespace SystematicLR.Controllers
{
    [LoginAuth]
    public class ReportsController : BaseController
    {
        // GET: Reports
        [Active(TreeViewEnum.treeAggregations)]
        public ActionResult Index(string c)
        {
            if (string.IsNullOrEmpty(c))
                return RedirectToNoAccess;
            List<AggResult> results = new List<AggResult>();
            string Title = "";
            SubTreeEnum ActiveSubTree = SubTreeEnum.no;
            switch (c)
            {
                case "cite":
                    results = DBServices.AggByCites(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Citation Count in Google";
                    ActiveSubTree = SubTreeEnum.AggCitation;
                    break;
                case "country":
                    results = DBServices.AggByCountry(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Country";
                    ActiveSubTree = SubTreeEnum.Aggcountry;
                    break;
                case "publisher":
                    results = DBServices.AggByPublisher(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Publisher";
                    ActiveSubTree = SubTreeEnum.AggPublisher;
                    break;
                case "referancetype":
                    results = DBServices.AggByReferanceType(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Referance Type";
                    ActiveSubTree = SubTreeEnum.AggReferanceType;
                    break;
                case "year":
                    results = DBServices.AggByYear(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Issuing Year";
                    ActiveSubTree = SubTreeEnum.AggYear;
                    break;
                case "version":
                    results = DBServices.AggByVersions(this.CurrentRepository.RepositoryId, this.ClientUser.UserId);
                    Title = "Aggregation By Version count in Google";
                    ActiveSubTree = SubTreeEnum.AggVarsion;
                    break;
                default:
                    break;
            }
            ViewBag.ActiveSubTree = ActiveSubTree.ToString();
            ViewBag.Title = Title;
            return View(results);
        }        
    }
}