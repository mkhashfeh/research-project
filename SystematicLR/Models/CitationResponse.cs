﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystematicLR.Models
{
    public class CitationResponse
    {
        public GoogleScholar.GSOutputCitation citations { set; get; }
        public GoogleScholar.GSOutputBibTeX bibData { set; get; }
    }
}