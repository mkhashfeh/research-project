﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystematicLR.Models
{
    public class DocumentData
    {
        public string IsFileDeleted { set; get; }
        public string ReferenceGuid { set; get; }
        public string Title { set; get; }
        public string Authers { set; get; }
        public int? Year { set; get; }
        public string Category { set; get; }
        public string Database { set; get; }
        public string FileLocalPath { set; get; }
        public string OnlineLink { set; get; }
        public bool ckAnalyze { set; get; }
        public string Country { set; get; }
        public string Publisher { set; get; }
        public string ReferenceType { set; get; }
        public string GSDocumentLink { set; get; }
        public string HostSite { set; get; }
        public int? CiteCount { set; get; }
        public string CitedBy { set; get; }
        public int? Versions { set; get; }
        public string lnkVersions { set; get; }
        public string GSCitationId { set; get; }
        public string lnkRelated { set; get; }
        public string Abstract { set; get; }
        public string GSDocumentType { set; get; }        
        public string Keywords { set; get; }
        public bool FoundGS { set; get; }
        public string APA { set; get; }
        public string Chicago { set; get; }
        public string Harvard { set; get; }
        public string MLA { set; get; }
        public string Vancouver { set; get; }
    }
}