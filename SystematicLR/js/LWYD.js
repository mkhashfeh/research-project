﻿
    function LoadingStart() {
        $("body").addClass("loading");

    }
function LoadingFinish() {
    $("body").removeClass("loading");
}
function SuccessMessage(message) {

    Lobibox.notify('success', {
        msg: message, sound: false
    });
}
function ErrorMessage(message) {

    Lobibox.notify('error', {
        msg: message, sound: false
    });
}
function WarningMessage(message) {

    Lobibox.notify('warning', {
        msg: message, sound: false
    });
}
function InfoMessage(message) {

    Lobibox.notify('info', {
        msg: message, sound: false,
    });
}
function ShowMessage(message) {
    //error='e', warning='w', success='s', info='i', validation='v'
    if (message.MessageTypeText == "success") {
        SuccessMessage(message.Message);
    }

    else if (message.MessageTypeText == "warning") {
        WarningMessage(message.Message);
    }

    else if (message.MessageTypeText == "error") {
        ErrorMessage(message.Message);
    }

    else if (message.MessageTypeText == "info") {
        InfoMessage(message.Message);
    }
}
