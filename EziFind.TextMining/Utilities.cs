﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Lucene.Net.Analysis;
using OpenNLP.Tools.Tokenize;
using OpenNLP.Tools.SentenceDetect;
using OpenNLP.Tools.PosTagger;
using OpenNLP.Tools.Chunker;
namespace EziFind.TextMining
{
    public class Utilities
    {
        public static string Stem(string text)
        {
            StringReader stringReader = new StringReader(text);
            TokenStream tokenStream = analyzer.TokenStream("defaultFieldName", stringReader);
            PorterStemFilter filter = new PorterStemFilter(tokenStream);
            Token token = filter.Next();

            StringBuilder sb = new StringBuilder();

            while (token != null)
            {
                sb.Append(token.TermText() + " ");
                token = filter.Next();
            }
            stringReader.Dispose();
            return sb.ToString();

        }
    }
}
