﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EziFind.TextMining
{
    public class Word
    {
        public string Original { set; get; }
        public string Stem { set; get; }
        public List<int> positions { set; get; }
        public decimal Size { set; get; }
        public bool isBold { set; get; }
        public Paragraph paragraph { set;get;}
        public bool isStopWord { get; }
        public bool isFromAbstract { set; get; }

    }
    public class Paragraph
    {
        public Guid ParagraphId { set; get; }
        public string text { set; get; }
    }
}
